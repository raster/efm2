# EFM

## Remake of Enlightenment's file manager

This re-does EFM so it can be used in and out-of-process too in E. This
moves the FS layer entirely to a child process that EFM talks to over
stdio. The only direct file access is via local "cached" files in
$HOME that are intended to always be fast access (the location could
move somewhere - but this is the only sane way - have to depend on
some fast data store).

![Screenshot](/data/readme/shot1.png)
![Screenshot](/data/readme/shot2.png)
![Screenshot](/data/readme/shot3.png)

-----

Things that need doing:

[TODO](TODO.md)

## Requirements

Must (EFL from git - must be latest master at any time):

* [efl](https://git.enlightenment.org/enlightenment/efl)

-----

## Compiling and Installing

Meson is the build system used for this project. For more information
please see [mesonbuild.com](https://mesonbuild.com)

You will need normal build tooling installed such as a compiler (gcc
or clang for example), pkg-config, ninja, any relevant package-dev or
package-devel packages if your distribution splits out development
headers (e.g. libc6-dev) etc.

Depending on where dependencies (like efl) are installed, you might
have to
set your PKG_CONFIG_PATH environment variable like:

```sh
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/lib/pkgconfig
```

Also note that some distributions like to add extra arch directories
to your library locations so you might have to have more like:

```sh
export
PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig:/usr/local/lib/x86_64-linux-gnu/pkgconfig:/usr/lib/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig
```

You will need to ensure that the destination library directory (e.g.
/usr/local/lib is in your /etc/ld.so.conf or /etc/ld.so.conf.d/
files and after installing anything that installs libraries you
re-run ldconfig. Please see relevant documentation on ldconfig and
ld.so.conf for your distribution.

You might also want to add the destination bin dir to your environment
variable PATH (see documentation on your shell PATH variable) such as:

```sh
export PATH=/usr/local/bin:/usr/bin:/bin
```

Normal compilation in /usr/local:

```sh
meson . build
ninja -C build
sudo ninja -C build install
```

For meson build generic options:

```sh
meson --help
```

For a list of project specific options supported:

```sh
cat meson_options.txt
```

To set 1 or more project specific options:

```sh
meson --prefix=/path/to -Doption=value [-Dother=value2] [...] . build
```

To display current configuration:

```sh
meson configure build
```

The above will only work after at least the following is done:

```sh
meson . build
```

### Quick build help

How to clean out the build and config and start fresh:

```sh
rm -rf build
```

How to make a dist tarball and check its build:
(must do it from git tree clone and commit all changes to git first)

```sh
ninja -C build dist
```

How to change prefix:

```sh
meson --prefix=/path/to/prefix . build
```

How to install in a specific destination directory for packaging:

```sh
DESTDIR=/path/to/destdir ninja -C build install
```

How to build with verbose output (full commands run):

```sh
ninja -C build -v
```

-----

To run this (for testing) either just run it and it will list/open the
current working directory:

```sh
efm
```

Or you can pass in a path to a directory to open up:

```sh
efm /path/to/dir
```

