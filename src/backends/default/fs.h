#ifndef FS_H
#define FS_H
#include <Eina.h>

Eina_Bool fs_scan(const char *src);
Eina_Bool fs_cp_rm(const char *src, const char *dst, Eina_Bool report_err,
                   Eina_Bool cp, Eina_Bool rm);
Eina_Bool fs_mv(const char *src, const char *dst, Eina_Bool report_err);
Eina_Bool fs_cp(const char *src, const char *dst, Eina_Bool report_err);
Eina_Bool fs_ln(const char *src, const char *dst, Eina_Bool report_err);

#endif