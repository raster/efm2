#ifndef THUMB_H
#define THUMB_H
#include <Elementary.h>
#include "efm_config.h"
#include "sha.h"
#include "thumb_check.h"

extern Evas_Object *win;
extern Evas_Object *subwin;
extern Evas_Object *image;

void thumb_image_write(Eet_File *ef, const char *key, Evas_Object *img,
                       Eina_Bool a, Eina_Bool lossy);

void thumb_url_str_get(const char *url, size_t max,
                       void (*cb)(void *data, const char *result),
                       const void *data);
void thumb_url_bin_get(const char *url, size_t max,
                       void (*cb)(void *data, const void *result, size_t size),
                       const void *data);

typedef struct
{
  char *url;
  int   w, h;
} Search_Result;

void thumb_search_image(const char *str,
                        void (*cb)(void *data, Eina_List *results_orig,
                                   Eina_List *results_cached),
                        void *data);

int thumb_image(Eet_File *ef, const char *path, const char *mime,
                const char *thumb);
int thumb_font(Eet_File *ef, const char *path, const char *mime,
               const char *thumb);
int thumb_paged(Eet_File *ef, const char *path, const char *mime,
                const char *thumb);
int thumb_music(Eet_File *ef, const char *path, const char *mime,
                const char *thumb);
int thumb_video(Eet_File *ef, const char *path, const char *mime,
                const char *thumb);
int thumb_edje(Eet_File *ef, const char *path, const char *mime,
               const char *thumb);

// utility
static inline void
scale(int *w, int *h, int maxw, int maxh, Eina_Bool no_scale_up)
{ // write a big 1024x1024 preview (but no larger than the original image)
  int ww, hh;

  ww = maxw;
  hh = (*h * maxw) / *w;
  if (hh > maxh)
    { // too tall = so limit height and scale down keeping aspect
      hh = maxh;
      ww = (*w * maxh) / *h;
    }
  if ((no_scale_up) && ((ww > *h) || (hh > *h)))
    {
      ww = *w;
      hh = *h;
    }
  *w = ww;
  *h = hh;
}

static inline void
scale_out(int *w, int *h, int maxw, int maxh, Eina_Bool no_scale_up)
{ // write a big 1024x1024 preview (but no larger than the original image)
  int ww, hh;

  ww = maxw;
  hh = (*h * maxw) / *w;
  if (hh < maxh)
    { // not tall enough = so limit height and scale down keeping aspect
      hh = maxh;
      ww = (*w * maxh) / *h;
    }
  if ((no_scale_up) && ((ww > *h) || (hh > *h)))
    {
      ww = *w;
      hh = *h;
    }
  *w = ww;
  *h = hh;
}
#endif