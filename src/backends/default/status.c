// enable eina_vpath ...
#define EFL_BETA_API_SUPPORT 1
// for vasprintf()
#define _GNU_SOURCE

#include "status.h"
#include "esc.h"

#include <Eina.h>
#include <Ecore_File.h>
#include <fcntl.h>

typedef enum
{
  MSG_OP,
  MSG_DST,
  MSG_COUNT,
  MSG_POS,
  MSG_ERR,
  MSG_END
} Msg_Type;

typedef struct
{
#define MSG_HEAD              \
  Eina_Thread_Queue_Msg head; \
  Msg_Type              type
  MSG_HEAD;
  void *dummy;
} Msg;

typedef struct
{
  MSG_HEAD;
  char *op;
} Msg_Op;

typedef struct
{
  MSG_HEAD;
  char *dst;
} Msg_Dst;

typedef struct
{
  MSG_HEAD;
  unsigned long long inc;
  char              *str;
} Msg_Count;

typedef struct
{
  MSG_HEAD;
  unsigned long long inc;
  char              *str;
} Msg_Pos;

typedef struct
{
  MSG_HEAD;
} Msg_End;

typedef struct
{
  MSG_HEAD;
  char *src;
  char *dst;
  char *str;
} Msg_Err;

static unsigned long long op_count      = 0;
static unsigned long long op_pos        = 0;
static unsigned long long op_count_prev = 0;
static unsigned long long op_pos_prev   = 0;
static Eina_Bool          op_end        = EINA_FALSE;
static Eina_Bool          op_end_prev   = EINA_FALSE;
static char              *op_str        = NULL;
static int                status_fd     = -1;
static char              *status_file   = NULL;
static Eina_Thread        status_thread;
static Eina_Thread_Queue *status_thq = NULL;

static void
_fd_printf(int fd, const char *fmt, ...)
{
  va_list args;
  char   *str;
  size_t  len;

  va_start(args, fmt);
  len = vasprintf(&str, fmt, args);
  va_end(args);

  if ((len == 0) || (!str)) return;
  write(fd, str, len);
  free(str);
}

static void
_status_flush(void)
{
  if ((op_count == op_count_prev) && (op_pos == op_pos_prev)
      && (op_end == op_end_prev))
    return;
  if ((op_count != op_count_prev) || (op_pos != op_pos_prev))
    _fd_printf(status_fd, "CMD progress pos=%llu count=%llu\n", op_pos,
               op_count);
  if (op_str) _fd_printf(status_fd, "CMD progress str=%s\n", op_str);
  if ((op_end) && (op_end != op_end_prev)) _fd_printf(status_fd, "CMD end\n");
  free(op_str);
  op_str        = NULL;
  op_count_prev = op_count;
  op_pos_prev   = op_pos;
  op_end_prev   = op_end;
}

static void *
_cb_status_thread(void *data EINA_UNUSED, Eina_Thread t EINA_UNUSED)
{
  Msg  *msg;
  void *ref;

  for (;;)
    { // loop and poll every 100ms (0.1s) for messages
      msg = eina_thread_queue_poll(status_thq, &ref);
      if (!msg)
        {
          // flush out write of last state - rate limit to 100ms
          _status_flush();
          usleep(100 * 1000); // wait for 100ms if no messages
          continue;
        }
      switch (msg->type)
        {
        case MSG_OP:
          {
            Msg_Op *msg2 = (Msg_Op *)msg;
            // write this out immediately
            _fd_printf(status_fd, "CMD op op=%s\n", msg2->op);
            _fd_printf(status_fd, "CMD pid pid=%i\n", (int)getpid());
            free(msg2->op);
          }
          break;
        case MSG_DST:
          {
            Msg_Dst *msg2 = (Msg_Dst *)msg;
            // write this out immediately
            _fd_printf(status_fd, "CMD dst dst=%s\n", msg2->dst);
            free(msg2->dst);
          }
          break;
        case MSG_COUNT:
          {
            Msg_Count *msg2 = (Msg_Count *)msg;
            op_count += msg2->inc;
            free(op_str);
            op_str = msg2->str;
          }
          break;
        case MSG_POS:
          {
            Msg_Pos *msg2 = (Msg_Pos *)msg;
            op_pos += msg2->inc;
            free(op_str);
            op_str = msg2->str;
          }
          break;
        case MSG_ERR:
          {
            Msg_Err *msg2 = (Msg_Err *)msg;
            // write this out immediately
            _fd_printf(status_fd, "CMD error err=%s\n", msg2->str);
            if (msg2->src)
              _fd_printf(status_fd, "CMD error src=%s\n", msg2->src);
            if (msg2->dst)
              _fd_printf(status_fd, "CMD error dst=%s\n", msg2->dst);
            _fd_printf(status_fd, "CMD error-done\n");
            free(msg2->src);
            free(msg2->dst);
            free(msg2->str);
            eina_thread_queue_wait_done(status_thq, ref);
            exit(1);
          }
          break;
        case MSG_END:
          {
            op_end = EINA_TRUE;
            _status_flush();
            if (status_fd >= 0)
              {
                close(status_fd);
                status_fd = -1;
              }
            if (status_file)
              {
                unlink(status_file);
                free(status_file);
                status_file = NULL;
              }
            eina_thread_queue_wait_done(status_thq, ref);
            return NULL;
          }
          break;
        default:
          break;
        }
      eina_thread_queue_wait_done(status_thq, ref);
    }
  return NULL;
}

void
status_begin(void)
{
  Eina_Bool ok;
  char      buf[PATH_MAX];

  op_count = op_pos = 0;
  eina_vpath_resolve_snprintf(buf, sizeof(buf), "(:usr.run:)/efm/ops");
  ecore_file_mkpath(buf);
  eina_vpath_resolve_snprintf(buf, sizeof(buf), "(:usr.run:)/efm/ops/%i.status",
                              (int)getpid());
  status_file = strdup(buf);
  status_fd   = open(status_file, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  status_thq  = eina_thread_queue_new();
  ok          = eina_thread_create(&status_thread, EINA_THREAD_BACKGROUND, -1,
                                   _cb_status_thread, NULL);
  if (!ok) abort();
}

void
status_end(void)
{
  Msg_End *msg;
  void    *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_End), &ref);
  msg->type = MSG_END;
  eina_thread_queue_send_done(status_thq, ref);
  eina_thread_join(status_thread);
}

void
status_op(const char *op)
{
  Msg_Op *msg;
  void   *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_Op), &ref);
  msg->type = MSG_OP;
  msg->op   = op ? escape(op) : NULL;
  eina_thread_queue_send_done(status_thq, ref);
}

void
status_dst(const char *dst)
{
  Msg_Dst *msg;
  void    *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_Dst), &ref);
  msg->type = MSG_DST;
  msg->dst  = dst ? escape(dst) : NULL;
  eina_thread_queue_send_done(status_thq, ref);
}

void
status_count(unsigned long long num_inc, const char *str)
{
  Msg_Count *msg;
  void      *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_Count), &ref);
  msg->type = MSG_COUNT;
  msg->inc  = num_inc;
  msg->str  = str ? escape(str) : NULL;
  eina_thread_queue_send_done(status_thq, ref);
}

void
status_pos(unsigned long long pos_inc, const char *str)
{
  Msg_Pos *msg;
  void    *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_Pos), &ref);
  msg->type = MSG_POS;
  msg->inc  = pos_inc;
  msg->str  = str ? escape(str) : NULL;
  eina_thread_queue_send_done(status_thq, ref);
}

void
status_error(const char *src, const char *dst, const char *str)
{
  Msg_Err *msg;
  void    *ref;

  msg       = eina_thread_queue_send(status_thq, sizeof(Msg_Err), &ref);
  msg->type = MSG_ERR;
  msg->src  = src ? escape(src) : NULL;
  msg->dst  = dst ? escape(dst) : NULL;
  msg->str  = str ? escape(str) : NULL;
  eina_thread_queue_send_done(status_thq, ref);
}
