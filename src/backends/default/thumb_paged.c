// generate thumbnail for images
#include "thumb.h"

// XXX: can do progressive resize down ie scale to 512 then take 512 and
// halve to 256 then halve it to 128 etc. rather than render from orig to
// target size....

static Evas_Object *im    = NULL;
static Eina_Bool    alpha = EINA_FALSE;
static int          iw = 0, ih = 0;

static void
_thumb_page_setup(void)
{ // create and show image
  im = evas_object_image_filled_add(evas_object_evas_get(subwin));
  evas_object_show(im);
}

static Eina_Bool
_thumb_page_file_set(const char *file, int page)
{ // set file to image, get size & alpha
  char buf[32];

  snprintf(buf, sizeof(buf), "%i", page);
  evas_object_image_file_set(im, file, buf);
  evas_object_image_size_get(im, &iw, &ih);
  if ((iw > 0) && (ih > 0)) return EINA_TRUE;
  alpha = evas_object_image_alpha_get(im);
  return EINA_FALSE;
}

int
thumb_paged(Eet_File *ef, const char *path, const char *mime EINA_UNUSED,
            const char *thumb EINA_UNUSED)
{
  const int     sizes[] = { 512, 256, 128, 64, 32, 16, 0 };
  int           w, h, i;
  char          buf[128];
  unsigned char a;

  _thumb_page_setup();
  // add filled image to then size accordingly
  _thumb_page_file_set(path, 0);
  // if size is bunk - we can't load it...
  if ((iw <= 0) || (ih < 0)) return 2;

  // write a big 1024x1024 preview (but no larger than the original doc)
  w = iw;
  h = ih;
  scale(&w, &h, 1024, 1024, EINA_TRUE);

  // resize to target size
  evas_object_resize(im, w, h);
  evas_object_resize(subwin, w, h);
  // render our current state and pick up pixel results
  elm_win_render(subwin);
  // save out preview size
  snprintf(buf, sizeof(buf), "image/preview");
  thumb_image_write(ef, buf, image, alpha, EINA_TRUE);
  snprintf(buf, sizeof(buf), "%i %i", w, h);
  eet_write(ef, "image/preview/size", buf, strlen(buf) + 1,
            EET_COMPRESSION_NONE);

  // multiple thumb sizes so can load/pick the best one at runtime
  for (i = 0; sizes[i] != 0; i++)
    {
      // scale down and keep aspect
      w = sizes[i];
      h = (ih * sizes[i]) / iw;
      if (h > sizes[i])
        { // too tall = so limit height and scale down keeping aspect
          h = sizes[i];
          w = (iw * sizes[i]) / ih;
        }
      // resize to target size
      evas_object_resize(im, w, h);
      evas_object_resize(subwin, w, h);
      // render our current state and pick up pixel results
      elm_win_render(subwin);
      // save out thumb size
      snprintf(buf, sizeof(buf), "image/thumb/%i", sizes[i]);
      thumb_image_write(ef, buf, image, alpha, EINA_TRUE);
    }

  // write original alpha flag
  a = alpha;
  if (alpha) eet_write(ef, "orig/image/alpha", &a, 1, EET_COMPRESSION_NONE);
  // write original size
  snprintf(buf, sizeof(buf), "%i %i", iw, ih);
  eet_write(ef, "orig/image/size", buf, strlen(buf) + 1, EET_COMPRESSION_NONE);

  // write page previews of the first 32 pages
  for (i = 0; i < 32; i++)
    {
      if (!_thumb_page_file_set(path, i)) break;
      w = iw;
      h = ih;
      scale(&w, &h, 512, 512, EINA_TRUE);
      snprintf(buf, sizeof(buf), "image/page/%i", i);
      evas_object_resize(im, w, h);
      evas_object_resize(subwin, w, h);
      // render our current state and pick up pixel results
      elm_win_render(subwin);
      // save out thumb size
      thumb_image_write(ef, buf, image, alpha, EINA_TRUE);
    }

  return 0;
}
