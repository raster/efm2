// for copy_file_range()
#include "efm_config.h"
#define _FILE_OFFSET_BITS 64

#include <Eina.h>
#include <Ecore_File.h>

#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <fcntl.h>

#include "status.h"
#include "fs.h"

static Eina_Strbuf *
error_strbuf_new(const char *op)
{
  Eina_Strbuf *buf = eina_strbuf_new();

  if (!buf) abort();
  eina_strbuf_append(buf, op);
  eina_strbuf_append(buf, ": ");
  return buf;
}

// generic error handler. special case handling all errnos for everything is
// pretty insane - so handle non-errors in switches and otherwise pass to
// this tio handle the reast in a generic way.
static void
_error_handle(const char *src, const char *dst, const char *op, int errno_in)
{
  Eina_Strbuf *buf = error_strbuf_new(op);

#define HNDL(_err, _str)                               \
_err:                                                  \
  eina_strbuf_append(buf, _str);                       \
  status_error(src, dst, eina_strbuf_string_get(buf)); \
  break
  switch (errno_in)
    {
      HNDL(case EACCES, "Access denied");
      HNDL(case EFAULT, "Memory Fault");
      HNDL(case ELOOP, "Too many symlinks");
      HNDL(case ENAMETOOLONG, "Name too long");
      HNDL(case ENOMEM, "Out of memory");
      HNDL(case ENOTDIR, "Path component is not a directory");
      HNDL(case EOVERFLOW, "Overflow");
      HNDL(case EDQUOT, "Over quota");
      HNDL(case EINVAL, "Invalid value");
      HNDL(case EMLINK, "Too many links");
      HNDL(case ENOENT, "Does not exist");
      HNDL(case ENOSPC, "Disk full");
      HNDL(case EPERM, "Permission denied");
      HNDL(case EROFS, "Read only filesystem");
      HNDL(case EBADF, "Bad file descriptor");
      HNDL(case EIO, "I/O error");
      HNDL(case EISDIR, "Destination is a directory");
      HNDL(case EFBIG, "File too big");
      HNDL(case ETXTBSY, "Text file busy");
      HNDL(case EBUSY, "File busy");
      HNDL(case ENOTEMPTY, "Destination is not empty");
      HNDL(case EEXIST, "File exists");
      HNDL(default, "Unknown error");
    }
#undef HNDL
  eina_strbuf_free(buf);
}

static void
error_ok_pos(const char *src EINA_UNUSED, const char *op, const char *str)
{
  Eina_Strbuf *buf = error_strbuf_new(op);

  eina_strbuf_append(buf, str);
  status_pos(1, eina_strbuf_string_get(buf));
  eina_strbuf_free(buf);
}

static Eina_Bool
fs_cp_file(const char *src, const char *dst, const char *op, struct stat src_st)
{
  // copy a normal file from src to dst - use optimized copy range if possible
  // and fall abck to read + write into userspace buffer otherwise. use the
  // struct stat mode passed in for created file. return true if fully
  // successful or false otherwise
  int       fd_in, fd_ou;
  void     *old_copy_buf = NULL;
  Eina_Bool res          = EINA_TRUE;

  fd_in = open(src, O_RDONLY);
  fd_ou = open(dst, O_WRONLY | O_CREAT, src_st.st_mode);
  if ((fd_in >= 0) && (fd_ou >= 0))
    {
      ssize_t   size = 1 * 1024 * 1024; // 1mb default for copy range
      ssize_t   ret, ret2;
      off_t     off_in = 0, off_ou = 0;
      Eina_Bool old_copy = EINA_FALSE;

      for (;;)
        {
          if (old_copy)
            {
              if (!old_copy_buf)
                {
                  size         = 256 * 1024; // drop to 256k buffer for r+w
                  old_copy_buf = malloc(size);
                  if (!old_copy_buf)
                    {
                      res = EINA_FALSE;
                      goto err;
                    }
                }
again_read:
              ret = read(fd_in, old_copy_buf, size);
              if (ret < 0)
                {
                  switch (errno)
                    {
                    case EAGAIN:
                    case EINTR:
                      goto again_read;
                    default:
                      _error_handle(src, NULL, op, errno);
                      res = EINA_FALSE;
                      goto err;
                    }
                }
              else
                {
                  off_in += ret;
again_write:
                  ret2 = write(fd_ou, old_copy_buf, ret);
                  if (ret2 < 0)
                    {
                      switch (errno)
                        {
                        case EAGAIN:
                        case EINTR:
                          goto again_write;
                        default:
                          _error_handle(NULL, dst, op, errno);
                          res = EINA_FALSE;
                          goto err;
                        }
                    }
                  else if (ret2 == ret)
                    {
                      status_pos(ret2, src);
                      off_ou += ret;
                      if (ret < size) break; // end of file
                    }
                }
            }
          else
            {
              ret = copy_file_range(fd_in, &off_in, fd_ou, &off_ou, size, 0);
              if (ret < 0)
                {
                  switch (errno)
                    {
                    case EOPNOTSUPP:
                    case EXDEV:
                      // fall back to old read+write copy into userspace buf
                      old_copy = EINA_TRUE;
                      break;
                    default:
                      _error_handle(src, dst, op, errno);
                      res = EINA_FALSE;
                      goto err;
                    }
                }
              else
                {
                  status_pos(ret, src);
                  if (ret < size) break; // end of file
                }
            }
        }
    }
  else res = EINA_FALSE;
err:
  if (old_copy_buf) free(old_copy_buf);
  if (fd_in >= 0) close(fd_in);
  if (fd_ou >= 0) close(fd_ou);
  return res;
}

// this scans a tree to build a potential operation progress count. it may
// not be 100% right as the fs can change while the scan happens and after
// so any move that devolves into a cp + rm isn't going to be atomic and
// handle a changing fs while it works anyway
Eina_Bool
fs_scan(const char *src)
{
  Eina_Bool      res = EINA_TRUE;
  Eina_Iterator *it;
  const char    *s;
  struct stat    st;
  const char    *op = "Scan";

  if (strlen(src) < 1) return EINA_FALSE;

  if (lstat(src, &st) != 0)
    {
      switch (errno)
        {
        case ENOENT: // ignore this error - file removed during scan ?
          return EINA_TRUE;
        default:
          _error_handle(src, NULL, op, errno);
          return EINA_FALSE;
        }
    }
  if (S_ISDIR(st.st_mode))
    { // it's a dir - scan this recursively
      it = eina_file_ls(src);
      if (it)
        {
          EINA_ITERATOR_FOREACH(it, s)
          {
            if ((res) && (!fs_scan(s))) res = EINA_FALSE;
            eina_stringshare_del(s);
          }
          eina_iterator_free(it);
        }
    }
  else
    {
      // the file itself count as 1 progress item - useful if it's a symlink
      // or a char or block device etc.
      status_count(1, ecore_file_file_get(src));
      // in addition each byte in the file count as a progress item too
      if (st.st_size > 0) status_count(st.st_size, ecore_file_file_get(src));
    }
  return res;
}

Eina_Bool
fs_cp_rm(const char *src, const char *dst, Eina_Bool report_err, Eina_Bool cp,
         Eina_Bool rm)
{ // cp_rm /path/to/src/filename /path/to/dst/filename
  Eina_Bool      res = EINA_TRUE;
  Eina_Iterator *it;
  const char    *s;
  struct stat    st;
  mode_t         old_umask;
  struct timeval times[2];
  const char    *op = "";
  int            ret;

  if ((!src) || (!dst) || (strlen(src) < 1)) return EINA_FALSE;

  if (rm && cp) op = "Move";
  else if (!rm && cp) op = "Copy";
  else if (rm && !cp) op = "Delete";

  old_umask = umask(0);
  if (lstat(src, &st) != 0)
    {
      switch (errno)
        {
        case ENOENT: // ignore this error - file removed during scan ?
          error_ok_pos(src, op, "File vanished");
          goto err;
        default:
          _error_handle(src, dst, op, errno);
          res = EINA_FALSE;
          goto err;
        }
    }
  if (S_ISDIR(st.st_mode))
    { // it's a dir - scan this recursively
      if (cp)
        {
          if (mkdir(dst, st.st_mode) != 0)
            {
              switch (errno)
                {
                case EEXIST: // ignore - mv would mv over this anyway
                  break;
                default:
                  _error_handle(NULL, dst, op, errno);
                  res = EINA_FALSE;
                  goto err;
                }
            }
        }
      it = eina_file_ls(src);
      if (it)
        {
          Eina_Strbuf *buf = eina_strbuf_new();

          if (!buf)
            {
              fprintf(stderr, "Out of memory!\n");
              abort();
            }
          EINA_ITERATOR_FOREACH(it, s)
          {
            const char *fs = ecore_file_file_get(s);

            if (buf)
              {
                if ((res) && (fs))
                  {
                    eina_strbuf_reset(buf);
                    eina_strbuf_append(buf, dst);
                    eina_strbuf_append(buf, "/");
                    eina_strbuf_append(buf, fs);
                    if (!fs_cp_rm(s, eina_strbuf_string_get(buf), report_err,
                                  cp, rm))
                      res = EINA_FALSE;
                  }
              }
            eina_stringshare_del(s);
          }
          eina_iterator_free(it);
          eina_strbuf_free(buf);
        }
    }
  else if (S_ISLNK(st.st_mode))
    {
      if (cp)
        {
          char    link[PATH_MAX];
          ssize_t lnsz;

          lnsz = readlink(src, link, sizeof(link));
          if ((lnsz > 0) && (lnsz < (ssize_t)sizeof(link)))
            {
              if (symlink(link, dst) < 0)
                { // soft error? e.g. mv on FAT fs? report but move on
                  error_ok_pos(dst, op, "Error creating symlink");
                }
            }
          else if (lnsz < 0)
            {
              switch (errno)
                {
                case ENOENT: // ignore this error - file removed during scan ?
                  error_ok_pos(src, op, "File vanished");
                  break;
                default:
                  _error_handle(src, dst, op, errno);
                  return EINA_FALSE;
                }
            }
          else
            { // 0 sized symlink ... WAT?
              error_ok_pos(src, op, "Zero sized symlink");
            }
        }
    }
  else if (S_ISFIFO(st.st_mode))
    {
      if (cp)
        {
          if (mkfifo(dst, st.st_mode) < 0)
            { // soft error? ignore?
            }
        }
    }
  else if (S_ISSOCK(st.st_mode))
    {
      if (cp)
        { // we can't just make sockets - so ignore but document here
        }
    }
  else if ((S_ISCHR(st.st_mode)) || (S_ISBLK(st.st_mode)))
    {
      if (cp)
        {
          if (mknod(dst, st.st_mode, st.st_rdev) < 0)
            { // soft error? ignore?
            }
        }
    }
  else
    {
      if (cp)
        {
          res = fs_cp_file(src, dst, op, st);
        }
    }
  if ((rm) && (res))
    {
      ret = 0;
      if (S_ISDIR(st.st_mode)) ret = rmdir(src);
      else ret = unlink(src);
      if (ret != 0)
        {
          switch (errno)
            {
            case ENOENT: // ignore missing
              break;
            default:
              _error_handle(src, NULL, op, errno);
              res = EINA_FALSE;
              goto err_unlink;
            }
        }
    }
err_unlink:
  chown(dst, st.st_uid, st.st_gid); // ignore err
// duplicate mtime+atime from src down to msic/nsec if possible
#ifdef STAT_NSEC
#  ifdef st_mtime
#    define STAT_NSEC_ATIME(st) (unsigned long long)((st).st_atim.tv_nsec)
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st).st_mtim.tv_nsec)
#    define STAT_NSEC_CTIME(st) (unsigned long long)((st).st_ctim.tv_nsec)
#  else
#    define STAT_NSEC_ATIME(st) (unsigned long long)((st).st_atimensec)
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st).st_mtimensec)
#    define STAT_NSEC_CTIME(st) (unsigned long long)((st).st_ctimensec)
#  endif
#else
#  define STAT_NSEC_ATIME(st) (unsigned long long)(0)
#  define STAT_NSEC_MTIME(st) (unsigned long long)(0)
#  define STAT_NSEC_CTIME(st) (unsigned long long)(0)
#endif
  times[0].tv_sec  = st.st_atime;
  times[0].tv_usec = STAT_NSEC_ATIME(st) * 1000;
  times[1].tv_sec  = st.st_mtime;
  times[1].tv_usec = STAT_NSEC_MTIME(st) * 1000;
  utimes(dst, times); // ingore err
err:
  umask(old_umask);
  return res;
}

Eina_Bool
fs_mv(const char *src, const char *dst, Eina_Bool report_err)
{ // mv /path/to/src/filename /path/to/dst/filename
  Eina_Bool   res = EINA_TRUE;
  int         ret;
  const char *op = "Move";

  ret = rename(src, dst);
  if (ret == 0) goto done;
  else
    {
      switch (errno)
        {
        case EXDEV: // revert to cp + rm
          res = fs_cp_rm(src, dst, report_err, EINA_TRUE, EINA_TRUE);
          goto done;
          break;
        default:
          if (report_err) _error_handle(src, dst, op, errno);
          res = EINA_FALSE;
          break;
        }
    }
done:
  status_pos(1, src);
  return res;
}

Eina_Bool
fs_cp(const char *src, const char *dst, Eina_Bool report_err)
{ // cp /path/to/src/filename /path/to/dst/filename
  Eina_Bool res;
  const char *op  = "Copy";

  res = fs_cp_rm(src, dst, report_err, EINA_TRUE, EINA_FALSE);
  if (report_err) _error_handle(src, dst, op, errno);
  status_pos(1, src);
  return res;
}

Eina_Bool fs_ln(const char *src, const char *dst, Eina_Bool report_err)
{ // ln -s /path/to/src/filename /path/to/dst/filename
  Eina_Bool res = EINA_TRUE;
  const char *op  = "Symlink";
  int         ret;

  ret = symlink(src, dst);
  if (ret == 0) goto done;
  else
    {
      if (report_err) _error_handle(src, dst, op, errno);
      res = EINA_FALSE;
    }
done:
  status_pos(1, src);
  return res;
}

// add fs_rm() fs_trash() fs_rename()
