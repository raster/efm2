#include "thumb.h"

void
thumb_image_write(Eet_File *ef, const char *key, Evas_Object *img, Eina_Bool a,
                  Eina_Bool lossy)
{ // get rendered pixels from image representing subwin
  void *pixels = evas_object_image_data_get(img, EINA_FALSE);
  int   w, h;

  if (!pixels) return;
  evas_object_image_size_get(img, &w, &h);
  if (lossy)
    eet_data_image_write(ef, key, pixels, w, h, a, 0, /* compr */
                         80,                          /* qual */
                         EET_IMAGE_JPEG);
  else
    eet_data_image_write(ef, key, pixels, w, h, a,
                         EET_COMPRESSION_HI, /* compr */
                         0,                  /* qual */
                         EET_IMAGE_LOSSLESS);
  evas_object_image_data_set(img, pixels); // put pixels back we borrowed
}
