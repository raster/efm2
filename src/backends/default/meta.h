#ifndef META_H
#define META_H
#include <Eina.h>

typedef struct _Meta_File Meta_File;

void meta_init(const char *config_dir);
void meta_shutdown(void);

void meta_sync(void);

void meta_path_sync(const char *path);

Eina_Stringshare *meta_get(const char *path, const char *meta);
void              meta_set(const char *path, const char *meta, const char *data);

char     *meta_path_find(const char *path, const char *extn);
char     *meta_path_user_find(const char *path, const char *extn);
Eina_Bool meta_path_prepare(const char *path);
Eina_Bool meta_path_can_write(const char *path);

Meta_File *meta_file_load(const char *path);
void       meta_file_free(Meta_File *mf);
Meta_File *meta_file_copy(Meta_File *mf, const char *path);
void       meta_file_del(Meta_File *mf);
void       meta_file_save(Meta_File *mf);

#endif