#include <Eina.h>
#include <Ecore.h>
#include <Ecore_File.h>
#include <Efreet.h>
#include <Efreet_Mime.h>
#include <Eet.h>

#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <fcntl.h>

#include "meta.h"
#include "status.h"
#include "fs.h"
#include "esc.h"
#include "sha.h"

static const char *config_dir = NULL;

typedef struct _File_Set
{
  const char *src, *dst;
} File_Set;

static void
mem_abort(void)
{
  fprintf(stderr, "Out of memory!\n");
  abort();
}

static Eina_List *
file_set_add(Eina_List *files, const char *src, const char *dst)
{
  File_Set *fs = calloc(1, sizeof(File_Set));
  if (!fs) mem_abort();
  fs->src = eina_stringshare_add(src);
  fs->dst = eina_stringshare_add(dst);
  if ((!fs->src) || (!fs->dst)) mem_abort();
  files = eina_list_append(files, fs);
  return files;
}

int
main(int argc EINA_UNUSED, char **argv EINA_UNUSED)
{ // mv [src] [dstdir]
  const char  *fname, *home_dir;
  Eina_Strbuf *buf   = NULL;
  Eina_List   *files = NULL, *l;
  File_Set    *fs;
  size_t       sz;
  char        *src = NULL, *srctmpdir = NULL, *srcdir = NULL;
  char        *dst = NULL, *dsttmpdir = NULL, *dsttmpfile = NULL;
  char         sbuf[PATH_MAX + 256];
  int          ret;
  struct stat  st;
  mode_t       prev_umask;
  Eina_Bool    src_can_write, dst_can_write;

  eina_init();
  eet_init();
  ecore_init();
  efreet_init();

  for (;;)
    { // read stdin of key=val\n ... or end\n
      if (!fgets(sbuf, sizeof(sbuf), stdin)) break;
      sz = strlen(sbuf);
      if (sz < 1) break;                             // too small
      if (sbuf[sz - 1] == '\n') sbuf[sz - 1] = '\0'; // remove \n if there
      if (!strncmp(sbuf, "src=", 4))
        {
          if (src) free(src);
          src = unescape(sbuf + 4);
        }
      else if (!strncmp(sbuf, "dst=", 4))
        {
          if (dst) free(dst);
          dst = unescape(sbuf + 4);
          if ((src) && (dst)) files = file_set_add(files, src, dst);
          if (src) free(src);
          if (dst) free(dst);
          src = NULL;
          dst = NULL;
        }
      else if (!strcmp(sbuf, "end")) break;
    }
  if (src) free(src);
  if (dst) free(dst);
  src = dst = NULL;

  config_dir = getenv("E_HOME_DIR");
  home_dir   = getenv("HOME");
  if (!home_dir) return 77; // no $HOME? definitely an error!
  if (!config_dir)
    {
      snprintf(sbuf, sizeof(sbuf), "%s/.e/e", home_dir);
      config_dir = eina_stringshare_add(sbuf);
    }
  meta_init(config_dir);

  buf = eina_strbuf_new();
  if (!buf) goto err;

  // set up status files for the op
  status_begin();
  status_op("cp");

  // build up a list of all files to mv and scan them to find how much
  EINA_LIST_FOREACH(files, l, fs)
  {
    struct stat stsrc, stdst;

    if (strlen(fs->src) < 1) goto err2;
    if (lstat(fs->src, &stsrc) != 0) break;
    if (lstat(fs->dst, &stdst) != 0) break;
    if (stsrc.st_dev == stdst.st_dev) status_count(1, fs->src);
    else if (!fs_scan(fs->src)) goto err2;
  }

  EINA_LIST_FOREACH(files, l, fs)
  {
    fname = ecore_file_file_get(fs->src);
    if (!fname) break;
    // when monitoring/watching a status file dst dst=xxx allows you to place
    // progress in the right dst dir
    status_dst(fs->dst);
    ret = stat(fs->dst, &st);
    if (ret != 0)
      { // if we can't stat the dstdir then we have issues
        status_error(fs->src, fs->dst, "Cannot stat destination");
        break;
      }

    // free up previous loop stuff
    free(src);
    free(srctmpdir);
    free(dst);
    free(dsttmpdir);
    free(dsttmpfile);
    src = srctmpdir = dst = dsttmpdir = dsttmpfile = NULL;

    // src and src temp dir
    src    = strdup(fs->src);
    srcdir = ecore_file_dir_get(fs->src);
    if (!srcdir)
      {
        status_error(fs->src, fs->dst, "Cannot get source dir");
        break;
      }
    eina_strbuf_reset(buf);
    eina_strbuf_append(buf, srcdir);
    eina_strbuf_append(buf, "/.efm");
    srctmpdir = strdup(eina_strbuf_string_get(buf));
    free(srcdir);

    // final dest file path - can we write to the meta there?
    eina_strbuf_reset(buf);
    eina_strbuf_append(buf, fs->dst);
    eina_strbuf_append(buf, "/");
    eina_strbuf_append(buf, fname);
    dst = strdup(eina_strbuf_string_get(buf));
    eina_strbuf_reset(buf);
    eina_strbuf_append(buf, fs->dst);
    eina_strbuf_append(buf, "/.efm");
    dsttmpdir = strdup(eina_strbuf_string_get(buf));
    eina_strbuf_reset(buf);
    eina_strbuf_append(buf, fs->dst);
    eina_strbuf_append(buf, "/.efm/");
    eina_strbuf_append(buf, fname);
    eina_strbuf_append(buf, ".tmp");
    dsttmpfile = strdup(eina_strbuf_string_get(buf));
    if ((!src) || (!srctmpdir) || (!dst) || (!dsttmpdir) || (!dsttmpfile))
      mem_abort();

    // can we modify the meta files in the src? (like mv/rm them?)
    src_can_write = meta_path_can_write(src);
    dst_can_write = meta_path_can_write(dst);

    // mkdir the /dstdir/.efm dir in the dest as we'll use it for tmp files
    prev_umask = umask(0);
    // make the dir using the same mode as the parent
    ret = mkdir(dsttmpdir, st.st_mode);
    if ((ret != 0) && (errno != EEXIST))
      {
        status_error(fs->src, eina_strbuf_string_get(buf),
                     "Cannot create directory");
        break;
      }
    umask(prev_umask);

    // mv the file to a tmp file: mv /srcdir/filename /dstdir/.efm/filename.tmp
    if (fs_cp(src, dsttmpfile, EINA_TRUE))
      { // it worked so deal with meta/thumbs
        char *src_meta, *dst_meta;
        Meta_File *mfsrc, *mfdst;

        mfsrc = meta_file_load(src);
        if (mfsrc)
          {
            mfdst = meta_file_copy(mfsrc, dst);
            if (mfdst)
              {
                meta_file_save(mfdst);
                meta_file_free(mfdst);
              }
            meta_file_free(mfsrc);
          }

        // thumbnail file for the base target file
        if (src_can_write) src_meta = meta_path_find(src, "thumb.efm");
        else src_meta = meta_path_user_find(src, "thumb.efm");
        if (dst_can_write) dst_meta = meta_path_find(dst, "thumb.efm");
        else dst_meta = meta_path_user_find(dst, "thumb.efm");

        if ((src_meta) && (dst_meta) && (meta_path_prepare(dst)))
          {
            status_count(1, src_meta);
            fs_cp(src_meta, dst_meta, EINA_FALSE);
            // fix up the oprig stat info to match so thub is valid
            // get stat info of new mv'd file
            if (stat(dsttmpfile, &st) == 0)
              {
                Eet_File     *ef;
                unsigned char statsha1[21];

                // sha1 the stat info
                statsha1[20] = 0;
                sha1_stat(&st, statsha1);
                // open the thumb for read+write
                ef = eet_open(dst_meta, EET_FILE_MODE_READ_WRITE);
                // update orig path just to be nice - not used for validity
                eet_write(ef, "orig/path", dst, strlen(dst),
                          EET_COMPRESSION_LOW);
                // write new stat info so it's valid
                eet_write(ef, "orig/stat/sha1", statsha1, 20,
                          EET_COMPRESSION_NONE);
                eet_close(ef);
              }
          }
        free(src_meta);
        free(dst_meta);

        // rename atomically the tmp mv'd file to its dest now thumb and meta
        // are ok/valid. i.e.
        // mv /dstdir/.efm/filename.tmp /distdir/filename
        ret = rename(dsttmpfile, dst);
        if (ret != 0)
          {
            status_error(dsttmpfile, dst, "Cannot rename");
            break;
          }
      }
    else break;
    // it all worked - let's clean up possibly empty tmp/efm dirs
    rmdir(dsttmpdir);
  }
err2:
  status_end();
  // free up leftover strings from loop
  free(src);
  free(srctmpdir);
  free(dst);
  free(dsttmpdir);
  free(dsttmpfile);

err:
  if (buf) eina_strbuf_free(buf);
  EINA_LIST_FREE(files, fs)
  {
    eina_stringshare_del(fs->src);
    eina_stringshare_del(fs->dst);
    free(fs);
  }

  meta_shutdown();

  efreet_shutdown();
  ecore_shutdown();
  eet_shutdown();
  eina_shutdown();
  return 0;
}