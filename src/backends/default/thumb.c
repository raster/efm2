#include "thumb.h"
#include "util.h"

Evas_Object *win    = NULL;
Evas_Object *subwin = NULL;
Evas_Object *image  = NULL;

static void
_thumb_dir_make(const char *thumb)
{ // create dir for the thumb to live in
  char *dir = ecore_file_dir_get(thumb);
  if (!dir) return;
  ecore_file_mkpath(dir);
  free(dir);
}

static Eet_File *
_thumb_output_open(const char *thumb)
{ // open target thumb tmp file we will later atomically replace target with
  char buf[PATH_MAX];

  snprintf(buf, sizeof(buf), "%s.tmp", thumb);
  return eet_open(buf, EET_FILE_MODE_WRITE);
}

static void
_thumb_output_close(Eet_File *ef, const char *thumb)
{ // close thumnb file and atomically rename tmp file on top of target
  char buf[PATH_MAX];

  eet_close(ef);
  snprintf(buf, sizeof(buf), "%s.tmp", thumb);
  // fix permissions on file to match parent dir
  util_file_mode_parent_copy(buf, EINA_FALSE);
  ecore_file_mv(buf, thumb);
}

EAPI_MAIN int
elm_main(int argc, char **argv)
{
  Eet_File     *ef;
  const char   *path, *mime, *thumb;
  int           ret;
  struct stat   st;
  unsigned char statsha1[20];

  if (argc < 4) return 100;

  path  = argv[1];
  mime  = argv[2];
  thumb = argv[3];

  // set up buffer win/canvas with sub win rendered inline as image
  elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
  elm_config_preferred_engine_set("buffer");
  win    = elm_win_add(NULL, "Efm-Thumb", ELM_WIN_BASIC);
  subwin = elm_win_add(win, "inlined", ELM_WIN_INLINED_IMAGE);
  elm_win_alpha_set(subwin, EINA_TRUE);
  image = elm_win_inlined_image_object_get(subwin);
  evas_object_show(subwin);
  evas_object_show(win);
  // manual rendering as we won't run the loop - only render then get results
  elm_win_norender_push(subwin);
  elm_win_norender_push(win);

  // stat orig file and store the state info we care about as a sha1 hash
  if (stat(path, &st) != 0) exit(1);
  sha1_stat(&st, statsha1);

  // ensure dest dir for thumb exists
  _thumb_dir_make(thumb);
  // open our thubm file ti write thnigs into it
  ef = _thumb_output_open(thumb);
  if (!ef) exit(3);

  // an edj file (theme, background, icon .... groups inside matter
  if (check_thumb_edje(path, mime)) ret = thumb_edje(ef, path, mime, thumb);
  // if it's a font - load it and render some text as thumb/preview
  else if (check_thumb_font(path, mime))
    ret = thumb_font(ef, path, mime, thumb);
  // a paged document - load multiple pages into thumb
  else if (check_thumb_paged(path, mime))
    ret = thumb_paged(ef, path, mime, thumb);
  // a music track/file
  else if (check_thumb_music(path, mime))
    ret = thumb_music(ef, path, mime, thumb);
  // a video file of a moive, series episode or something else
  else if (check_thumb_video(path, mime))
    ret = thumb_video(ef, path, mime, thumb);
  // otherwise handle as an image
  else ret = thumb_image(ef, path, mime, thumb);

  // write out the original file path so we could walk through all thumbs
  // and find which thumbs no longer have an original file left
  eet_write(ef, "orig/path", path, strlen(path), EET_COMPRESSION_LOW);
  // write out our sha1 of the file stat info - quick and mostly right
  // way to check if the thumb is up to date with file
  eet_write(ef, "orig/stat/sha1", statsha1, 20, EET_COMPRESSION_NONE);
  // done - finish file write and atomic rename
  _thumb_output_close(ef, thumb);

  // if we failed to generate the thumb - delete what we were building
  if (ret != 0) unlink(thumb);

  return ret;
}
ELM_MAIN()
