// generate thumbnail files for fonts
#include "thumb.h"

static Evas_Object *
_thumb_text_setup(const char *file, const char *str, int maxw, int maxh,
                  int size, int *size_chosen)
{
  Evas_Object *o;
  Evas_Coord   w, h;
  int          sizeup, sizedn, psize;

  o = evas_object_text_add(evas_object_evas_get(subwin));
  evas_object_color_set(o, 255, 255, 255, 255);
  evas_object_text_text_set(o, str);
  if (size == 0) evas_object_text_font_set(o, file, maxh);
  else evas_object_text_font_set(o, file, size);
  evas_object_show(o);
  if (size == 0)
    { // auto find a size that fits in maxw/maxh
      size   = maxh;
      sizeup = size * 2;
      sizedn = 1;
      psize  = size + 100; // dummy psize for first loop
      for (;;)
        {
          evas_object_geometry_get(o, NULL, NULL, &w, &h);
          if ((w <= maxw) && (h <= maxh))
            {                                    // it fits
              if (abs(size - psize) <= 1) break; // we found the best size
              psize  = size;
              size   = (size + sizeup) / 2;
              sizedn = psize;
            }
          else
            {
              if (abs(size - psize) <= 1) break; // we found the best size
              psize  = size;
              size   = (size + sizedn) / 2;
              sizeup = psize;
            }
          evas_object_text_font_set(o, file, size);
        }
    }
  *size_chosen = size;
  return o;
}

static void
_thumb_font_2_line(const char *str1, const char *str2, Eet_File *ef,
                   const char *path, int szw, int szh, const char *key)
{
  Evas_Object *tx1, *tx2;
  int          sz1, sz2, w, h;

  tx1 = _thumb_text_setup(path, str1, szw, szh / 2, 0, &sz1);
  tx2 = _thumb_text_setup(path, str2, szw, szh / 2, 0, &sz2);
  if (sz1 < sz2)
    {
      evas_object_del(tx2);
      sz2 = sz1;
      tx2 = _thumb_text_setup(path, str2, szw, szh / 2, sz2, &sz2);
    }
  else if (sz2 < sz1)
    {
      evas_object_del(tx1);
      sz1 = sz2;
      tx1 = _thumb_text_setup(path, str1, szw, szh / 2, sz1, &sz1);
    }
  evas_object_geometry_get(tx1, NULL, NULL, &w, &h);
  evas_object_move(tx1, (szw - w) / 2, ((szh / 2) - h) / 2);
  evas_object_geometry_get(tx2, NULL, NULL, &w, &h);
  evas_object_move(tx2, (szw - w) / 2, (szh / 2) + (((szh / 2) - h) / 2));
  evas_object_resize(subwin, szw, szh);
  elm_win_render(subwin);
  thumb_image_write(ef, key, image, EINA_TRUE, EINA_FALSE);
  evas_object_del(tx1);
  evas_object_del(tx2);
}

int
thumb_font(Eet_File *ef, const char *path, const char *mime EINA_UNUSED,
           const char *thumb EINA_UNUSED)
{
  const int sizes[] = { 512, 256, 128, 64, 32, 16, 0 };
  char      buf[128];
  int       i;
  char      one = 1;

  // XXX: we don't handle any font load errors - eg 0 sized
  for (i = 0; sizes[i] != 0; i++)
    {
      snprintf(buf, sizeof(buf), "image/thumb/%i", sizes[i]);
      _thumb_font_2_line("ABC", "def", ef, path, sizes[i], sizes[i], buf);
    }
  _thumb_font_2_line("Lorem ipsum dolor sit amet 012/789 #!?",
                     "日本語 にほんご ソフト 中文 華語 한국", ef, path, 2048,
                     256, "image/preview");
  eet_write(ef, "image/thumb/mono", &one, 1, EET_COMPRESSION_NONE);
  return 0;
}
