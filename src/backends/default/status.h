#ifndef STATUS_H
#define STATUS_H
#include <Eina.h>

// status reporting writes status to a log file USERRUNTIME/efm/ops/PID.status
// where USERRUNTIME might for example be /var/run/user/UID .
//
// this will be rate limited and thus not write a line for every change but
// the last line or few lines in the file will be the latest status with a
// small time lag so can be relied on to monitor and read new lines from
// as they get appended and then display as needed. if everything succeeds
// and the op completes the file will be delted. if there is an error, the
// file is left there with the last few lines having some error information
// so this can be displayed to the user
//
// lines in the status file are of the format
// CMD command [arg1] [arg2] ...
// where the first 4 chars are "CMD " and then a non-whitespace containing
// command string, then possibly a space char and then arguments until a
// a newline "\n" char for end of line. arguments mabay just be a set of
// whitespace delimited words (so a string until the newline) or perhaps
// of the form key=value depending on the command

void status_begin(void);
void status_end(void);
void status_op(const char *op);
void status_dst(const char *dst);
void status_count(unsigned long long num_inc, const char *str);
void status_pos(unsigned long long pos_inc, const char *str);
// this call will cause the status thread to report the error then exit(1)
void status_error(const char *src, const char *dst, const char *str);

#endif