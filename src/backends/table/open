#!/bin/bash

##############################################################################
# Global state for this backend everything needs access to
##############################################################################
DIR=""

##############################################################################
# Helper functions useful for everyone
##############################################################################

# Print an error string to stderr
# e.g. e_err "This is an error"
function e_err() {
  >&2 echo "$@"
}

# take escaped str 2nd arg and produce non-escaped str in 1st arg var
# like "boop%5f%5f%3d%3d%4073x" -> "boop__==@73x"
# e.g. e_val_unescape VAR "boop%5f%5f%3d%3d%4073x"
#      echo $VAR
#      boop__==@73x
function e_val_unescape() {
  local -n V=$1
  read <<< ${2}
  V=$( echo -e ${REPLY//%/\\x} );
}

# take normal str 2nd arg and produce escaped str in 1st arg var
# like "boop__==@73x" -> "boop%5f%5f%3d%3d%4073x"
# e.g. e_val_unescape VAR "boop__==@73x"
#      echo $VAR
#      boop%5f%5f%3d%3d%4073x
function e_val_escape() {
  local -n V=$1
  local STR=${2}
  local STRLEN=${#STR}
  local NEWSTR=""
  local I CHR OUT
  for (( I=0; I<$STRLEN; I++ )); do
    CHR=${STR:$I:1}
    case "$CHR" in
      [-/.a-zA-Z0-9] ) OUT="${CHR}" ;;
                   * ) printf -v OUT '%%%02x' "'$CHR"
    esac
    NEWSTR+="${OUT}"
  done
  V=${NEWSTR}
}

# read a line on stdin - check it starts with CMD if there reutrn 0 (success)
# and if it's not, return an error (non-zero). line is broken into an array
# delimted by spaces so it'll be: (command, arg1=x, arg2=y, arg3=z) for
# example.
function e_line_read() {
  local -n V=${1}
  local PIFS="$IFS"
  local C
  IFS=" "
  read -a V
  IFS="$PIFS"
# check command line starts with CMD
  C=${V[0]}
  if test -z "$C"; then return 1; fi
  if [ ${C} != "CMD" ]; then return 1; fi
  return 0
}

# produce proper command line back to efm - all command sstart with "CMD "
function e_cmd() {
  echo "CMD $@"
}

# loop forever handling input cmd son stdin from efm
function e_loop() {
  local CMD ARGS ARGSLEN NEWARGS PIFS KEY VAL
  while [ 1 ]; do
    if e_line_read LINE; then
      CMD=${LINE[1]}
      ARGS=("${LINE[@]:2}")
      ARGSLEN=${#LINE[@]}
      NEWARGS=()
      for (( I=0; I<$ARGSLEN; I++ )); do
        PIFS="$IFS"; IFS="="
        read -a A <<< ${ARGS[$I]}
        IFS="$PIFS"
        KEY=${A[0]}
        if test -n "$KEY"; then
          e_val_unescape VAL ${A[1]}
          NEWARGS+=(${KEY})
          PIFS="$IFS"; IFS=""
          NEWARGS+=(${VAL})
          IFS="$PIFS"
        fi
      done
      handle_cmd NEWARGS "$CMD"
    fi
  done
}

##############################################################################
# FS backend code specific to this backend
##############################################################################

function handle_cmd_dir_set() {
  local F M D
# force detailed view mode
  e_cmd "viewmode-set mode=list_detailed"
# explicitly set column header strings
  e_cmd "detail-header-set col=0 label=nomnom"
  e_cmd "detail-header-set col=1 size=60 label=h-one"
  e_cmd "detail-header-set col=2 size=70 label=h-two"
  e_cmd "detail-header-set col=3 size=80 label=h-three"
  e_cmd "detail-header-set col=4 size=90 label=h-four"
  e_cmd "detail-header-set col=5 size=130 label=h-five"
  e_cmd "detail-header-set col=6 size=90 label=h-six"

# test timers
  e_cmd "timer-add name=ONE-OFF delay=1500"
  e_cmd "timer-add name=REPEAT-800ms repeat=true delay=800"
# begin initial listing of files
  e_cmd "list-begin"
# define some params we're going to use
# an icon image to use
  e_val_escape M ${DIR}"ic.jpg"
  M="type=file icon="${M}
# add one new file (4 of them) with params

  D="detail-format=text,popdown,progress,checkview,button,size"
  e_val_escape F ${DIR}"abba"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=one detail2=Opt1!dance.gif,*Opt2!std:clock detail3=33/100 detail4=1 detail5=Click!dance.gif detail6=576/65536"

  D="detail-format=text,popdown,progress,checkview,button,size"
  e_val_escape F ${DIR}"boopy__==!@#$%^&*();"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=one detail2=Opt1,Opt2,*Opt3,Opt4 detail3=noval,55/100 detail4=0 detail5=Pressme!std:security-high detail6=16384/65536"

  D="detail-format=icon,check,circle,segmentsel,timestamp,slider"
  e_val_escape F ${DIR}"g h i"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=std:dialog-error detail2=true detail3=cc::selected detail4=*!dance.gif,!std:audio-card,!std:camera detail5=1696322215 detail6=0/100/33"

  D="detail-format=icon,check,circle,segmentsel,timestamp,slider"
  e_val_escape F ${DIR}"y~"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=dance.gif detail2=false detail3=#ff665580 detail4=Option1,Opt2,*O3 detail5=1296332215 detail6=50/200/175"

  D="detail-format=icon,text,circle,text,slider,size"
  e_val_escape F ${DIR}"zz blah blah blah"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=std:utilities-terminal detail2=Textgoesherethatislong detail3=#fec746 detail4=XXX detail5=0/10/2 detail6=32768/65536"

  D="detail-format=icon,text,circle,text,slider,size"
  e_val_escape F ${DIR}"zzz smelly fish"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=std:software-update-available detail2=hello%20world detail3=cc:/fg/normal/desklock/fprint/success detail4=Booya detail5=0/20/5/15 detail6=4096/65536"

  D="detail-format=text,text,text,text,bargraph,bargraph"
  e_val_escape F ${DIR}"zzzz1"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=0-100,50,70,80,100,15,30,70,65,62,61,55,43,10,14,17,50 detail6=#ff443388,0-10,3,4,0,4,10,8|#ffee3388,0-10,1,5,9,10,7,6"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz2"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz3"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz4"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz5"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz6"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz7"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"

  D="detail-format=text,text,text,text,text,text"
  e_val_escape F ${DIR}"zzzz8"
  e_cmd "file-add path="${F}" "${M}" "${D}\
    " detail1=a detail2=b detail3=c detail4=d detail5=e detail6=f"
# end initial listing of files
  e_cmd "list-end"
}

##############################################################################
# This function must be provided by fs backend given above shared e_ script
##############################################################################
function handle_cmd() {
  local -n ARGS=${1}
  local ARGSLEN=${#ARGS[@]}
  local CMD

  CMD=${2}
  case "$CMD" in
    # handle command to set the dir to scan/look at
    dir-set )
      if [ ${ARGS[0]} = "path" ]; then
        DIR=${ARGS[1]}
        handle_cmd_dir_set ${DIR}
      fi
      ;;
    timer )
      if [ ${ARGS[0]} = "name" ]; then
        NAME=${ARGS[1]}
        e_err "timer :$NAME:"
        if [ ${NAME} = "REPEAT-800ms" ]; then
          e_val_escape F ${DIR}"abba"
          R=$[ $RANDOM * 100 ];
          R=$[ $R / 32767 ];
          e_cmd "file-detail path="${F}" detail3="${R}"/100"
        fi
      fi
      ;;
    # commands this fs doesn;'t handle (yet?)
    meta-set )
      ;;
    cnp-cut )
      ;;
    cnp-copy )
      ;;
    cnp-paste )
      ;;
    dnd-drag-begin )
      ;;
    dnd-drag-end )
      ;;
    dnd-drop )
      ;;
    dnd-hover )
      ;;
    file-run )
      ;;
    file-clicked )
      ;;
    file-selected )
      ;;
    file-unselected )
      ;;
    file-delete )
      ;;
    file-rename )
      ;;
    file-detail-change )
      e_err "file-detail-change" ${ARGS[0]} ${ARGS[1]} ${ARGS[2]} ${ARGS[3]} ${ARGS[4]} ${ARGS[5]} ${ARGS[6]} ${ARGS[7]}
      ;;
    file-detail-clicked )
      e_err "file-detail-clicked" ${ARGS[0]} ${ARGS[1]} ${ARGS[2]} ${ARGS[3]}
      ;;
    file-detail-select )
      e_err "file-detail-select" ${ARGS[0]} ${ARGS[1]} ${ARGS[2]} ${ARGS[3]} ${ARGS[4]} ${ARGS[5]}
      ;;
  esac
}

# loop parsing input lines
e_loop
