// the efm view - intended to be put in an elm scroller - though could
// work without and display a set of file icons in some view mode and
// allow them to update, sort, drag and drop, rename, delete, copy/paste,
// etc. etc.
//
// maximum # of files in a dir that is sane: 10,000
// maximum number of files in a dir on a real system to be fast with: 3,000
#include "Edje.h"
#include "cmd.h"
#include "efm.h"
#include "efl_ui_check_eo.legacy.h"
#include "efm_icon.h"
#include "efm_util.h"
#include "efm_dnd.h"
#include "efm_back_end.h"
#include "efm_custom.h"
#include "efm_private.h"
#include "eina_types.h"
#include "elm_table_eo.legacy.h"

int _log_dom = -1;

Eina_List *_efm_list         = NULL;
Eina_List *_pending_exe_dels = NULL;

static Evas_Smart      *_smart     = NULL;
static Evas_Smart_Class _sc        = EVAS_SMART_CLASS_INIT_NULL;
static Evas_Smart_Class _sc_parent = EVAS_SMART_CLASS_INIT_NULL;

#define ENTRY                                       \
  Smart_Data *sd = evas_object_smart_data_get(obj); \
  if (!sd) return

void
_cb_header_change(void *data)
{
  Smart_Data *sd = data;

  sd->header_change_job = NULL;
  evas_object_smart_callback_call(sd->o_smart, "header_change", NULL);
}

static void
_cb_lost_selection(void *data, Elm_Sel_Type selection EINA_UNUSED)
{
  Smart_Data *sd = evas_object_smart_data_get(data);

  if (sd->cnp_have)
    {
      sd->cnp_have = EINA_FALSE;
      sd->cnp_cut  = EINA_FALSE;
      printf("XXX: lost select\n");
    }
}

static void
_cnp_copy_files(Smart_Data *sd)
{
  Eina_Strbuf *strbuf;
  const char  *str = NULL;

  strbuf = eina_strbuf_new();
  if (!strbuf) return;
  if (_selected_icons_uri_strbuf_append(sd, strbuf))
    {
      str = eina_strbuf_string_get(strbuf);
      if (str)
        {
          elm_cnp_selection_set(sd->o_scroller, ELM_SEL_TYPE_CLIPBOARD,
                                ELM_SEL_FORMAT_URILIST, str, strlen(str));
          elm_cnp_selection_loss_callback_set(sd->o_scroller,
                                              ELM_SEL_TYPE_CLIPBOARD,
                                              _cb_lost_selection, sd->o_smart);
        }
    }
  if (sd->cnp_cut)
    {
      Eina_Strbuf *buf = cmd_strbuf_new("cnp-cut");

      if (str) printf("XXX: CUT: [%s]\n", str);
      else printf("XXX: CUT: no str\n");
      if (buf)
        {
          _uri_list_cmd_strbuf_append(buf, "path", str);
          cmd_strbuf_exe_consume(buf, sd->exe_open);
        }
    }
  else
    {
      Eina_Strbuf *buf = cmd_strbuf_new("cnp-copy");

      if (str) printf("XXX: COPY: [%s]\n", str);
      else printf("XXX: COPY: no str\n");
      if (buf)
        {
          _uri_list_cmd_strbuf_append(buf, "path", str);
          cmd_strbuf_exe_consume(buf, sd->exe_open);
        }
    }
  eina_strbuf_free(strbuf);
}

static Eina_Bool
_cb_sel_get(void *data, Evas_Object *_obj EINA_UNUSED, Elm_Selection_Data *ev)
{
  Smart_Data *sd = evas_object_smart_data_get(data);

  if (!sd) return EINA_TRUE;
  printf("XXX: GET SEL %i\n", ev->format);
  if (ev->format & ELM_SEL_FORMAT_URILIST)
    {
      char **plist, **p, *esc, *tmp;

      tmp = malloc(ev->len + 1);
      if (tmp)
        {
          Eina_Strbuf *buf = cmd_strbuf_new("cnp-paste");

          memcpy(tmp, ev->data, ev->len);
          tmp[ev->len] = 0;
          plist        = eina_str_split(tmp, "\n", -1);
          for (p = plist; *p != NULL; p++)
            {
              if (**p)
                {
                  esc = _escape_parse(*p);
                  if (!esc) continue;
                  printf("XXX: PASTE FILE: [%s]\n", esc);
                }
            }
          free(*plist);
          free(plist);
          switch (ev->action)
            { // reality is we will not get an action in the selection
            case ELM_XDND_ACTION_COPY:
              cmd_strbuf_append(buf, "action", "copy");
              break;
            case ELM_XDND_ACTION_MOVE:
              cmd_strbuf_append(buf, "action", "move");
              break;
            case ELM_XDND_ACTION_ASK:
              cmd_strbuf_append(buf, "action", "ask");
              break;
            case ELM_XDND_ACTION_LIST:
              cmd_strbuf_append(buf, "action", "list");
              break;
            case ELM_XDND_ACTION_LINK:
              cmd_strbuf_append(buf, "action", "link");
              break;
            case ELM_XDND_ACTION_DESCRIPTION:
              cmd_strbuf_append(buf, "action", "description");
              break;
            default:
              break;
            }
          if (buf)
            {
              _uri_list_cmd_strbuf_append(buf, "path", tmp);
              cmd_strbuf_exe_consume(buf, sd->exe_open);
            }
          free(tmp);
        }
    }
  return EINA_TRUE;
}

static void
_cnp_paste_files(Smart_Data *sd)
{
  elm_cnp_selection_get(sd->o_scroller, ELM_SEL_TYPE_CLIPBOARD,
                        ELM_SEL_FORMAT_URILIST, _cb_sel_get, sd->o_smart);
}

static void
_cb_key_down(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
             void *event_info)
{
  Smart_Data          *sd      = data;
  Evas_Event_Key_Down *ev      = event_info;
  Eina_Bool            handled = EINA_FALSE;

  // these keys we use/steal for navigation, so want them for sure
  if (!strcmp(ev->key, "Up")) handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_UP);
  else if (!strcmp(ev->key, "Down"))
    handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_DOWN);
  else if (!strcmp(ev->key, "Left"))
    handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_LEFT);
  else if (!strcmp(ev->key, "Right"))
    handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_RIGHT);
  else if (!strcmp(ev->key, "Prior"))
    handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_PGUP);
  else if (!strcmp(ev->key, "Next"))
    handled = _icon_focus_dir(sd, EFM_FOCUS_DIR_PGDN);
  else if ((!strcmp(ev->key, "Return")) || (!strcmp(ev->key, "KP_Enter")))
    {
      if (sd->last_focused)
        {
          Icon        *icon   = sd->last_focused;
          Eina_Strbuf *strbuf = eina_strbuf_new();

          if (!icon->selected) _icon_select(icon);

          if (strbuf)
            {
              if (_selected_icons_uri_strbuf_append(sd, strbuf))
                {
                  Eina_Strbuf *buf = cmd_strbuf_new("file-run");

                  _icon_open_with_cmd_strbuf_append(buf, "open-with", sd);
                  _uri_list_cmd_strbuf_append(buf, "path",
                                              eina_strbuf_string_get(strbuf));
                  cmd_strbuf_exe_consume(buf, sd->exe_open);
                }
              eina_strbuf_free(strbuf);
            }
          handled = EINA_TRUE;
        }
    }
  else if (!strcmp(ev->key, "space"))
    {
      if (sd->last_focused)
        {
          Icon *icon = sd->last_focused;

          if (evas_key_modifier_is_set(ev->modifiers, "Shift"))
            { // range select
              if (!icon->selected)
                {
                  if (icon->sd->last_selected)
                    _select_range(icon->sd->last_selected, icon);
                  else _icon_select(icon);
                }
            }
          else if (evas_key_modifier_is_set(ev->modifiers, "Control"))
            { // multi-single select toggle
              if (!icon->selected) _icon_select(icon);
              else _icon_unselect(icon);
            }
          else
            { // select just one file so unselect previous files
              _unselect_all(icon->sd);
              if (!icon->selected) _icon_select(icon);
              else _icon_unselect(icon);
            }
          handled = EINA_TRUE;
        }
    }
  else if (!strcmp(ev->key, "Escape"))
    {
      handled = _unselect_all(sd);
    }
  // XXX: hmm - should we handle this?
  //   else if (!strcmp(ev->key, "Backspace"))
  //     {
  //     }
  else if (!strcmp(ev->key, "Delete"))
    {
      Eina_Strbuf *strbuf = eina_strbuf_new();

      if (strbuf)
        {
          if (_selected_icons_uri_strbuf_append(sd, strbuf))
            {
              Eina_Strbuf *buf = cmd_strbuf_new("file-delete");

              _uri_list_cmd_strbuf_append(buf, "path",
                                          eina_strbuf_string_get(strbuf));
              cmd_strbuf_exe_consume(buf, sd->exe_open);
            }
          eina_strbuf_free(strbuf);
        }
      handled = EINA_TRUE;
    }
  else if (!strcmp(ev->key, "Home"))
    {
      if (sd->icons)
        {
          sd->last_focused_before = sd->last_focused;
          sd->last_focused        = sd->icons->data;
          _icon_focus(sd);
          handled = EINA_TRUE;
        }
    }
  else if (!strcmp(ev->key, "End"))
    {
      if (sd->icons)
        {
          sd->last_focused_before = sd->last_focused;
          sd->last_focused        = eina_list_last(sd->icons)->data;
          _icon_focus(sd);
          handled = EINA_TRUE;
        }
    }
  else if ((!strcmp(ev->key, "c"))
           && (evas_key_modifier_is_set(ev->modifiers, "Control")))
    {
      sd->cnp_have = EINA_TRUE;
      sd->cnp_cut  = EINA_FALSE;
      _cnp_copy_files(sd);
      handled = EINA_TRUE;
    }
  else if ((!strcmp(ev->key, "x"))
           && (evas_key_modifier_is_set(ev->modifiers, "Control")))
    {
      sd->cnp_have = EINA_TRUE;
      sd->cnp_cut  = EINA_TRUE;
      _cnp_copy_files(sd);
      handled = EINA_TRUE;
    }
  else if ((!strcmp(ev->key, "v"))
           && (evas_key_modifier_is_set(ev->modifiers, "Control")))
    {
      _cnp_paste_files(sd);
      handled = EINA_TRUE;
    }
  else if ((!strcmp(ev->key, "Insert"))
           && (evas_key_modifier_is_set(ev->modifiers, "Shift")))
    {
      _cnp_paste_files(sd);
      handled = EINA_TRUE;
    }
  // flag on hold if we use the  event - otherwise pass it on
  if (handled)
    {
      ev->event_flags |= EVAS_EVENT_FLAG_ON_HOLD;
      sd->key_control = EINA_TRUE;
    }
  // XXX: if we're dnding - modify dnd action based on shift, ctrl etc.
  printf("XXX: KEY: [%c] [%s]\n", handled ? '#' : ' ', ev->key);
}

static void
_cb_refocus(void *data)
{
  Smart_Data *sd = data;

  sd->refocus_job = NULL;
  if (sd->rename_icon) return;
  //  printf("XXX: REFOCUS\n");
  if (sd->focused)
    {
      //      printf("XXX:  focus widget\n");
      evas_object_focus_set(sd->o_clip, EINA_TRUE);
      if (sd->key_control) _icon_focus_show(sd);
    }
  else
    {
      //      printf("XXX:  unfocused widget\n");
      _icon_focus_hide(sd);
      // XXX: exit rename mode if on...
    }
}

static void
_cb_focus_out(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
              void *event_info EINA_UNUSED)
{
  Smart_Data *sd = data;

  //  printf("XXX: FOCUS OUT CLIP\n");
  if (sd->refocus_job) ecore_job_del(sd->refocus_job);
  sd->refocus_job = ecore_job_add(_cb_refocus, sd);
}

static void
_cb_back_mouse_down(void *data, Evas *e EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED, void *event_info)
{
  Smart_Data            *sd = data;
  Evas_Event_Mouse_Down *ev = event_info;

  if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) return;
  // XXX: exit rename mode if on...
  elm_object_focus_set(sd->o_scroller, EINA_TRUE);
  if (ev->button == 1)
    {
      sd->back_down   = EINA_TRUE;
      sd->back_down_x = ev->canvas.x;
      sd->back_down_y = ev->canvas.y;
      _efm_sel_store_start(sd, ev->canvas.x, ev->canvas.y);
    }
  else if (ev->button == 3)
    {
      sd->back3_down   = EINA_TRUE;
      sd->back3_down_x = ev->canvas.x;
      sd->back3_down_y = ev->canvas.y;
    }
  if (sd->rename_icon) _icon_rename_end(sd->rename_icon);
  _icon_focus_hide(sd);
}

static void
_cb_back_mouse_up(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                  void *event_info)
{
  Smart_Data          *sd = data;
  Evas_Event_Mouse_Up *ev = event_info;
  Evas_Coord           dx, dy;
  Eina_Bool            dragged = EINA_FALSE;

  if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) return;
  if (ev->button == 1)
    {
      // XXX: kill longpress timer
      sd->back_down = EINA_FALSE;
      _icon_focus_hide(sd);
      if (!sd->sel_show)
        {
          dx = ev->canvas.x - sd->back_down_x;
          dy = ev->canvas.y - sd->back_down_y;
          if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
          if (!dragged)
            {
              if ((!evas_key_modifier_is_set(ev->modifiers, "Shift"))
                  && (!evas_key_modifier_is_set(ev->modifiers, "Control")))
                _unselect_all(sd);
            }
        }
      else _efm_sel_end(sd);
    }
  else if (ev->button == 3)
    { // right mouse click
      printf("XXX: right mouse back\n");
      sd->back3_down = EINA_FALSE;
      dx             = ev->canvas.x - sd->back3_down_x;
      dy             = ev->canvas.y - sd->back3_down_y;
      if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
      if (!dragged)
        {
          if ((!evas_key_modifier_is_set(ev->modifiers, "Shift"))
              && (!evas_key_modifier_is_set(ev->modifiers, "Control")))
            {
              _efm_popup_main_menu_add(sd, sd->back3_down_x, sd->back3_down_y);
            }
        }
    }
}

static void
_cb_back_mouse_move(void *data, Evas *e EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED, void *event_info)
{
  Smart_Data            *sd = data;
  Evas_Event_Mouse_Move *ev = event_info;
  Evas_Coord             dx, dy;
  Eina_Bool              dragged = EINA_FALSE;

  if (!sd->back_down) return;
  // XXX: kill longpress timer
  dx = ev->cur.canvas.x - sd->back_down_x;
  dy = ev->cur.canvas.y - sd->back_down_y;
  if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
  if (!dragged) return;
  if ((!evas_key_modifier_is_set(ev->modifiers, "Shift"))
      && (!evas_key_modifier_is_set(ev->modifiers, "Control")))
    _unselect_all(sd);
  if (!sd->sel_show) _efm_sel_start(sd);
  _efm_sel_store_end(sd, ev->cur.canvas.x, ev->cur.canvas.y);
  _efm_sel_position(sd);
}

static void
_reposition_detail_header_items(Smart_Data *sd)
{
  int        i, vw, vh;
  Evas_Coord det_x, det_w, x, w, xp = 0;

  if (!sd->o_detail_header) return;
  if (!sd->o_overlay_grid) return;
  evas_object_geometry_get(sd->o_overlay_grid, &det_x, NULL, &det_w, NULL);
  elm_grid_size_get(sd->o_overlay_grid, &vw, &vh);
  elm_grid_size_set(sd->o_detail_header, vw, 100);
  for (i = 0; i < 7; i++)
    {
      if (i < 6)
        {
          if (!sd->o_list_detail[i]) continue;
          elm_grid_pack_get(sd->o_list_detail[i], &x, NULL, NULL, NULL);
          w = (x - xp);
        }
      else
        {
          w = vw - xp;
          x = xp + w;
        }
      if (!sd->o_detail_header_item[i]) continue;
      elm_grid_pack(sd->o_detail_header, sd->o_detail_header_item[i], xp, 0, w,
                    100);
      xp = x;
    }
}

static void
_reposition_detail_bars(Smart_Data *sd)
{
  int        i;
  Evas_Coord x, w;

  for (i = 0; i < 6; i++)
    {
      if (!sd->o_list_detail_swallow[i]) continue;
      evas_object_geometry_get(sd->o_list_detail_swallow[i], &x, NULL, &w,
                               NULL);
      x -= sd->geom.x;
      if (!sd->o_list_detail[i]) continue;
      elm_grid_pack(sd->o_overlay_grid, sd->o_list_detail[i], x, 0, w, 100);
    }
  _reposition_detail_header_items(sd);
}

void
_redo_detail_sizes(Smart_Data *sd)
{
  Evas_Object *o;
  char         buf[128];
  int          i;

  for (i = 0; i < 6; i++)
    {
      o = sd->o_list_detail_swallow[i];
      snprintf(buf, sizeof(buf), "e.swallow.detail%i", i + 1);
      evas_object_size_hint_min_set(
        o, sd->config.detail_min_w[i] * _scale_get(sd), 0);
      edje_object_part_swallow(sd->o_list_detailed_dummy, buf, o);
    }
  _reposition_detail_bars(sd);
  _detail_realized_items_resize(sd);
}

static void
_cb_overlay_detail_swallow_move(void *data, Evas *e EINA_UNUSED,
                                Evas_Object *obj EINA_UNUSED,
                                void *info       EINA_UNUSED)
{
  _reposition_detail_bars(data);
}

static void
_cb_overlay_detail_swallow_resize(void *data, Evas *e EINA_UNUSED,
                                  Evas_Object *obj EINA_UNUSED,
                                  void *info       EINA_UNUSED)
{
  _reposition_detail_bars(data);
}

static void
_cb_overlay_grid_resize(void *data, Evas *e EINA_UNUSED, Evas_Object *obj,
                        void *info EINA_UNUSED)
{
  Smart_Data *sd = data;
  Evas_Coord  w;

  evas_object_geometry_get(obj, NULL, NULL, &w, NULL);
  elm_grid_size_set(sd->o_overlay_grid, w, 100);
}

static void
_cb_overlay_detail_mouse_down(void *data, Evas *e EINA_UNUSED, Evas_Object *obj,
                              void *info)
{
  Smart_Data            *sd = data;
  Evas_Event_Mouse_Down *ev = info;
  int                    i;

  if (ev->button == 1)
    {
      sd->detail_down   = EINA_TRUE;
      sd->detail_down_x = ev->canvas.x;
      sd->detail_down_y = ev->canvas.y;
      for (i = 0; i < 6; i++)
        {
          if (obj == sd->o_list_detail[i])
            {
              sd->detail_down_start_min_w
                = sd->config.detail_min_w[i] * _scale_get(sd);
              break;
            }
        }
    }
}

static void
_cb_overlay_detail_mouse_up(void *data, Evas *e EINA_UNUSED,
                            Evas_Object *obj EINA_UNUSED, void *info)
{
  Smart_Data          *sd = data;
  Evas_Event_Mouse_Up *ev = info;

  if (ev->button == 1)
    {
      if (sd->detail_down)
        {
          sd->detail_down = EINA_FALSE;
        }
    }
}

static void
_cb_overlay_detail_mouse_move(void *data, Evas *e EINA_UNUSED, Evas_Object *obj,
                              void *info)
{
  Smart_Data            *sd = data;
  Evas_Event_Mouse_Move *ev = info;
  Evas_Object           *o;
  char                   buf[128];
  int                    i;

  if (sd->detail_down)
    {
      for (i = 0; i < 6; i++)
        {
          if (obj == sd->o_list_detail[i])
            {
              o = sd->o_list_detail_swallow[i];
              sd->config.detail_min_w[i]
                = (sd->detail_down_start_min_w
                   - (ev->cur.canvas.x - sd->detail_down_x))
                  / _scale_get(sd);
              if (sd->config.detail_min_w[i] < 0)
                sd->config.detail_min_w[i] = 0;
              snprintf(buf, sizeof(buf), "e.swallow.detail%i", i + 1);
              evas_object_size_hint_min_set(
                o, sd->config.detail_min_w[i] * _scale_get(sd), 0);
              edje_object_part_swallow(sd->o_list_detailed_dummy, buf, o);
              _reposition_detail_bars(sd);
              _detail_realized_items_resize(sd);
              break;
            }
        }
    }
}

static void
_add_overlay_objects(Smart_Data *sd)
{
  Evas_Object *o;
  Evas        *e;
  const char  *theme_edj_file, *grp;
  char         buf[128];
  int          i;

  e = evas_object_evas_get(sd->o_scroller);

  for (i = 0; i < 6; i++)
    {
      if (sd->o_list_detail_swallow[i])
        {
          evas_object_del(sd->o_list_detail_swallow[i]);
          sd->o_list_detail_swallow[i] = NULL;
        }
    }
  if (sd->o_overlay_info)
    {
      evas_object_del(sd->o_overlay_info);
      sd->o_overlay_info = NULL;
    }
  if (sd->o_overlay_grid)
    {
      evas_object_del(sd->o_overlay_grid);
      sd->o_overlay_grid = NULL;
    }
  if (sd->o_overlay_grid_fill)
    {
      evas_object_del(sd->o_overlay_grid_fill);
      sd->o_overlay_grid_fill = NULL;
    }
  for (i = 0; i < 6; i++)
    {
      sd->o_list_detail[i] = NULL;
    }

  sd->o_overlay_grid_fill = o = elm_grid_add(sd->o_scroller);
  elm_grid_size_set(o, 1, 1);
  elm_object_part_content_set(sd->o_scroller, "elm.swallow.overlay", o);
  evas_object_show(o);

  sd->o_overlay_grid = o = elm_grid_add(sd->o_scroller);
  evas_object_event_callback_add(o, EVAS_CALLBACK_RESIZE,
                                 _cb_overlay_grid_resize, sd);
  elm_grid_size_set(o, 100, 100);
  elm_grid_pack(sd->o_overlay_grid_fill, o, 0, 0, 1, 1);
  evas_object_show(o);

  sd->o_overlay_info = o = edje_object_add(e);
  grp                    = "e/fileman/default/overlay";
  theme_edj_file         = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);
  elm_grid_pack(sd->o_overlay_grid_fill, o, 0, 0, 1, 1);
  evas_object_show(o);

  for (i = 0; i < 6; i++)
    {
      sd->o_list_detail[i] = o = edje_object_add(e);
      grp                      = "e/fileman/default/detail-sizer";
      theme_edj_file           = elm_theme_group_path_find(NULL, grp);
      edje_object_file_set(o, theme_edj_file, grp);
      elm_grid_pack(sd->o_overlay_grid, o, 100 - i - 10, 0, 1, 100);
      if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
        evas_object_show(o);
      evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN,
                                     _cb_overlay_detail_mouse_down, sd);
      evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP,
                                     _cb_overlay_detail_mouse_up, sd);
      evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_MOVE,
                                     _cb_overlay_detail_mouse_move, sd);

      sd->o_list_detail_swallow[i] = o = evas_object_rectangle_add(e);
      evas_object_pass_events_set(o, EINA_TRUE);
      snprintf(buf, sizeof(buf), "e.swallow.detail%i", i + 1);
      evas_object_size_hint_min_set(
        o, sd->config.detail_min_w[i] * _scale_get(sd), 0);
      evas_object_event_callback_add(o, EVAS_CALLBACK_MOVE,
                                     _cb_overlay_detail_swallow_move, sd);
      evas_object_event_callback_add(o, EVAS_CALLBACK_RESIZE,
                                     _cb_overlay_detail_swallow_resize, sd);
      edje_object_part_swallow(sd->o_list_detailed_dummy, buf, o);
      evas_object_show(o);
    }
}

static void
_cb_canvas_resize(void *data, Evas *e EINA_UNUSED, void *event_info EINA_UNUSED)
{ // canvas resized and so handle so new visible icons are visible etc.
  Smart_Data  *sd  = data;
  Evas_Object *obj = sd->o_smart;

  evas_object_smart_changed(obj);
}

// gui code
static void
_smart_add(Evas_Object *obj)
{ // create a new efm view
  Smart_Data  *sd;
  Evas_Object *o;
  Evas        *e;
  const char  *theme_edj_file, *grp;

  if (_log_dom < 0)
    _log_dom = eina_log_domain_register("efm", EINA_COLOR_WHITE);
  sd = calloc(1, sizeof(Smart_Data));
  if (!sd) return;
  evas_object_smart_data_set(obj, sd);

  _sc_parent.add(obj);

  e = evas_object_evas_get(obj);

  evas_event_callback_add(e, EVAS_CALLBACK_CANVAS_VIEWPORT_RESIZE,
                          _cb_canvas_resize, sd);

  sd->config.view_mode = EFM_VIEW_MODE_ICONS;
  sd->config.sort_mode = EFM_SORT_MODE_NAME | EFM_SORT_MODE_LABEL_NOT_PATH
                         | EFM_SORT_MODE_NOCASE | EFM_SORT_MODE_DIRS_FIRST;
  // XXX: get this from config...
  sd->config.icon_size         = 40;
  sd->config.detail_min_w[0]   = 50;
  sd->config.detail_min_w[1]   = 120;
  sd->config.detail_min_w[2]   = 130;
  sd->config.detail_min_w[3]   = 60;
  sd->config.detail_min_w[4]   = 60;
  sd->config.detail_min_w[5]   = 120;
  sd->config.backend           = eina_stringshare_add("default");
  sd->config.detail_heading[0] = eina_stringshare_add("Name");
  sd->config.detail_heading[1] = eina_stringshare_add("Size");
  sd->config.detail_heading[2] = eina_stringshare_add("Date");
  sd->config.detail_heading[3] = eina_stringshare_add("Type");
  sd->config.detail_heading[4] = eina_stringshare_add("User");
  sd->config.detail_heading[5] = eina_stringshare_add("Group");
  sd->config.detail_heading[6] = eina_stringshare_add("Permissions");

  evas_object_size_hint_min_set(obj, 1, 1);

  sd->o_smart = obj;

  sd->o_clip = o = evas_object_rectangle_add(e);
  evas_object_smart_member_add(o, obj);
  evas_object_color_set(o, 255, 255, 255, 255);
  evas_object_show(o);

  sd->o_back = o = evas_object_rectangle_add(e);
  evas_object_color_set(o, 0, 0, 0, 0);
  evas_object_smart_member_add(o, obj);
  evas_object_clip_set(o, sd->o_clip);
  evas_object_show(o);
  evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN,
                                 _cb_back_mouse_down, sd);
  evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP, _cb_back_mouse_up,
                                 sd);
  evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_MOVE,
                                 _cb_back_mouse_move, sd);

  sd->o_sel = o = edje_object_add(e);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_smart_member_add(o, obj);
  evas_object_clip_set(o, sd->o_clip);
  grp            = "e/fileman/default/rubberband";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);

  sd->o_focus = o = edje_object_add(e);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_smart_member_add(o, obj);
  evas_object_clip_set(o, sd->o_clip);
  grp            = "elm/focus_highlight/top/default";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);

  sd->o_over = o = edje_object_add(e);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_smart_member_add(o, obj);
  evas_object_clip_set(o, sd->o_clip);
  grp            = "e/fileman/default/list/drop_in";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);

  sd->handler_exe_del
    = ecore_event_handler_add(ECORE_EXE_EVENT_DEL, _cb_exe_del, sd);
  sd->handler_exe_data
    = ecore_event_handler_add(ECORE_EXE_EVENT_DATA, _cb_exe_data, sd);

  sd->thread_data      = calloc(1, sizeof(Smart_Data_Thread));
  sd->thread_data->sd  = sd;
  sd->thread_data->thq = eina_thread_queue_new();
  sd->thread = ecore_thread_feedback_run(_cb_thread_main, _cb_thread_notify,
                                         _cb_thread_done, _cb_thread_done,
                                         sd->thread_data, EINA_TRUE);
  evas_object_event_callback_priority_add(sd->o_clip, EVAS_CALLBACK_KEY_DOWN,
                                          -100, _cb_key_down, sd);
  evas_object_event_callback_add(sd->o_clip, EVAS_CALLBACK_FOCUS_OUT,
                                 _cb_focus_out, sd);
  sd->menu_provider.cb = efm_menu_provider_default;
  _efm_list = eina_list_prepend(_efm_list, obj);
}

static void
_smart_del(Evas_Object *obj)
{ // delete/free efm view
  Block            *block;
  Icon             *icon;
  Evas             *e;
  int               i;
  Smart_Data_Timer *st;
  ENTRY;

  efm_menu_provider_select(sd->o_smart, -1);
  _efm_list = eina_list_remove(_efm_list, obj);
  e         = evas_object_evas_get(obj);
  evas_event_callback_del_full(e, EVAS_CALLBACK_CANVAS_VIEWPORT_RESIZE,
                               _cb_canvas_resize, sd);
  if (sd->size_bars_update_job)
    {
      ecore_job_del(sd->size_bars_update_job);
      sd->size_bars_update_job = NULL;
    }
  if (sd->size_max_update_job)
    {
      ecore_job_del(sd->size_max_update_job);
      sd->size_max_update_job = NULL;
    }
  if (sd->header_change_job)
    {
      ecore_job_del(sd->header_change_job);
      sd->header_change_job = NULL;
    }
  if (sd->drag_icon)
    {
      if (sd->o_scroller) elm_drag_cancel(sd->o_scroller);
      sd->drag_icon->sd = NULL;
      sd->drag_icon     = NULL;
    }
  if (sd->refocus_job)
    {
      ecore_job_del(sd->refocus_job);
      sd->refocus_job = NULL;
    }
  if (sd->thread)
    {
      if (sd->thread_data) sd->thread_data->sd = NULL;
      ecore_thread_cancel(sd->thread);
      sd->thread = NULL;
    }
  eina_stringshare_replace(&(sd->config.path), NULL);
  eina_stringshare_replace(&(sd->config.backend), NULL);
  for (i = 0; i < 7; i++)
    eina_stringshare_replace(&(sd->config.detail_heading[i]), NULL);
  if (sd->exe_open)
    {
      Pending_Exe_Del *pend = calloc(1, sizeof(Pending_Exe_Del));

      if (pend)
        {
          pend->exe         = sd->exe_open;
          pend->timer       = ecore_timer_add(5.0, _cb_exe_pending_timer, pend);
          _pending_exe_dels = eina_list_append(_pending_exe_dels, pend);
        }
      ecore_exe_interrupt(sd->exe_open);
      sd->exe_open = NULL;
    }
  if (sd->handler_exe_del)
    {
      ecore_event_handler_del(sd->handler_exe_del);
      sd->handler_exe_del = NULL;
    }
  if (sd->handler_exe_data)
    {
      ecore_event_handler_del(sd->handler_exe_data);
      sd->handler_exe_data = NULL;
    }
  EINA_LIST_FREE(sd->blocks, block) _block_free(block);
  EINA_LIST_FREE(sd->icons, icon) _icon_free(icon);
  for (i = 0; i < 6; i++)
    {
      if (sd->o_list_detail_swallow[i])
        {
          evas_object_del(sd->o_list_detail_swallow[i]);
          sd->o_list_detail_swallow[i] = NULL;
        }
    }
  if (sd->o_detail_header)
    {
      evas_object_del(sd->o_detail_header);
      sd->o_detail_header = NULL;
    }
  if (sd->o_clip)
    {
      evas_object_del(sd->o_clip);
      sd->o_clip = NULL;
    }
  if (sd->o_focus)
    {
      evas_object_del(sd->o_focus);
      sd->o_focus = NULL;
    }
  if (sd->o_back)
    {
      evas_object_del(sd->o_back);
      sd->o_back = NULL;
    }
  if (sd->o_sel)
    {
      evas_object_del(sd->o_sel);
      sd->o_sel = NULL;
    }
  if (sd->o_over)
    {
      evas_object_del(sd->o_over);
      sd->o_over = NULL;
    }
  if (sd->o_list_detailed_dummy)
    {
      evas_object_del(sd->o_list_detailed_dummy);
      sd->o_list_detailed_dummy = NULL;
    }
  if (sd->o_overlay_grid)
    {
      evas_object_del(sd->o_overlay_grid);
      sd->o_overlay_grid = NULL;
    }
  if (sd->o_overlay_grid_fill)
    {
      evas_object_del(sd->o_overlay_grid_fill);
      sd->o_overlay_grid_fill = NULL;
    }
  if (sd->reblock_job)
    {
      ecore_job_del(sd->reblock_job);
      sd->reblock_job = NULL;
    }
  if (sd->focus_animator)
    {
      ecore_animator_del(sd->focus_animator);
      sd->focus_animator = NULL;
    }
  if (sd->scroll_timer)
    {
      ecore_timer_del(sd->scroll_timer);
      sd->scroll_timer = NULL;
    }
  if (sd->dnd_over_open_timer)
    {
      ecore_timer_del(sd->dnd_over_open_timer);
      sd->dnd_over_open_timer = NULL;
    }
  if (sd->dnd_scroll_timer)
    {
      ecore_timer_del(sd->dnd_scroll_timer);
      sd->dnd_scroll_timer = NULL;
    }
  EINA_LIST_FREE(sd->timers, st)
  {
    eina_stringshare_del(st->name);
    ecore_timer_del(st->timer);
    free(st);
  }
  if (sd->dnd_drop_data)
    {
      free(sd->dnd_drop_data);
      sd->dnd_drop_data = NULL;
    }
  _icon_custom_data_free(sd);

  // XXX: anything special with scroller?
  sd->o_scroller = NULL;
  sd->o_smart    = NULL;

  _sc_parent.del(obj);
  evas_object_smart_data_set(obj, NULL);
}

static void
_smart_move(Evas_Object *obj, Evas_Coord x, Evas_Coord y)
{ // efm view object moved
  ENTRY;

  if ((sd->geom.x == x) && (sd->geom.y == y)) return;
  sd->geom.x = x;
  sd->geom.y = y;
  evas_object_smart_changed(obj);
  evas_object_move(sd->o_clip, x, y);
  evas_object_move(sd->o_list_detailed_dummy, x, y);
  if (sd->sel_show)
    {
      _efm_sel_store_end(sd, sd->back_x, sd->back_y);
      _efm_sel_position(sd);
//      if (!sd->scroll_timer)
//        sd->scroll_timer
//          = ecore_timer_add(SCROLL_SEL_TIMER, _cb_sel_bounds_scroll_timer, sd);
    }
}

static void
_smart_resize(Evas_Object *obj, Evas_Coord w, Evas_Coord h)
{ // efm veiw object resized
  Eina_Bool width_change = EINA_FALSE;
  Eina_Bool height_change = EINA_FALSE;
  ENTRY;

  if ((sd->geom.w == w) && (sd->geom.h == h)) return;
  if (w != sd->geom.w) width_change = EINA_TRUE;
  if (h != sd->geom.h) height_change = EINA_TRUE;
  sd->geom.w = w;
  sd->geom.h = h;
  evas_object_smart_changed(obj);
  evas_object_resize(sd->o_clip, w, h);
  // if width changed we have to re-flow (relayout) icons - unless it's custom
  // layout where we have fixed  geom per icon
  if ((width_change)
      && ((sd->config.view_mode == EFM_VIEW_MODE_ICONS)
          || (sd->config.view_mode == EFM_VIEW_MODE_LIST)
          || (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)))
    sd->relayout = EINA_TRUE;
  if ((height_change)
      && ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_VERTICAL)
          || (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL)))
    sd->relayout = EINA_TRUE;
  if (width_change)
    {
      evas_object_resize(sd->o_list_detailed_dummy, w, sd->list_detailed_min_h);
      printf("RSZW: %p %i\n", sd->o_list_detailed_dummy, w);
    }
}

static void
_layout_bounds_increase(Eina_Rectangle *bounds, Eina_Rectangle *newrect)
{
  int x1, y1, x2, y2;

  x1 = bounds->x;
  y1 = bounds->y;
  x2 = x1 + bounds->w;
  y2 = y1 + bounds->h;
  if (newrect->x < x1) x1 = newrect->x;
  if (newrect->y < y1) y1 = newrect->y;
  if ((newrect->x + newrect->w) > x2) x2 = newrect->x + newrect->w;
  if ((newrect->y + newrect->h) > y2) y2 = newrect->y + newrect->h;
  bounds->x = x1;
  bounds->y = y1;
  bounds->w = x2 - x1;
  bounds->h = y2 - y1;
}

static void
_relayout_icons(Smart_Data *sd)
{ // wall all blocks and icons in blocks and assign them geometry
  Eina_List *bl, *il;
  Block     *block;
  Icon      *icon;
  Evas_Coord x, y, minw, minh;

  // this is a row by row top-left to bottom-right layout
  x = y = 0;
  minw = minh = 0;
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    EINA_LIST_FOREACH(block->icons, il, icon)
    { // icon is at urrent x,y
      icon->geom.x = x;
      icon->geom.y = y;
      icon->geom.w = sd->icon_min_w;
      icon->geom.h = sd->icon_min_h;
      if (sd->config.view_mode == EFM_VIEW_MODE_ICONS)
        {
          x += icon->geom.w;
          if ((x + icon->geom.w) > sd->geom.w)
            { // if next icon over end of row, start a new row
              x = 0;
              y += icon->geom.h;
            }
        }
      else if (sd->config.view_mode == EFM_VIEW_MODE_ICONS_VERTICAL)
        {
          y += icon->geom.h;
          if ((y + icon->geom.h) > sd->geom.h)
            { // if next icon over end of row, start a new row
              y = 0;
              x += icon->geom.w;
            }
        }
      if (block->icons == il) // first icon in block
        block->bounds = icon->geom;
      else // expand block bounds based on icon geom
        _layout_bounds_increase(&block->bounds, &icon->geom);
    }
    // adjust view minw/h if block expands it
    if ((block->bounds.x + block->bounds.w) > minw)
      minw = block->bounds.x + block->bounds.w;
    if ((block->bounds.y + block->bounds.h) > minh)
      minh = block->bounds.y + block->bounds.h;
  }
  // set min size for scroller to know content size
  if (sd->config.view_mode == EFM_VIEW_MODE_ICONS)
    evas_object_size_hint_min_set(sd->o_smart, sd->icon_min_w, minh);
  else if (sd->config.view_mode == EFM_VIEW_MODE_ICONS_VERTICAL)
    evas_object_size_hint_min_set(sd->o_smart, minw, sd->icon_min_h);
}

static void
_recalc(Smart_Data *sd)
{ // recalc position of icons and which may or may not be visible now
  Eina_List     *bl, *il;
  Block         *block;
  Icon          *icon;
  Eina_Rectangle viewport, rect;
  Evas          *e;
  int            num = 0;
  const char    *theme_edj_file;

  e = evas_object_evas_get(sd->o_smart);
  evas_output_viewport_get(e, &(viewport.x), &(viewport.y), &(viewport.w),
                           &(viewport.h));
  theme_edj_file
    = elm_theme_group_path_find(NULL, "e/fileman/default/icon/fixed");
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  { // walk all our blocks and per clock ... is that block visible
    rect = block->bounds;
    rect.x += sd->geom.x;
    rect.y += sd->geom.y;
    if (eina_rectangles_intersect(&rect, &viewport))
      { // if the block intersects the viewport, then look into it
        // check all the icon to see if each is visible
        EINA_LIST_FOREACH(block->icons, il, icon)
        { // check each icon...
          rect = icon->geom;
          rect.x += sd->geom.x;
          rect.y += sd->geom.y;
          // if the icon is within the viewport ...
          if (eina_rectangles_intersect(&rect, &viewport))
            { // we are within vierwport - so could be visible
              if (icon->changed)
                { // icon changed - let's redo it, so del the old
                  _icon_object_clear(icon);
                }
              icon->changed = EINA_FALSE;
              // no icon yet ... it needs to be created
              if (!icon->o_base)
                { // no object - let's create one
                  // about to realize if it hasn't been
                  if (!icon->realized) icon->block->realized_num++;
                  _icon_object_add(icon, sd, e, theme_edj_file, EINA_TRUE, num);
                }
              // position the icon object where it should be
              evas_object_geometry_set(icon->o_base, rect.x, rect.y, rect.w,
                                       rect.h);
              if (icon->over)
                evas_object_geometry_set(icon->sd->o_over, rect.x, rect.y,
                                         rect.w, rect.h);
            }
          else
            { // icon not in viewport
              if (icon->realized)
                { // it's realized so unrealize it
                  icon->realized = EINA_FALSE;
                  icon->block->realized_num--;
                  _icon_object_clear(icon);
                }
            }
          num++;
        }
      }
    else if (block->realized_num > 0)
      { // block is realized but NOT in the viewport (not visible)
        EINA_LIST_FOREACH(block->icons, il, icon)
        { // go through each icon now
          if (icon->realized)
            { // if the icon was realized then unrealize it
              if (icon->realized) block->realized_num--;
              icon->realized = EINA_FALSE;
              _icon_object_clear(icon);
            }
        }
        num += eina_list_count(block->icons);
      }
    else num += eina_list_count(block->icons);
  }

  evas_object_geometry_set(sd->o_back, sd->geom.x, sd->geom.y, sd->geom.w,
                           sd->geom.h);
  _efm_focus_position(sd);
  _efm_sel_position(sd);
}

static void
_listing_do(Smart_Data *sd)
{
  printf("XXX: LISTING DO   -----------------------------------------\n");
  sd->listing_done = EINA_FALSE;
  if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM)
      || (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL))
    {
      _icon_custom_data_bitmap_clear(sd);
    }
}

void
_listing_done(Smart_Data *sd)
{
  printf("XXX: LISTING DONE -----------------------------------------\n");
  sd->listing_done = EINA_TRUE;
  if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM)
      || (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL))
    {
      if (sd->reblock_job) sd->listing_done_reblock = EINA_TRUE;
      else
        {
          _icon_custom_data_all_icons_fill(sd);
          _recalc(sd);
        }
    }
}

static void
_relayout_icons_custom(Smart_Data *sd)
{ // wall all blocks and icons in blocks and assign them geometry
  Eina_List *bl, *il;
  Block     *block;
  Icon      *icon;
  Evas_Coord minw, minh;

  // this is a row by row top-left to bottom-right layout
  minw = minh = 0;
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    EINA_LIST_FOREACH(block->icons, il, icon)
    { // icon is at urrent x,y
      if (icon->geom.w == INVALID)
        {
          icon->geom.w = sd->icon_min_w;
          icon->geom.h = sd->icon_min_h;
        }
      if (sd->listing_done)
        {
          if (icon->geom.x == INVALID) _icon_custom_position_find(icon);
          else _icon_custom_position_placed(icon);
        }
      else
        {
          if (icon->geom.x != INVALID) _icon_custom_position_placed(icon);
        }
      if (block->icons == il) // first icon in block
        block->bounds = icon->geom;
      else // expand block bounds based on icon geom
        _layout_bounds_increase(&block->bounds, &icon->geom);
    }
    // adjust view minw/h if block expands it
    if ((block->bounds.x + block->bounds.w) > minw)
      minw = block->bounds.x + block->bounds.w;
    if ((block->bounds.y + block->bounds.h) > minh)
      minh = block->bounds.y + block->bounds.h;
  }
  // set min size for scroller to know content size
  evas_object_size_hint_min_set(sd->o_smart, minw, minh);
}

static void
_relayout_list(Smart_Data *sd)
{ // wall all blocks and icons in blocks and assign them geometry
  Eina_List *bl, *il;
  Block     *block;
  Icon      *icon;
  Evas_Coord x, y, minw, minh;

  // this is a row by row top-left to bottom-right layout
  x = y = 0;
  minw = minh = 0;
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    EINA_LIST_FOREACH(block->icons, il, icon)
    { // icon is at urrent x,y
      icon->geom.x = x;
      icon->geom.y = y;
      icon->geom.w = sd->geom.w;
      if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
        icon->geom.h = sd->list_detailed_min_h;
      else icon->geom.h = sd->list_min_h;
      y += icon->geom.h;
      if (block->icons == il) // first icon in block
        block->bounds = icon->geom;
      else // expand block bounds based on icon geom
        _layout_bounds_increase(&block->bounds, &icon->geom);
    }
    // adjust view minw/h if block expands it
    if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
      {
        if ((sd->list_detailed_min_w) > minw)
          minw = block->bounds.x + sd->list_detailed_min_w;
      }
    else
      {
        if ((sd->list_min_w) > minw) minw = block->bounds.x + sd->list_min_w;
      }
    if ((block->bounds.y + block->bounds.h) > minh)
      minh = block->bounds.y + block->bounds.h;
  }
  // set min size for scroller to know content size
  evas_object_size_hint_min_set(sd->o_smart, minw, minh);
}

static void
_relayout(Smart_Data *sd)
{ // wall all blocks and icons in blocks and assign them geometry
  //   printf("XXXXX relayout...\n");
  // XXX: add other layouts like:
  // XXX: fixed position per icon layout (just calc block bounds)
  // XXX: column layout (top-left down then next col along)
  // XXX: list layout
  if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS) ||
      (sd->config.view_mode == EFM_VIEW_MODE_ICONS_VERTICAL))
    _relayout_icons(sd);
  else if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM) ||
           (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL))
    _relayout_icons_custom(sd);
  else if (sd->config.view_mode == EFM_VIEW_MODE_LIST) _relayout_list(sd);
  else if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
    _relayout_list(sd);
}

void
_reset(Smart_Data *sd)
{
  Eina_Strbuf *buf;
  Block       *block;
  Icon        *icon;
  static char  envbuf[PATH_MAX];
  const char  *s;

  // clear out stuff there so we can start listing again...
  efm_menu_provider_select(sd->o_smart, -1);
  sd->file_max = 0;
  EINA_LIST_FREE(sd->blocks, block) _block_free(block);
  while (sd->icons)
    {
      icon = sd->icons->data;

      _icon_free(icon);
    }
  if (sd->reblock_job)
    {
      ecore_job_del(sd->reblock_job);
      sd->reblock_job = NULL;
    }
  if (sd->exe_open)
    {
      Pending_Exe_Del *pend = calloc(1, sizeof(Pending_Exe_Del));

      if (pend)
        {
          pend->exe         = sd->exe_open;
          pend->timer       = ecore_timer_add(5.0, _cb_exe_pending_timer, pend);
          _pending_exe_dels = eina_list_append(_pending_exe_dels, pend);
        }
      ecore_exe_interrupt(sd->exe_open);
      sd->exe_open = NULL;
    }
  if (sd->thread)
    {
      if (sd->thread_data) sd->thread_data->sd = NULL;
      ecore_thread_cancel(sd->thread);
      sd->thread = NULL;
    }
  // now start up opening up a dir again
  sd->thread_data      = calloc(1, sizeof(Smart_Data_Thread));
  sd->thread_data->sd  = sd;
  sd->thread_data->thq = eina_thread_queue_new();
  sd->thread = ecore_thread_feedback_run(_cb_thread_main, _cb_thread_notify,
                                         _cb_thread_done, _cb_thread_done,
                                         sd->thread_data, EINA_TRUE);
  buf        = eina_strbuf_new();
  s          = getenv("E_HOME_DIR");
  if (s) eina_strbuf_append(buf, s);
  else
    {
      s = getenv("HOME");
      eina_strbuf_append(buf, s);
      eina_strbuf_append(buf, "/.e/e");
    }
  eina_strbuf_append(buf, "/efm/backends/");
  eina_strbuf_append(buf, sd->config.backend);
  snprintf(envbuf, sizeof(envbuf), "EFM_BACKEND_DIR=%s",
           eina_strbuf_string_get(buf));
  eina_strbuf_append(buf, "/open");
  if (!ecore_file_can_exec(eina_strbuf_string_get(buf)))
    {
      eina_strbuf_reset(buf);

      eina_strbuf_append(buf, elm_app_lib_dir_get());
      eina_strbuf_append(buf, "/efm/backends/");
      eina_strbuf_append(buf, sd->config.backend);
      snprintf(envbuf, sizeof(envbuf), "EFM_BACKEND_DIR=%s",
               eina_strbuf_string_get(buf));
      eina_strbuf_append(buf, "/open");
    }
  putenv(envbuf);
  sd->exe_open
    = ecore_exe_pipe_run(eina_strbuf_string_get(buf),
                         ECORE_EXE_PIPE_READ | ECORE_EXE_PIPE_READ_LINE_BUFFERED
                           | ECORE_EXE_PIPE_WRITE,
                         NULL);
  eina_strbuf_free(buf);

  if (sd->config.path)
    {
      printf("XXX: OPEN [ %s ]\n", sd->config.path);
      _listing_do(sd);
      buf = cmd_strbuf_new("dir-set");
      cmd_strbuf_append(buf, "path", sd->config.path);
      cmd_strbuf_exe_consume(buf, sd->exe_open);
    }
}

static void
_cb_scroller_focus(void *data, Evas_Object *obj EINA_UNUSED,
                   void *info EINA_UNUSED)
{
  Smart_Data *sd = data;

  //  printf("XXX: FOCUS\n");
  sd->focused = EINA_TRUE;
  evas_object_focus_set(sd->o_clip, EINA_TRUE);
}

static void
_cb_scroller_unfocus(void *data, Evas_Object *obj EINA_UNUSED,
                     void *info EINA_UNUSED)
{
  Smart_Data *sd = data;

  //  printf("XXX: UNFOCUS\n");
  sd->focused = EINA_FALSE;
}

static void
_smart_calculate(Evas_Object *obj)
{
  ENTRY;

  if ((sd->reblocked) || (sd->relayout))
    { // if we red-d the list of blocks (added/removed files) or need to
      // re-layout due to q width or otheer geometry change
      _relayout(sd);
      sd->reblocked = EINA_FALSE;
      sd->relayout  = EINA_FALSE;
    }
  // moved/resize icons and/or realize/unrealize (create or delete objects
  // that we need to awe only keep a limited set of ivisible/active icons
  // that we need)
  _recalc(sd);
}

//////////////////////////////////////////////////////////////////////////////

Evas_Object *
efm_add(Evas_Object *parent)
{
  if (!_smart)
    {
      evas_object_smart_clipped_smart_set(&_sc_parent);
      _sc           = _sc_parent;
      _sc.name      = "efm";
      _sc.version   = EVAS_SMART_CLASS_VERSION;
      _sc.add       = _smart_add;
      _sc.del       = _smart_del;
      _sc.resize    = _smart_resize;
      _sc.move      = _smart_move;
      _sc.calculate = _smart_calculate;
    };
  if (!_smart) _smart = evas_smart_class_new(&_sc);
  return evas_object_smart_add(evas_object_evas_get(parent), _smart);
}

static void
_cb_detail_header_resize(void *data, Evas *e EINA_UNUSED,
                         Evas_Object *obj EINA_UNUSED, void *info EINA_UNUSED)
{
  Smart_Data *sd = data;

  _reposition_detail_header_items(sd);
}

static void
_cb_detail_header_del(void *data, Evas *e EINA_UNUSED,
                      Evas_Object *obj EINA_UNUSED, void *info EINA_UNUSED)
{
  Smart_Data *sd = data;
  int         i;

  sd->o_detail_header = NULL;
  for (i = 0; i < 7; i++) sd->o_detail_header_item[i] = NULL;
}

static void
_cb_detail_header_item_hint(void *data, Evas *e EINA_UNUSED, Evas_Object *obj,
                            void *info EINA_UNUSED)
{
  Smart_Data *sd = data;
  Evas_Coord  h, minh = 0;
  int         i;

  if (!sd->o_detail_header) return;
  for (i = 0; i < 7; i++)
    {
      if (obj == sd->o_detail_header_item[i])
        {
          evas_object_size_hint_min_get(obj, NULL, &h);
          sd->detail_header_min_h[i] = h;
          break;
        }
    }
  for (i = 0; i < 7; i++)
    {
      h = sd->detail_header_min_h[i];
      if (h > minh) minh = h;
    }
  evas_object_size_hint_min_set(sd->o_detail_header, 0, minh);
}

static void
_cb_detail_radio_changed(void *data, Evas_Object *obj EINA_UNUSED,
                         void *info EINA_UNUSED)
{
  Smart_Data   *sd = data;
  Efm_Sort_Mode sort_mode;

  sort_mode = elm_radio_value_get(sd->o_detail_header_item[0]);
  efm_path_sort_mode_set(
    sd->o_smart, sort_mode | (sd->config.sort_mode & EFM_SORT_MODE_FLAGS));
  evas_object_smart_callback_call(sd->o_smart, "sort_mode", NULL);
}

static void
_detail_setup(Smart_Data *sd)
{
  Evas_Object *o, *o_grid, *o_radio_group;

  sd->o_detail_header = o_grid = o = elm_grid_add(sd->o_scroller);
  evas_object_event_callback_add(o, EVAS_CALLBACK_RESIZE,
                                 _cb_detail_header_resize, sd);
  evas_object_event_callback_add(o, EVAS_CALLBACK_DEL, _cb_detail_header_del,
                                 sd);

  o_radio_group = o           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[0] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_NAME);
  elm_grid_pack(o_grid, o, 0, 0, 40, 100);
  elm_object_text_set(o, sd->config.detail_heading[0]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[1] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_SIZE);
  elm_grid_pack(o_grid, o, 40, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[1]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[2] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_DATE);
  elm_grid_pack(o_grid, o, 50, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[2]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[3] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_MIME);
  elm_grid_pack(o_grid, o, 60, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[3]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[4] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_USER);
  elm_grid_pack(o_grid, o, 70, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[4]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[5] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_GROUP);
  elm_grid_pack(o_grid, o, 80, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[5]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  o                           = elm_radio_add(sd->o_detail_header);
  sd->o_detail_header_item[6] = o;
  evas_object_event_callback_add(o, EVAS_CALLBACK_CHANGED_SIZE_HINTS,
                                 _cb_detail_header_item_hint, sd);
  elm_object_style_set(o, "sort_header");
  elm_radio_state_value_set(o, EFM_SORT_MODE_PERMISSIONS);
  elm_grid_pack(o_grid, o, 90, 0, 10, 100);
  elm_radio_group_add(o, o_radio_group);
  elm_object_text_set(o, sd->config.detail_heading[6]);
  evas_object_smart_callback_add(o, "changed", _cb_detail_radio_changed, sd);
  evas_object_show(o);

  elm_radio_value_set(o_radio_group, sd->config.sort_mode & EFM_SORT_MODE_MASK);
}

////////
void
efm_scroller_set(Evas_Object *obj, Evas_Object *scroller)
{
  Evas_Object *o, *o2;
  const char  *theme_edj_file, *grp;
  Evas        *e;
  ENTRY;

  // XXX: do any necessary re-inits ? should not be needed as scroller should
  // XXX: remove scroller drop target...
  // never change once set up at start
  sd->o_scroller = scroller;

  e = evas_object_evas_get(obj);

  o              = edje_object_add(e);
  grp            = "e/fileman/default/icon/fixed";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);
  o2 = evas_object_rectangle_add(e);
  evas_object_size_hint_min_set(o2, sd->config.icon_size * _scale_get(sd),
                                sd->config.icon_size * _scale_get(sd));
  edje_object_part_swallow(o, "e.swallow.icon", o2);
  edje_object_size_min_calc(o, &(sd->icon_min_w), &(sd->icon_min_h));
  evas_object_del(o2);
  evas_object_del(o);

  o              = edje_object_add(e);
  grp            = "e/fileman/default/list/fixed";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);
  edje_object_size_min_calc(o, &(sd->list_min_w), &(sd->list_min_h));
  evas_object_del(o);

  sd->o_list_detailed_dummy = o = edje_object_add(e);
  evas_object_smart_member_add(o, obj);
  grp            = "e/fileman/default/list/detailed";
  theme_edj_file = elm_theme_group_path_find(NULL, grp);
  edje_object_file_set(o, theme_edj_file, grp);
  edje_object_size_min_calc(o, &(sd->list_detailed_min_w),
                            &(sd->list_detailed_min_h));
  evas_object_color_set(o, 0, 0, 0, 0);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_show(o);

  evas_object_smart_callback_add(sd->o_scroller, "focused", _cb_scroller_focus,
                                 sd);
  evas_object_smart_callback_add(sd->o_scroller, "unfocused",
                                 _cb_scroller_unfocus, sd);
  _drop_init(sd);
  _add_overlay_objects(sd);
}

Evas_Object *
efm_scroller_get(Evas_Object *obj)
{
  ENTRY NULL;

  return sd->o_scroller;
}

Evas_Object *
efm_detail_header_get(Evas_Object *obj)
{
  ENTRY NULL;

  if (!sd->o_detail_header) _detail_setup(sd);
  return sd->o_detail_header;
}

////////
char *
_sanitize_dir(const char *path)
{ // sanitize path to remove multiple / chars like //usr/bin or /usr///bin
  const char *p, *pp;
  char       *path2, *p2;

  if (!path) return NULL;
  path2 = malloc(strlen(path) + 2);
  for (pp = NULL, p2 = path2, p = path; *p;)
    {
      if ((pp) && (*p == '/') && (*pp == '/'))
        { // prev chr is / and current is / - skip
          pp = p++;
        }
      else
        {
          *p2++ = *p;
          pp    = p++;
        }
    }
  if ((pp) && (*pp != '/'))
    { // add a trailing / on the end as we want to keep from adding it
      *p2 = '/';
      p2++;
    }
  *p2 = 0;
  return path2;
}

void
efm_path_set(Evas_Object *obj, const char *path)
{
  char *path_sane;
  ENTRY;

  path_sane = _sanitize_dir(path);
  if (!path_sane) return;
  if ((sd->config.path) && (!strcmp(sd->config.path, path_sane))) goto done;
  eina_stringshare_replace(&(sd->config.path), path_sane);
  _reset(sd);
done:
  free(path_sane);
}

const char *
efm_path_get(Evas_Object *obj)
{
  ENTRY NULL;

  return sd->config.path;
}

void
efm_path_parent(Evas_Object *obj)
{
  char *tmps, *p;

  ENTRY;
  if (!sd->config.path) return;
  tmps = strdup(sd->config.path);
  if (!tmps) return;
  p = strrchr(tmps, '/');
  if ((p) && (p != tmps))
    {
      *p = 0;
      p  = strrchr(tmps, '/');
      if (p) p[1] = 0;
    }
  eina_stringshare_replace(&sd->config.path, tmps);
  _reset(sd);
  free(tmps);
}

////////
void
efm_path_view_mode_set(Evas_Object *obj, Efm_View_Mode mode)
{
  int i;
  ENTRY;

  if (sd->config.view_mode == mode) return;
  sd->config.view_mode = mode;
  for (i = 0; i < 6; i++)
    {
      if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
        evas_object_show(sd->o_list_detail[i]);
      else evas_object_hide(sd->o_list_detail[i]);
    }
  if (sd->header_change_job) ecore_job_del(sd->header_change_job);
  sd->header_change_job = ecore_job_add(_cb_header_change, sd);
  _reset(sd);
}

Efm_View_Mode
efm_path_view_mode_get(Evas_Object *obj)
{
  ENTRY EFM_VIEW_MODE_ICONS;

  return sd->config.view_mode;
}

void
efm_path_sort_mode_set(Evas_Object *obj, Efm_Sort_Mode mode)
{
  ENTRY;

  if (sd->config.sort_mode == mode) return;
  sd->config.sort_mode = mode;
  _reset(sd);
}

Efm_Sort_Mode
efm_path_sort_mode_get(Evas_Object *obj)
{
  ENTRY EFM_SORT_MODE_NAME;

  return sd->config.sort_mode;
}

Evas_Coord
efm_column_min_get(Evas_Object *obj, int col)
{
  ENTRY 0;

  if (col < 0) return 0;
  else if (col >= 6) return 0;
  return sd->config.detail_min_w[col];
}

void
efm_column_min_set(Evas_Object *obj, int col, Evas_Coord w)
{
  ENTRY;

  if (col < 0) return;
  else if (col >= 6) return;
  sd->config.detail_min_w[col] = w;
  _redo_detail_sizes(sd);
}

void
efm_column_label_set(Evas_Object *obj, int col, const char *label)
{
  ENTRY;

  if (col < 0) return;
  else if (col > 7) return;
  eina_stringshare_replace(&(sd->config.detail_heading[col]), label);
  if (sd->o_detail_header_item[col])
    elm_object_text_set(sd->o_detail_header_item[col],
                        sd->config.detail_heading[col]);
}

const char *
efm_column_label_get(Evas_Object *obj, int col)
{
  ENTRY NULL;

  if (col < 0) return NULL;
  else if (col > 7) return NULL;
  return sd->config.detail_heading[col];
}

Evas_Coord
efm_iconsize_get(Evas_Object *obj)
{
  ENTRY 0;

  return sd->config.icon_size;
}

void
efm_iconsize_set(Evas_Object *obj, Evas_Coord sz)
{
  ENTRY;

  sd->config.icon_size = sz;
}

const char *
efm_backend_get(Evas_Object *obj)
{
  ENTRY NULL;

  return sd->config.backend;
}

void
efm_backend_set(Evas_Object *obj, const char *backend)
{
  ENTRY;

  if (!backend) backend = "default";
  eina_stringshare_replace(&(sd->config.backend), backend);
}

void
efm_menu_provider_set(Evas_Object *obj, Efm_Menu_Provider cb, void *data)
{ // set a func to build/show a menu and handle interaction with it
  ENTRY;

  sd->menu_provider.cb = cb;
  sd->menu_provider.data = data;
}

static Eina_Bool
_item_find_call(Evas_Object *obj, const Efm_Menu *m, int index)
{
  int i;

  for (i = 0; i < m->item_num; i++)
    {
      if (m->item[i].index == index)
        {
          Efm_Menu_Item_Callback fn = m->item[i].private1;

          if (fn)
            fn(m->item[i].private2, m->item[i].private3, obj,
               &(m->item[i]));
          return EINA_TRUE;
        }
      else if (m->item[i].type == EFM_MENU_ITEM_SUBMENU)
        {
          if (_item_find_call(obj, m->item[i].sub, index)) return EINA_TRUE;
        }
    }
  return EINA_FALSE;
}

void
efm_menu_provider_select(Evas_Object *obj, int index)
{ // call this to tell efm what was selected, or index -1 for dismissed
  Efm_Menu *m;
  Efm_Menu_Callback cb;
  ENTRY;

  m = sd->menu_provider.menu;
  if (!m) return;
  if (index >= 0) _item_find_call(obj, m, index);

  sd->menu_provider.menu = NULL;
  cb                     = m->private1;
  if (cb) cb(m->private2, m->private3, obj, m);
  _efm_menu_del(m);
  sd->menu_provider.menu_data = sd->menu_provider.cb(
    sd->menu_provider.data, sd->menu_provider.menu_data, obj, NULL, 0, 0);
}

void
_efm_menu_show(Evas_Object *obj, Efm_Menu *m, Evas_Coord x, Evas_Coord y)
{ // called within efm to ask to show a popup menu, m is NULL to dismiss
  ENTRY;

  if (!sd->menu_provider.cb) return;
  sd->menu_provider.menu = m;
  sd->menu_provider.menu_data
    = sd->menu_provider.cb(sd->menu_provider.data, sd->menu_provider.menu_data,
                           obj, sd->menu_provider.menu, x, y);
}

static void
_cb_menu_dismissed(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{ // called when the menu is closed
  efm_menu_provider_select(data, -1);
}

static void
_cb_menu_item(void *data, Evas_Object *obj, void *event_info EINA_UNUSED)
{
  Evas_Object *efm = evas_object_data_get(obj, "efm");
  Efm_Menu_Item *mi  = data;

  efm_menu_provider_select(efm, mi->index);
}

static Evas_Object *
_menu_icon_radcheck_add(Evas_Object *o_base, int icon_col, const char *icon, int icon_num)
{
  Evas_Object *o_table, *o;

  o_table = o = elm_table_add(o_base);
  elm_table_homogeneous_set(o, EINA_TRUE);
  // add a "dummy" never shown check+radio in first cell for sizing.
  // homogenous cells mean 2nd cell will be same size too if there
  elm_table_pack(o, elm_check_add(o_base), 0, 0, 1, 1);
  elm_table_pack(o, elm_radio_add(o_base), 0, 0, 1, 1);
  if (icon)
    { // we have an icon to add to the icon column
      // icon can be,,,,
      //
      // "std:menu-eject" - native to elm menus (strip std:)
      // "std:menu/folder" - native to elm menus (strip std:)
      // "file:/path/to/file.png" - use efm_icon
      // "/path/to/file.png" - same as above
      // "icon:/path/to/file" - use efm_icon
      // "thumb:/path/to/file" - use efm_icon
      // "video:/path/to/file.mp4" - use efm_icon
      // "fdo:icon-name" - use efreet to look up icon then use that
      o = NULL;
      if (!strncmp(icon, "std:", 4))
        {
          o = elm_icon_add(o_base);
          elm_icon_standard_set(o, icon + 4);
        }
      else if (!strncmp(icon, "file:", 5))
        {
          o = efm_icon_add(o_base);
          efm_icon_keep_aspect_set(o, EINA_TRUE);
          efm_icon_file_set(o, icon + 5);
        }
      else if (!strncmp(icon, "/", 1))
        {
          o = efm_icon_add(o_base);
          efm_icon_keep_aspect_set(o, EINA_TRUE);
          efm_icon_file_set(o, icon);
        }
      else if (!strncmp(icon, "thumb:", 6))
        {
          o = efm_icon_add(o_base);
          efm_icon_keep_aspect_set(o, EINA_TRUE);
          efm_icon_thumb_set(o, icon + 6);
        }
      else if (!strncmp(icon, "video:", 6))
        {
          o = efm_icon_add(o_base);
          efm_icon_keep_aspect_set(o, EINA_TRUE);
          efm_icon_video_set(o, icon + 6);
        }
      else if (!strncmp(icon, "fdo:", 4))
        {
          o = elm_icon_add(o_base);
          elm_icon_standard_set(o, icon + 4);
        }
      else if (!strncmp(icon, "edje:", 5))
        {
          char *dup = strdup(icon);

          if (dup)
            {
              char *delim = strchr(dup, '|'); // | == magic delimiter char
              if (delim)
                {
                  *delim = 0; // break string at delim /path/file.edj|group
                  o      = edje_object_add(evas_object_evas_get(o_base));
                  edje_object_file_set(o, dup + 5, delim + 1);
                }
              free(dup);
            }
        }
      // XXX: what if its an alpha thumb - needs color multiply by fg!
      if (o)
        {
          evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
          evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
          elm_table_pack(o_table, o, icon_col, 0, 1, 1);
          evas_object_show(o);
        }
    }
  else if (icon_num)
    { // other icons in the list, but not this one - so dummy rect
      o = evas_object_rectangle_add(evas_object_evas_get(o_base));
      elm_table_pack(o_table, o, icon_col, 0, 1, 1);
    }
  return o_table;
}

static void _menu_create_walk(void *data, Evas_Object *obj, const Efm_Menu *m,
                              Evas_Object *o_menu, Elm_Object_Item *it_parent)
{
  Elm_Object_Item *it;
  Evas_Object     *o, *o_radio = NULL, *o_table;
  int              i, radioval = 0;
  int              icon_num = 0, checkradio_num = 0, do_table, icon_col, radcheck_col;

  // count columns for icon and radio
  for (i = 0; i < m->item_num; i++)
    {
      const Efm_Menu_Item *mi = &(m->item[i]);

      if (mi->type == EFM_MENU_ITEM_CHECK) checkradio_num++;
      else if (mi->type == EFM_MENU_ITEM_RADIO) checkradio_num++;
      if (mi->icon) icon_num++;
    }
  do_table = checkradio_num + icon_num;
  for (i = 0; i < m->item_num; i++)
    {
      const Efm_Menu_Item *mi = &(m->item[i]);

      icon_col = 0;
      radcheck_col = 0;
      if (checkradio_num > 0) icon_col = 1;
      switch (mi->type)
        {
        case EFM_MENU_ITEM_NORMAL:
          it = elm_menu_item_add(o_menu, it_parent, NULL, mi->label,
                                 _cb_menu_item, mi);
          if (do_table)
            {
              o_table
                = _menu_icon_radcheck_add(o_menu, icon_col, mi->icon, icon_num);
              elm_object_item_content_set(it, o_table);
            }
          o_radio = NULL;
          break;
        case EFM_MENU_ITEM_SUBMENU:
          it = elm_menu_item_add(o_menu, it_parent, NULL, mi->label,
                                 NULL, NULL);
          if (do_table)
            {
              o_table
              = _menu_icon_radcheck_add(o_menu, icon_col, mi->icon, icon_num);
              elm_object_item_content_set(it, o_table);
            }
          _menu_create_walk(data, obj, mi->sub, o_menu, it);
          o_radio = NULL;
          break;
        case EFM_MENU_ITEM_SEPARATOR:
          elm_menu_item_separator_add(o_menu, it_parent);
          o_radio = NULL;
          break;
        case EFM_MENU_ITEM_CHECK:
          it = elm_menu_item_add(o_menu, it_parent, NULL, mi->label,
                                 _cb_menu_item, mi);
          if (do_table)
            {
              o_table
                = _menu_icon_radcheck_add(o_menu, icon_col, mi->icon, icon_num);
              o = elm_check_add(o_menu);
              elm_check_selected_set(o, mi->selected);
              elm_table_pack(o_table, o, radcheck_col, 0, 1, 1);
              evas_object_show(o);
              elm_object_item_content_set(it, o_table);
            }
          o_radio = NULL;
          break;
        case EFM_MENU_ITEM_RADIO:
          it = elm_menu_item_add(o_menu, it_parent, NULL, mi->label,
                                 _cb_menu_item, mi);
          if (do_table)
            {
              o_table
                = _menu_icon_radcheck_add(o_menu, icon_col, mi->icon, icon_num);
              o = elm_radio_add(o_menu);
              if (!o_radio)
                {
                  o_radio  = o;
                  radioval = 0;
                }
              else elm_radio_group_add(o, o_radio);
              elm_radio_state_value_set(o, radioval);
              if (mi->selected) elm_radio_value_set(o_radio, radioval);
              elm_table_pack(o_table, o, radcheck_col, 0, 1, 1);
              evas_object_show(o);
              elm_object_item_content_set(it, o_table);
              radioval++;
            }
          break;
        default:
          break;
        }
    }
}

void *
efm_menu_provider_default(void *data, void *menu_data, Evas_Object *obj, const Efm_Menu *m,
                          Evas_Coord x, Evas_Coord y)
{
  if (m)
    {
      Evas_Object *o, *o_top;

      o_top  = efm_scroller_get(obj);
      if (o_top) o_top = elm_object_top_widget_get(o_top);
      else o_top = elm_object_top_widget_get(obj);
      o      = elm_menu_add(o_top);
      if (o)
        {
          evas_object_data_set(o, "efm", obj);
          _menu_create_walk(data, obj, m, o, NULL);
          evas_object_smart_callback_add(o, "dismissed", _cb_menu_dismissed,
                                         obj);
          elm_menu_move(o, x, y);
          evas_object_show(o);
          elm_menu_open(o);
          return o;
        }
    }
  else
    {
      if (menu_data) evas_object_del(menu_data);
    }
  return NULL;
}
