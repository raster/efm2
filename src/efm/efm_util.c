#include "cmd.h"
#include "efm.h"
#include "efm_util.h"
#include "efm_icon.h"
#include "efm_graph.h"
#include "efm_dnd.h"
#include "efm_back_end.h"
#include "efm_private.h"
#include "eina_strbuf.h"
#include "eina_types.h"
#include "mimeapps.h"

// util funcs for the efm view
static inline int
_xtov(char x)
{
  if ((x >= '0') && (x <= '9')) return x - '0';
  if ((x >= 'a') && (x <= 'f')) return 10 + (x - 'a');
  if ((x >= 'A') && (x <= 'F')) return 10 + (x - 'A');
  return 0;
}

static unsigned int
_xtoi(const char *str)
{
  unsigned int v = 0;
  const char  *s;

  for (s = str; *s; s++)
    {
      v <<= 4;
      v += _xtov(*s);
    }
  return v;
}

char *
_escape_parse(const char *str)
{
  char       *dest = malloc(strlen(str) + 1);
  char       *d;
  const char *s;

  for (d = dest, s = str; *s; d++)
    {
      if ((s[0] == '%') && (!isspace(s[1])))
        {
          if ((s[1]) && (s[2]))
            {
              *d = (_xtov(s[1]) << 4) | (_xtov(s[2]));
              s += 3;
            }
          else s++;
        }
      else
        {
          *d = s[0];
          s++;
        }
    }
  *d = 0;
  return dest;
}

static void
_strbuf_escape_append(Eina_Strbuf *strbuf, const char *str)
{
  const char  hex[] = "0123456789abcdef";
  const char *s;

  for (s = str; *s; s++)
    {
      if ((*s <= ',') || (*s == '%') || ((*s >= ':') && (*s <= '@'))
          || ((*s >= '[') && (*s <= '`')) || (*s >= '{'))
        {
          eina_strbuf_append_char(strbuf, '%');
          eina_strbuf_append_char(strbuf, hex[(*s >> 4) & 0xf]);
          eina_strbuf_append_char(strbuf, hex[*s & 0xf]);
        }
      else eina_strbuf_append_char(strbuf, *s);
    }
}

double
_scale_get(Smart_Data *sd)
{
  double s1 = elm_object_scale_get(sd->o_scroller);
  double s2 = elm_config_scale_get();
  return s1 * s2;
}

Eina_List *
_icons_path_find(const char *path)
{
  Eina_List   *l, *bl, *il;
  Block       *block;
  Evas_Object *obj;
  Eina_Strbuf *strbuf;
  Icon        *icon  = NULL;
  Eina_List   *icons = NULL;

  strbuf = eina_strbuf_new();
  if (!strbuf) return NULL;

  EINA_LIST_FOREACH(_efm_list, l, obj)
  {
    ENTRY icons;

    EINA_LIST_FOREACH(sd->blocks, bl, block)
    {
      EINA_LIST_FOREACH(block->icons, il, icon)
      {
        eina_strbuf_reset(strbuf);
        eina_strbuf_append(strbuf, sd->config.path);
        eina_strbuf_append(strbuf, icon->info.file);
        if (!strcmp(path, eina_strbuf_string_get(strbuf)))
          icons = eina_list_append(icons, icon);
      }
    }
  }
  eina_strbuf_free(strbuf);
  return icons;
}

Eina_Bool
_selected_icons_uri_strbuf_append(Smart_Data *sd, Eina_Strbuf *strbuf)
{
  Eina_List *bl, *il;
  Block     *block;
  Icon      *icon;
  Eina_Bool  added = EINA_FALSE;

  // walk all files and add the selected ones to the uri list buf
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (block->selected_num == 0) continue; // skip blocks with 0 sel
    EINA_LIST_FOREACH(block->icons, il, icon)
    {
      if (!icon->selected) continue; // skip icons not selected
      eina_strbuf_append(strbuf, "file://");
      _strbuf_escape_append(strbuf, icon->sd->config.path);
      _strbuf_escape_append(strbuf, icon->info.file);
      eina_strbuf_append_char(strbuf, '\n');
      added = EINA_TRUE;
    }
  }
  return added;
}

void
_detail_realized_items_resize(Smart_Data *sd)
{
  Eina_List   *bl, *il;
  Block       *block;
  Icon        *icon;
  Evas_Object *o;
  int          i;
  char         buf[128];

  if (sd->config.view_mode != EFM_VIEW_MODE_LIST_DETAILED) return;
  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (block->realized_num == 0) continue; // skip non-realized
    EINA_LIST_FOREACH(block->icons, il, icon)
    {
      if (!icon->realized) continue; // skip non-realized
      for (i = 0; i < 6; i++)
        {
          snprintf(buf, sizeof(buf), "e.swallow.detail%i", i + 1);
          o = icon->o_list_detail_swallow[i];
          evas_object_size_hint_min_set(
            o, sd->config.detail_min_w[i] * _scale_get(sd), 0);
          edje_object_part_swallow(icon->o_base, buf, o);
        }
    }
  }
}

static Icon *
_icon_find_closest(Eina_List *list, Evas_Coord x, Evas_Coord y, Icon *not_icon,
                   Efm_Focus_Dir dir)
{
  Eina_List *l;
  Icon      *icon, *closest = NULL;
  Evas_Coord closest_dist = 0x7fffffff, ix, iy, dx, dy, dist;

  EINA_LIST_FOREACH(list, l, icon)
  {
    if (icon == not_icon) continue;
    ix = icon->geom.x + (icon->geom.w / 2);
    iy = icon->geom.y + (icon->geom.h / 2);
    switch (dir)
      {
      case EFM_FOCUS_DIR_UP:
      case EFM_FOCUS_DIR_PGUP:
        if (iy >= y) continue;
        break;
      case EFM_FOCUS_DIR_DOWN:
      case EFM_FOCUS_DIR_PGDN:
        if (iy <= y) continue;
        break;
      case EFM_FOCUS_DIR_LEFT:
        if (ix >= x) continue;
        break;
      case EFM_FOCUS_DIR_RIGHT:
        if (ix <= x) continue;
        break;
      default:
        break;
      }
    dx   = x - ix;
    dy   = y - iy;
    dist = (dx * dx) + (dy * dy);
    if (dist < closest_dist)
      {
        closest      = icon;
        closest_dist = dist;
      }
  }
  return closest;
}

void
_efm_focus_position(Smart_Data *sd)
{
  Evas_Coord ix, iy, iw, ih, ix2, iy2, iw2, ih2;

  if (!sd->last_focused) return;
  ix2 = ix = sd->last_focused->geom.x;
  iy2 = iy = sd->last_focused->geom.y;
  iw2 = iw = sd->last_focused->geom.w;
  ih2 = ih = sd->last_focused->geom.h;
  if (sd->last_focused_before)
    {
      ix = sd->last_focused_before->geom.x;
      iy = sd->last_focused_before->geom.y;
      iw = sd->last_focused_before->geom.w;
      ih = sd->last_focused_before->geom.h;
    }
  if (sd->focus_pos < 1.0)
    {
      ix2 = ((ix * (1.0 - sd->focus_pos)) + (ix2 * sd->focus_pos));
      iy2 = ((iy * (1.0 - sd->focus_pos)) + (iy2 * sd->focus_pos));
      iw2 = ((iw * (1.0 - sd->focus_pos)) + (iw2 * sd->focus_pos));
      ih2 = ((ih * (1.0 - sd->focus_pos)) + (ih2 * sd->focus_pos));
    }
  evas_object_geometry_set(sd->o_focus, sd->geom.x + ix2, sd->geom.y + iy2, iw2,
                           ih2);
}

Eina_Rectangle
_efm_sel_rect_get(Smart_Data *sd)
{
  Evas_Coord     x1, y1, x2, y2;
  Eina_Rectangle rect;

  if (sd->sel_x1 < sd->sel_x2)
    {
      x1 = sd->sel_x1;
      x2 = sd->sel_x2;
    }
  else
    {
      x1 = sd->sel_x2;
      x2 = sd->sel_x1;
    }
  if (sd->sel_y1 < sd->sel_y2)
    {
      y1 = sd->sel_y1;
      y2 = sd->sel_y2;
    }
  else
    {
      y1 = sd->sel_y2;
      y2 = sd->sel_y1;
    }
  rect.x = x1;
  rect.y = y1;
  rect.w = x2 - x1 + 1;
  rect.h = y2 - y1 + 1;
  return rect;
}

void
_efm_sel_position(Smart_Data *sd)
{
  Eina_Rectangle rect;

  if (!sd->sel_show) return;
  rect = _efm_sel_rect_get(sd);
  evas_object_geometry_set(sd->o_sel, sd->geom.x + rect.x, sd->geom.y + rect.y,
                           rect.w, rect.h);
}

static Eina_Bool
_cb_focus_anim(void *data)
{
  Smart_Data *sd = data;

  sd->focus_pos
    = (ecore_loop_time_get() - sd->focus_start_time) / FOCUS_ANIM_TIME;
  if (sd->focus_pos < 0.0) sd->focus_pos = 0.0;
  _efm_focus_position(sd);
  if (sd->focus_pos >= 1.0)
    {
      sd->focus_animator = NULL;
      return EINA_FALSE;
    }
  return EINA_TRUE;
}

void
_icon_focus(Smart_Data *sd)
{
  Evas_Coord ix, iy, iw, ih;

  if ((!sd->last_focused) && (!sd->last_focused_before)) return;
  if (!sd->focus_show)
    { // just show the focus now
      if (sd->last_focused)
        {
          sd->focus_show = EINA_TRUE;
          ix             = sd->last_focused->geom.x;
          iy             = sd->last_focused->geom.y;
          iw             = sd->last_focused->geom.w;
          ih             = sd->last_focused->geom.h;
          evas_object_geometry_set(sd->o_focus, sd->geom.x + ix,
                                   sd->geom.y + iy, iw, ih);
          edje_object_signal_emit(sd->o_focus, "elm,action,focus,show", "elm");
          evas_object_raise(sd->o_focus);
          evas_object_show(sd->o_focus);
          sd->last_focused_before = sd->last_focused;
        }
    }
  else
    { // transition focus to new place
      if (sd->last_focused_before)
        {
          ix = sd->last_focused_before->geom.x;
          iy = sd->last_focused_before->geom.y;
          iw = sd->last_focused_before->geom.w;
          ih = sd->last_focused_before->geom.h;
        }
      else if (sd->last_focused)
        {
          ix = sd->last_focused->geom.x;
          iy = sd->last_focused->geom.y;
          iw = sd->last_focused->geom.w;
          ih = sd->last_focused->geom.h;
        }
      else
        {
          evas_object_hide(sd->o_focus);
          return;
        }
      evas_object_geometry_set(sd->o_focus, sd->geom.x + ix, sd->geom.y + iy,
                               iw, ih);
      edje_object_signal_emit(sd->o_focus, "elm,action,focus,show", "elm");
      evas_object_raise(sd->o_focus);
      evas_object_show(sd->o_focus);
      if (!sd->focus_animator)
        sd->focus_animator = ecore_animator_add(_cb_focus_anim, sd);
    }
  if (sd->o_scroller)
    {
      if (sd->last_focused)
        {
          ix = sd->last_focused->geom.x;
          iy = sd->last_focused->geom.y;
          iw = sd->last_focused->geom.w;
          ih = sd->last_focused->geom.h;
          elm_scroller_region_bring_in(sd->o_scroller, ix, iy, iw, ih);
        }
    }
}

Eina_Bool
_icon_focus_dir(Smart_Data *sd, Efm_Focus_Dir dir)
{
  Eina_List *bl, *il, *realized = NULL;
  Icon      *icon;
  Block     *block;
  Evas_Coord ix, iy, iw, ih, vh;

  if (!sd->icons) return EINA_FALSE;
  if (!sd->last_focused)
    {
      EINA_LIST_FOREACH(sd->blocks, bl, block)
      {
        if (block->realized_num > 0)
          {
            EINA_LIST_FOREACH(block->icons, il, icon)
            {
              if (icon->realized) realized = eina_list_append(realized, icon);
            }
          }
      }
      icon = NULL;

      switch (dir)
        {
        case EFM_FOCUS_DIR_UP:
        case EFM_FOCUS_DIR_PGUP: // bottom right
          icon
            = _icon_find_closest(realized, sd->geom.w, sd->geom.h, NULL, dir);
          break;
        case EFM_FOCUS_DIR_DOWN:
        case EFM_FOCUS_DIR_RIGHT:
        case EFM_FOCUS_DIR_PGDN: // top left
          icon = _icon_find_closest(realized, 0, 0, NULL, dir);
          break;
        case EFM_FOCUS_DIR_LEFT: // top right
          icon = _icon_find_closest(realized, sd->geom.w, 0, NULL, dir);
          break;
        default:
          break;
        }
      eina_list_free(realized);
      if (!icon) return EINA_FALSE;
      printf("focus %s\n", icon->info.file);
      sd->last_focused_before = NULL;
      sd->last_focused        = icon;
      sd->focus_pos           = 1.0;
    }
  else
    {
      ix   = sd->last_focused->geom.x;
      iy   = sd->last_focused->geom.y;
      iw   = sd->last_focused->geom.w;
      ih   = sd->last_focused->geom.h;
      icon = NULL;
      switch (dir)
        {
        case EFM_FOCUS_DIR_UP:
          if (!((sd->last_focused)
                && (sd->last_focused == sd->icons->data))) // first item
            {
              icon = _icon_find_closest(sd->icons, ix + (iw / 2), iy,
                                        sd->last_focused, dir);
            }
          break;
        case EFM_FOCUS_DIR_DOWN:
          if (!((sd->last_focused)
                && (sd->last_focused
                    == eina_list_last(sd->icons)->data))) // last item
            {
              icon = _icon_find_closest(sd->icons, ix + (iw / 2), iy + ih,
                                        sd->last_focused, dir);
            }
          break;
        case EFM_FOCUS_DIR_LEFT:
          if (!((sd->last_focused)
                && (sd->last_focused == sd->icons->data))) // first item
            {
              icon = _icon_find_closest(sd->icons, ix, iy + (ih / 2),
                                        sd->last_focused, dir);
              if (!icon)
                {
                  EINA_LIST_FOREACH(sd->icons, il, icon)
                  {
                    if (icon == sd->last_focused)
                      {
                        if (il->prev) icon = il->prev->data;
                        break;
                      }
                  }
                }
            }
          break;
        case EFM_FOCUS_DIR_RIGHT:
          if (!((sd->last_focused)
                && (sd->last_focused
                    == eina_list_last(sd->icons)->data))) // last item
            {
              icon = _icon_find_closest(sd->icons, ix + iw, iy + (ih / 2),
                                        sd->last_focused, dir);
              if (!icon)
                {
                  EINA_LIST_FOREACH(sd->icons, il, icon)
                  {
                    if (icon == sd->last_focused)
                      {
                        if (il->next) icon = il->next->data;
                        break;
                      }
                  }
                }
            }
          break;
        case EFM_FOCUS_DIR_PGUP:
          if (!((sd->last_focused)
                && (sd->last_focused == sd->icons->data))) // first item
            {
              vh = 0;
              if (sd->o_scroller)
                evas_object_geometry_get(sd->o_scroller, NULL, NULL, NULL, &vh);
              icon = _icon_find_closest(sd->icons, ix + (iw / 2), iy - vh,
                                        sd->last_focused, dir);
              if (!icon) icon = sd->icons->data;
              printf("pu %p\n", icon);
            }
          break;
        case EFM_FOCUS_DIR_PGDN:
          if (!((sd->last_focused)
                && (sd->last_focused
                    == eina_list_last(sd->icons)->data))) // last item
            {
              vh = 0;
              if (sd->o_scroller)
                evas_object_geometry_get(sd->o_scroller, NULL, NULL, NULL, &vh);
              icon = _icon_find_closest(sd->icons, ix + (iw / 2), iy + ih + vh,
                                        sd->last_focused, dir);
              if (!icon) icon = eina_list_last(sd->icons)->data;
              printf("pd %p + %i\n", icon, vh);
            }
          break;
        default:
          break;
        }
      if (!icon) return EINA_FALSE;
      printf("focus %s\n", icon->info.file);
      sd->last_focused_before = sd->last_focused;
      sd->last_focused        = icon;
      sd->focus_pos           = 0.0;
      sd->focus_start_time    = ecore_loop_time_get();
    }

  _icon_focus(sd);
  return EINA_TRUE;
}

void
_icon_focus_show(Smart_Data *sd)
{
  if (sd->focus_show) return;
  sd->focus_show = EINA_TRUE;
  evas_object_show(sd->o_focus);
}

void
_icon_focus_hide(Smart_Data *sd)
{
  if (!sd->focus_show) return;
  sd->focus_show = EINA_FALSE;
  evas_object_hide(sd->o_focus);
}

void
_icon_object_clear(Icon *icon)
{
  int i;

  // XXX: have a cache for base and icon objects to avoid re-creating them
  for (i = 0; i < 6; i++)
    {
      if (icon->o_list_detail_swallow[i])
        {
          evas_object_del(icon->o_list_detail_swallow[i]);
          icon->o_list_detail_swallow[i] = NULL;
        }
      if (icon->o_list_detail_swallow2[i])
        {
          evas_object_del(icon->o_list_detail_swallow2[i]);
          icon->o_list_detail_swallow2[i] = NULL;
        }
    }
  if (icon->o_icon)
    {
      evas_object_del(icon->o_icon);
      icon->o_icon = NULL;
    }
  if (icon->o_entry)
    {
      evas_object_del(icon->o_entry);
      icon->o_entry = NULL;
    }
  if (icon->o_base)
    {
      evas_object_del(icon->o_base);
      icon->o_base = NULL;
    }
  if ((icon->over) && (icon->sd)) evas_object_hide(icon->sd->o_over);
}

static void
_icon_text_update(Icon *icon)
{ // update the label text to be the file or correct label string
  const char *str = NULL;
  char       *txt = NULL;

  if (!icon->o_base) return;
  if (icon->down)
    {
      if (icon->info.label_clicked) str = icon->info.label_clicked;
    }
  if ((icon->selected) && (!str))
    {
      if (icon->info.label_selected) str = icon->info.label_selected;
    }
  if (!str)
    {
      if (icon->info.label) str = icon->info.label;
      else if (icon->info.file) str = icon->info.file;
    }
  if (!str) str = "???";
  if (icon->sd->config.view_mode >= EFM_VIEW_MODE_LIST) txt = strdup(str);
  else txt = elm_entry_utf8_to_markup(str);
  edje_object_part_text_set(icon->o_base, "e.text.label", txt);
  free(txt);
}

void
_icon_rename_end(Icon *icon)
{
  if (icon->sd->rename_icon != icon) return;
  icon->sd->rename_icon = NULL;
  printf("end renaming icon %s\n", icon->info.file);
  icon->renaming = EINA_FALSE;
  evas_object_del(icon->o_entry);
  icon->o_entry = NULL;
  elm_object_focus_set(icon->sd->o_scroller, EINA_TRUE);
}

static void
_cb_entry_aborted(void *data, Evas_Object *obj EINA_UNUSED,
                  void *event_info EINA_UNUSED)
{
  Icon *icon = data;

  _icon_rename_end(icon);
}

static void
_cb_entry_activated(void *data, Evas_Object *obj EINA_UNUSED,
                    void *event_info EINA_UNUSED)
{
  Icon       *icon = data;
  const char *txt;
  char       *utf;

  txt = elm_object_text_get(icon->o_entry);
  if (txt)
    {
      utf = elm_entry_markup_to_utf8(txt);
      if (utf)
        {
          Eina_Strbuf *buf = cmd_strbuf_new("file-rename");

          _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
          cmd_strbuf_append(buf, "new-name", utf);
          cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
          printf("XXX: rename to [%s]\n", utf);
          free(utf);
        }
    }
  _icon_rename_end(icon);
}

static void
_icon_rename_begin(Icon *icon)
{
  Evas_Object *o;
  char        *mkup;

  if (icon->sd->rename_icon) _icon_rename_end(icon->sd->rename_icon);
  if (icon->sd->drag) return;
  icon->sd->rename_icon = icon;
  icon->renaming        = EINA_TRUE;
  icon->o_entry = o = elm_entry_add(icon->sd->o_scroller);
  elm_entry_single_line_set(o, EINA_TRUE);
  elm_entry_scrollable_set(o, EINA_TRUE);
  elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_OFF);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  // s = elm_entry_markup_to_utf8(s);
  mkup = elm_entry_utf8_to_markup(icon->info.file);
  elm_object_text_set(o, mkup);
  free(mkup);
  evas_object_smart_callback_add(o, "aborted", _cb_entry_aborted, icon);
  evas_object_smart_callback_add(o, "activated", _cb_entry_activated, icon);
  edje_object_part_swallow(icon->o_base, "e.swallow.entry", o);
  evas_object_show(o);
  elm_object_focus_set(o, EINA_TRUE);
}

static Eina_Bool
_cb_icon_longpress_timer(void *data)
{
  Icon *icon = data;

  icon->longpress_timer = NULL;
  printf("XXX: Long press [%s]\n", icon->info.file);
  return EINA_FALSE;
}

Eina_Bool
_file_video_is(const char *file)
{
  // XXX: this probably should look up in config
  const char *videxts[]
    = { "webm", "ogv",  "mp4",    "m4v", "mpg", "mp2",  "mpeg",  "mpeg2",
        "avi",  "flv",  "wmv",    "f4v", "swf", "mkv",  "avchd", "mov",
        "3gp",  "3g2",  "prores", "vob", "mts", "m2ts", "ts",    "qt",
        "rm",   "rmvb", "viv",    "asf", "amv", "m2v",  "mpe",   "mpv",
        "svi",  "mxf",  "nsv",    "f4p", "f4b", NULL };
  const char *ext = strrchr(file, '.');
  int         i;

  if (!ext) return EINA_FALSE;
  for (i = 0; videxts[i]; i++)
    {
      if (!strcasecmp(ext + 1, videxts[i])) return EINA_TRUE;
    }
  return EINA_FALSE;
}

static void
_icon_file_set(Icon *icon, const char *file)
{
  if (_file_video_is(file))
    efm_icon_video_set(icon->o_icon, file);
  else
    efm_icon_file_set(icon->o_icon, file);
}

void
_icon_path_cmd_strbuf_append(Eina_Strbuf *strbuf, const char *key,
                             Smart_Data *sd, Icon *icon)
{
  Eina_Strbuf *buf = eina_strbuf_new();

  if (!buf) return;
  eina_strbuf_append(buf, sd->config.path);
  eina_strbuf_append(buf, icon->info.file);
  cmd_strbuf_append(strbuf, key, eina_strbuf_string_get(buf));
  eina_strbuf_free(buf);
}

static Eina_Bool
_file_direct_run_get(Icon *icon)
{
  if ((!icon->info.dir) && (!icon->info.special))
    { // not dir or special node
      const char *f = cmd_key_find(icon->cmd, "mode");

      if (f)
        { // get mode strig hex and chexck execute bits
          int mode = strtoull(f, NULL, 16);

          if (mode & (S_IXUSR | S_IXGRP | S_IXOTH)) return EINA_TRUE;
        }
    }
  return EINA_FALSE;
}

static Eina_Bool
_file_desktop_exec_get(Icon *icon)
{
  Eina_Bool ret = EINA_FALSE;

  if (!strcmp(icon->info.mime, "application/x-desktop"))
    {
      char buf[PATH_MAX];
      Efreet_Desktop *d;

      snprintf(buf, sizeof(buf), "%s%s", icon->sd->config.path, icon->info.file);
      d = efreet_desktop_new(buf);
      if (d)
        {
          if ((d->exec) && (d->exec[0])) ret = EINA_TRUE;
          efreet_desktop_unref(d);
        }
    }
  return ret;
}

void
_icon_open_with_cmd_strbuf_append(Eina_Strbuf *strbuf, const char *key,
                                  Smart_Data *sd)
{
  Eina_List *bl, *il;
  Block     *block;
  Icon      *icon;

  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (block->selected_num == 0) continue; // skip blocks with 0 sel
    EINA_LIST_FOREACH(block->icons, il, icon)
    {
      if (!icon->selected) continue; // skip icons not selected
      // FIXME: for now just pick the first found and use its mime. later
      // let's use the most common mme type instead amongst all selected
      // files
      if ((!icon->info.mime)
          || (!strcmp(icon->info.mime, "application/x-executable"))
          || (!strcmp(icon->info.mime, "application/x-shellscript")))
        {
          if (_file_direct_run_get(icon))
            {
              // tell whoever is opening to direct execute
              cmd_strbuf_append(strbuf, key, "..EXECUTE..");
              printf("FFF: direct exec [%s] [%s]\n", icon->info.file,
                     icon->info.mime);
              return;
            }
          continue;
        }
      if (_file_desktop_exec_get(icon))
        {
          cmd_strbuf_append(strbuf, key, "..DESKTOP..");
          printf("FFF: [%s]: desktop [%s]\n", icon->info.file, icon->info.mime);
          return;
        }
      printf("FFF: [%s]: [%s]\n", icon->info.file, icon->info.mime);
      Efreet_Desktop *d = _mimeapps_handler_find(icon->info.mime);
      if (!d) continue;
      // open-with=/path/to/file.desktop
      cmd_strbuf_append(strbuf, key, d->orig_path);
      efreet_desktop_unref(d);
      return;
    }
  }
}

void
_uri_list_cmd_strbuf_append(Eina_Strbuf *strbuf, const char *key,
                            const char *urilist)
{
  char  *tmps = strdup(urilist), *se, *s2, *s;
  size_t len;

  if (!tmps) return;
  for (s = tmps; *s; s = s2)
    {
      se = strchr(s, '\n');
      if (!se)
        {
          len = strlen(s);
          se  = s + len;
          s2  = NULL;
        }
      else s2 = se + 1;
      *se = '\0';
      cmd_strbuf_append(strbuf, key, s);
      if (!s2) break;
    }
  free(tmps);
}

static void
_cb_icon_mouse_down(void *data, Evas *e EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED, void *event_info)
{
  Evas_Event_Mouse_Down *ev   = event_info;
  Icon                  *icon = data;
  Evas_Coord             x, y;

  if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) return;
  if ((icon->sd->rename_icon) && (icon->sd->rename_icon != icon))
    _icon_rename_end(icon->sd->rename_icon);
  elm_object_focus_set(icon->sd->o_scroller, EINA_TRUE);
  icon->sd->key_control         = EINA_FALSE;
  icon->sd->last_focused        = NULL;
  icon->sd->last_focused_before = NULL;
  icon->sd->just_dragged        = EINA_FALSE;
  _icon_focus_hide(icon->sd);
  if (ev->button == 1)
    { // left mouse
      evas_object_raise(icon->o_base);
      if (icon->longpress_timer) ecore_timer_del(icon->longpress_timer);
      icon->longpress_timer
        = ecore_timer_add(ICON_LONGPRESS_TIMER, _cb_icon_longpress_timer, icon);
      icon->down = EINA_TRUE;
      if (ev->flags == EVAS_BUTTON_NONE)
        { // a click - not double-clicked. regular click
          icon->down_x = ev->canvas.x;
          icon->down_y = ev->canvas.y;
          evas_object_geometry_get(icon->o_base, &x, &y, NULL, NULL);
          icon->down_rel_x = icon->down_x - x;
          icon->down_rel_y = icon->down_y - y;
          if (!icon->info.thumb)
            {
              if (icon->edje)
                edje_object_signal_emit(icon->o_icon, "e,state,clicked", "e");
              else if (icon->info.icon_clicked)
                _icon_file_set(icon, icon->info.icon_clicked);
            }
          _icon_text_update(icon);
          // XXX; start longpress timer
        }
      else if (ev->flags == EVAS_BUTTON_DOUBLE_CLICK)
        { // double clicked
          Eina_Strbuf *buf    = cmd_strbuf_new("file-run");
          Eina_Strbuf *strbuf = eina_strbuf_new();

          printf("XXX: DBL\n");
          if (strbuf)
            {
              _icon_path_cmd_strbuf_append(buf, "over", icon->sd, icon);
              _icon_open_with_cmd_strbuf_append(buf, "open-with", icon->sd);
              _selected_icons_uri_strbuf_append(icon->sd, strbuf);
              _uri_list_cmd_strbuf_append(buf, "path",
                                          eina_strbuf_string_get(strbuf));
              cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
              eina_strbuf_free(strbuf);
            }
        }
    }
  else if (ev->button == 3)
    { // right mouse
      evas_object_raise(icon->o_base);
      if (ev->flags == EVAS_BUTTON_NONE)
        { // a click - not double-clicked. regular click
          icon->down   = EINA_TRUE;
          icon->down_x = ev->canvas.x;
          icon->down_y = ev->canvas.y;
          evas_object_geometry_get(icon->o_base, &x, &y, NULL, NULL);
          icon->down_rel_x = icon->down_x - x;
          icon->down_rel_y = icon->down_y - y;
        }
    }
}

static void
_icon_select_update(Icon *icon)
{
  int i;

  if (!icon->o_base) return;
  if (icon->selected)
    {
      edje_object_signal_emit(icon->o_base, "e,state,selected", "e");
      if (icon->edje)
        edje_object_signal_emit(icon->o_icon, "e,state,selected", "e");
      else if (icon->info.icon_selected)
        _icon_file_set(icon, icon->info.icon_selected);
      for (i = 0; i < 6; i++)
        {
          if ((icon->o_list_detail_swallow2[i])
              && (evas_object_data_get(icon->o_list_detail_swallow2[i],
                                       "is_edje")))
            edje_object_signal_emit(icon->o_list_detail_swallow2[i],
                                    "e,state,selected", "e");
        }
    }
  else
    {
      edje_object_signal_emit(icon->o_base, "e,state,unselected", "e");
      if (icon->edje)
        edje_object_signal_emit(icon->o_icon, "e,state,normal", "e");
      else if ((icon->info.pre_lookup_icon)
               && (icon->info.pre_lookup_icon[0] == '/'))
        _icon_file_set(icon, icon->info.pre_lookup_icon);
      for (i = 0; i < 6; i++)
        {
          if ((icon->o_list_detail_swallow2[i])
              && (evas_object_data_get(icon->o_list_detail_swallow2[i],
                                       "is_edje")))
            edje_object_signal_emit(icon->o_list_detail_swallow2[i],
                                    "e,state,unselected", "e");
        }
    }
  _icon_text_update(icon);
}

static Eina_Bool
_cb_dnd_over_open_timer(void *data)
{
  Smart_Data *sd = data;

  sd->dnd_over_open_timer = NULL;
  if (sd->drop_over)
    {
      Eina_Strbuf *buf = cmd_strbuf_new("dnd-hover");

      printf("XXX: open hover dir [%s]\n", sd->drop_over->info.file);
      _icon_path_cmd_strbuf_append(buf, "path", sd, sd->drop_over);
      cmd_strbuf_exe_consume(buf, sd->exe_open);
      // XXX: open dir smart callback
    }
  return EINA_FALSE;
}

void
_icon_over_off(Icon *icon)
{
  icon->over = EINA_FALSE;
  edje_object_signal_emit(icon->sd->o_over, "e,state,unselected", "e");
  evas_object_hide(icon->sd->o_over);
  if (icon->sd->dnd_over_open_timer)
    {
      ecore_timer_del(icon->sd->dnd_over_open_timer);
      icon->sd->dnd_over_open_timer = NULL;
    }
}

void
_icon_over_on(Icon *icon)
{
  if (icon->over) return;
  if (icon->sd->over_icon)
    {
      _icon_over_off(icon->sd->over_icon);
      icon->sd->over_icon = NULL;
    }
  if (!icon->info.dir) return;
  if ((icon->sd->drag_icon) && (icon->info.file)
      && (icon->sd->drag_icon->info.file)
      && (!strcmp(icon->sd->drag_icon->info.file, icon->info.file)))
    return;
  if (icon->sd->dnd_over_open_timer)
    ecore_timer_del(icon->sd->dnd_over_open_timer);
  icon->sd->dnd_over_open_timer
    = ecore_timer_add(DND_OVER_OPEN_TIMER, _cb_dnd_over_open_timer, icon->sd);
  icon->sd->over_icon = icon;
  icon->over          = EINA_TRUE;
  edje_object_signal_emit(icon->sd->o_over, "e,state,selected", "e");
  evas_object_raise(icon->sd->o_over);
  evas_object_show(icon->sd->o_over);
  evas_object_geometry_set(icon->sd->o_over, icon->sd->geom.x + icon->geom.x,
                           icon->sd->geom.y + icon->geom.y, icon->geom.w,
                           icon->geom.h);
}

void
_icon_select(Icon *icon)
{
  if (icon->selected) return;
  icon->selected = EINA_TRUE;
  icon->block->selected_num++;
  icon->sd->last_selected = icon;
  _icon_select_update(icon);
  if (icon->sd)
    {
      Eina_Strbuf *buf = cmd_strbuf_new("file-selected");
      _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
      cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
    }
}

void
_icon_unselect(Icon *icon)
{
  if (!icon->selected) return;
  if (icon->sd->last_selected == icon) icon->sd->last_selected = NULL;
  icon->selected = EINA_FALSE;
  icon->block->selected_num--;
  _icon_select_update(icon);
  if (icon->sd)
    {
      Eina_Strbuf *buf = cmd_strbuf_new("file-unselected");
      _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
      cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
    }
}

Eina_Bool
_unselect_all(Smart_Data *sd)
{
  Eina_List *bl, *il;
  Icon      *icon;
  Block     *block;
  Eina_Bool  had_selected = EINA_FALSE;

  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (block->selected_num > 0)
      {
        had_selected = EINA_TRUE;
        EINA_LIST_FOREACH(block->icons, il, icon) { _icon_unselect(icon); }
      }
  }
  return had_selected;
}

void
_select_range(Icon *icon_from, Icon *icon_to)
{
  Eina_List *l;
  Icon      *icon;
  Eina_Bool  sel = EINA_FALSE;

  EINA_LIST_FOREACH(icon_from->sd->icons, l, icon)
  {
    if (!sel)
      {
        if ((icon_from == icon) || (icon_to == icon))
          {
            _icon_select(icon);
            sel = EINA_TRUE;
          }
      }
    else
      {
        _icon_select(icon);
        if ((icon_from == icon) || (icon_to == icon)) return;
      }
  }
}

static void
_cb_icon_mouse_up(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                  void *event_info)
{
  Evas_Event_Mouse_Up *ev   = event_info;
  Icon                *icon = data;
  Evas_Coord           dx, dy;
  Eina_Bool            dragged = EINA_FALSE;

  if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) goto done;
  if ((ev->button == 1) && (icon->sd->sel_show))
    _efm_sel_end(icon->sd);
  else if ((ev->button == 1) && (icon->down))
    { // click + release
      dx = ev->canvas.x - icon->down_x;
      dy = ev->canvas.y - icon->down_y;
      if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
      if (icon->longpress_timer)
        {
          ecore_timer_del(icon->longpress_timer);
          icon->longpress_timer = NULL;
        }
      if (!dragged)
        {
          Eina_Strbuf *buf = cmd_strbuf_new("file-clicked");

          printf("XXX: mouse clicked\n");
          _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
          cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
          if (evas_key_modifier_is_set(ev->modifiers, "Shift"))
            { // range select
              if (!icon->selected)
                {
                  if (icon->sd->last_selected)
                    _select_range(icon->sd->last_selected, icon);
                  else _icon_select(icon);
                }
            }
          else if (evas_key_modifier_is_set(ev->modifiers, "Control"))
            { // multi-single select toggle
              if (!icon->selected) _icon_select(icon);
              else _icon_unselect(icon);
            }
          else
            { // select just one file so unselect previous files
              _unselect_all(icon->sd);
              if (!icon->selected) _icon_select(icon);
              else _icon_unselect(icon);
            }
        }
      else _icon_select_update(icon);
      icon->sd->last_focused = icon;
      icon->drag             = EINA_FALSE;
    }
  else if ((ev->button == 3) && (icon->down))
    { // right mouse click
      // XXX: handle right mouse click on an icon
      dx = ev->canvas.x - icon->down_x;
      dy = ev->canvas.y - icon->down_y;
      if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
      if (!dragged)
        {
          printf("XXX: right mouse\n");
          _efm_popup_icon_menu_add(icon->sd, icon, icon->down_x,
                                   icon->down_y);
        }
      icon->sd->last_focused = icon;
      icon->drag             = EINA_FALSE;
    }
  _icon_focus_hide(icon->sd);
done:
  icon->down = EINA_FALSE;
}

Icon *
_icon_dup(Icon *icon)
{
  Icon *icon2;

  icon2 = calloc(1, sizeof(Icon));
  if (!icon2) return NULL;
  icon2->geom     = icon->geom;
  icon2->sd       = icon->sd;
  icon2->down_x   = icon->down_x;
  icon2->down_y   = icon->down_y;
  icon2->selected = icon->selected;
  icon2->down     = icon->down;
  icon2->edje     = icon->edje;
  icon2->info     = icon->info;
  icon2->last_x   = icon->last_x;
  icon2->last_y   = icon->last_y;
#define DUPSTRSHARE(X) \
  if (icon->info.X) icon2->info.X = eina_stringshare_add(icon->info.X)
  DUPSTRSHARE(file);
  DUPSTRSHARE(label);
  DUPSTRSHARE(label_selected);
  DUPSTRSHARE(label_clicked);
  DUPSTRSHARE(mime);
  DUPSTRSHARE(icon);
  DUPSTRSHARE(icon_selected);
  DUPSTRSHARE(icon_clicked);
  DUPSTRSHARE(mime_icon);
  DUPSTRSHARE(pre_lookup_icon);
  DUPSTRSHARE(thumb);
#undef DUPSTRSHARE
  icon2->cmd = cmd_dup(icon->cmd);
  return icon2;
}

static Eina_Bool
_cb_sel_bounds_scroll_timer(void *data)
{
  Smart_Data *sd = data;
  Evas_Coord  sx, sy, sw, sh;

  if (!sd->o_scroller) return EINA_TRUE;
  evas_object_geometry_get(sd->o_scroller, &sx, &sy, &sw, &sh);
  if ((sd->back_x < sx) || (sd->back_y < sy) || (sd->back_x >= (sx + sw))
      || (sd->back_y >= (sy + sh)))
    {
      elm_scroller_region_bring_in(sd->o_scroller, sd->sel_x2, sd->sel_y2, 1,
                                   1);
    }
  return EINA_TRUE;
}

void
_efm_sel_store_start(Smart_Data *sd, Evas_Coord x, Evas_Coord y)
{
  sd->sel_x1 = x - sd->geom.x;
  sd->sel_y1 = y - sd->geom.y;
  sd->back_x = x;
  sd->back_y = y;
}

void
_efm_sel_store_end(Smart_Data *sd, Evas_Coord x, Evas_Coord y)
{
  sd->sel_x2 = x - sd->geom.x;
  sd->sel_y2 = y - sd->geom.y;
  sd->back_x = x;
  sd->back_y = y;
}

void
_efm_sel_start(Smart_Data *sd)
{
  if (sd->sel_show) return;
  sd->sel_show = EINA_TRUE;
  _icon_focus_hide(sd);
  evas_object_show(sd->o_sel);
  evas_object_raise(sd->o_sel);
  if (!sd->scroll_timer)
    sd->scroll_timer
      = ecore_timer_add(SCROLL_SEL_TIMER, _cb_sel_bounds_scroll_timer, sd);
}

void
_efm_sel_end(Smart_Data *sd)
{
  Eina_List     *bl, *il;
  Icon          *icon;
  Block         *block;
  Eina_Rectangle r = _efm_sel_rect_get(sd);

  if (!sd->sel_show) return;
  sd->sel_show = EINA_FALSE;

  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (eina_rectangles_intersect(&r, &(block->bounds)))
      {
        EINA_LIST_FOREACH(block->icons, il, icon)
        {
          if (eina_rectangles_intersect(&r, &(icon->geom))) _icon_select(icon);
        }
      }
  }
  evas_object_hide(sd->o_sel);
  if (sd->scroll_timer)
    {
      ecore_timer_del(sd->scroll_timer);
      sd->scroll_timer = NULL;
    }
}

static void
_cb_icon_mouse_move(void *data, Evas *e EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED, void *event_info)
{
  Evas_Event_Mouse_Move *ev   = event_info;
  Icon                  *icon = data;
  Evas_Coord             dx, dy;
  Eina_Bool              dragged = EINA_FALSE;
  Efm_Action             action  = EFM_ACTION_MOVE;

    if (!icon->down) return;
  dx = ev->cur.canvas.x - icon->down_x;
  dy = ev->cur.canvas.y - icon->down_y;
  if (((dx * dx) + (dy * dy)) > (5 * 5)) dragged = EINA_TRUE;
  if (!dragged) return;
  if (ev->buttons & (1 << 0)) // left mouse pressed
    {
      Smart_Data *sd = icon->sd;

      if (sd->sel_show)
        {
          _efm_sel_store_end(sd, ev->cur.canvas.x, ev->cur.canvas.y);
          _efm_sel_position(sd);
          return;
        }
      else if ((evas_key_modifier_is_set(ev->modifiers, "Control")) ||
               (evas_key_modifier_is_set(ev->modifiers, "Shift")))
        { // start shift/ctrl drag band
          if (!sd->sel_show)
            {
              _efm_sel_store_start(sd, ev->cur.canvas.x, ev->cur.canvas.y);
              _efm_sel_store_end(sd, ev->cur.canvas.x, ev->cur.canvas.y);
              _efm_sel_start(sd);
            }
          _efm_sel_position(sd);
          return;
        }
    }
  if (icon->sd->drag) return;
  if (icon->longpress_timer)
    {
      ecore_timer_del(icon->longpress_timer);
      icon->longpress_timer = NULL;
    }
  // mouse has been dragged
  if (!icon->selected)
    {
      if ((!evas_key_modifier_is_set(ev->modifiers, "Shift"))
          && (!evas_key_modifier_is_set(ev->modifiers, "Control")))
        _unselect_all(icon->sd);
      _icon_select(icon);
    }
  icon->down = EINA_FALSE;
  _icon_select_update(icon);
  if (icon->renaming) return;
  printf("DDD: drag!!!!\n");
  icon->last_x = ev->cur.canvas.x;
  icon->last_y = ev->cur.canvas.y;
  icon->drag   = EINA_TRUE;
  // begin the drag with that list of files
  // XXX: if you use no modifier - do default (move)
  // XXX: if you hold ctrl then do copy
  // XXX: on win ... to/from removable drive it copies always
  // XXX: if you hold shift then do move
  // XXX; on mac option key forces a copy normally except from
  //      removable device then it forces a move
  // XXX: this "except to/from removable drive" is bad/inconsistent imho
  if (ev->buttons & (1 << 2)) // right mouse pressed
    action = EFM_ACTION_ASK;
  _drag_start(icon, action);
}

static void
_icon_resized(Icon *icon)
{
  int w, h;

  // get the icon size and if it has alpha
  efm_icon_size_get(icon->o_icon, &w, &h);
  // set the aspect ratio hint based on the above and re-swallow
  evas_object_size_hint_aspect_set(icon->o_icon, EVAS_ASPECT_CONTROL_BOTH, w,
                                   h);
  if ((efm_icon_mono_get(icon->o_icon))
      && (edje_object_part_exists(icon->o_base, "e.swallow.icon_mono")))
    edje_object_part_swallow(icon->o_base, "e.swallow.icon_mono", icon->o_icon);
  else edje_object_part_swallow(icon->o_base, "e.swallow.icon", icon->o_icon);
}

static void
_cb_icon_resized(void *data, Evas_Object *obj EINA_UNUSED,
                 void *info EINA_UNUSED)
{ // the icon image/thumb has been loaded so update the icon base
  Icon *icon = data;

  _icon_resized(icon);
}

static void
_cb_icon_loaded(void *data, Evas_Object *obj EINA_UNUSED,
                void *info EINA_UNUSED)
{ // the icon image/thumb has been loaded so update the icon base
  Icon     *icon = data;
  Eina_Bool alpha;

  alpha = efm_icon_alpha_get(icon->o_icon);
  _icon_resized(icon);
  if (icon->info.thumb)
    { // if it's a thumbnail - let's emit to show it
      if (alpha)
        edje_object_signal_emit(icon->o_base, "e,action,thumb,gen,alpha", "e");
      else edje_object_signal_emit(icon->o_base, "e,action,thumb,gen", "e");
    }
}

static void
_cb_icon_label_longpress(void *data, Evas_Object *obj EINA_UNUSED,
                         const char *emission EINA_UNUSED,
                         const char *source   EINA_UNUSED)
{
  Icon *icon = data;

  if ((icon->sd->drag) || (icon->sd->just_dragged)) return;
  _icon_rename_begin(icon);
}

static int
_icon_detail_size_display(unsigned long long size, char unit[8])
{
#define K 1024ULL
  unsigned long long sizetbl[] = { K,
                                   0,
                                   0,
                                   K * K,
                                   (K)-1,
                                   10,
                                   K * K * K,
                                   (K * K) - 1,
                                   20,
                                   K * K * K * K,
                                   (K * K * K) - 1,
                                   30,
                                   K * K * K * K * K,
                                   (K * K * K * K) - 1,
                                   40,
                                   K * K * K * K * K * K,
                                   (K * K * K * K * K) - 1,
                                   50,
                                   K * K * K * K * K * K * K,
                                   (K * K * K * K * K * K) - 1,
                                   60 };
#undef K
  char *sizes = "bKMGTPE"; /* "ZY" (Zetta, Yotta) not handled - beyond 64b */
  int   i, i3;

  unit[0] = '?';
  for (i = 0; i < 7; i++)
    {
      i3 = i * 3;
      if (size < sizetbl[(i3)])
        {
          size = (size + sizetbl[i3 + 1]) >> sizetbl[i3 + 2];
          strncpy(unit, &(sizes[i]), 1);
          break;
        }
    }
  unit[1] = '\0';
  return (int)size; // int is fine as we reduced the value to unit size
}

static void
_icon_detail_text_set(Icon *icon, int col, const char *detail)
{
  char buf[128];

  snprintf(buf, sizeof(buf), "e.text.detail%i", col + 1);
  if (detail) edje_object_part_text_set(icon->o_base, buf, detail);
}

static Evas_Object *
_icon_detail_rectangle_add(Icon *icon, Smart_Data *sd, Evas *e, int col,
                           const char *detail)
{
  Evas_Object *o;
  char         buf[128];

  _icon_detail_text_set(icon, col, detail);
  icon->o_list_detail_swallow[col] = o = evas_object_rectangle_add(e);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_color_set(o, 0, 0, 0, 0);
  evas_object_size_hint_min_set(
    o, sd->config.detail_min_w[col] * _scale_get(sd), 0);
  snprintf(buf, sizeof(buf), "e.swallow.detail%i", col + 1);
  edje_object_part_swallow(icon->o_base, buf, o);
  return o;
}

static Evas_Object *
_icon_detail_grid_add(Icon *icon, Smart_Data *sd, int col)
{
  Evas_Object *o;
  char         buf[128];

  icon->o_list_detail_swallow[col] = o = elm_grid_add(sd->o_scroller);
  elm_grid_size_set(o, 1, 1);
  evas_object_size_hint_min_set(
    o, sd->config.detail_min_w[col] * _scale_get(sd), 0);
  snprintf(buf, sizeof(buf), "e.swallow.detail%i", col + 1);
  edje_object_part_swallow(icon->o_base, buf, o);
  return o;
}

static Evas_Object *
_icon_detail_grid_sub_edje_add(Icon *icon, Evas *e, const char *theme_edj_file,
                               int col, const char *group)
{
  Evas_Object *o;

  icon->o_list_detail_swallow2[col] = o = edje_object_add(e);
  edje_object_file_set(o, theme_edj_file, group);
  evas_object_data_set(o, "is_edje", o);
  if (icon->selected) edje_object_signal_emit(o, "e,state,selected", "e");
  else edje_object_signal_emit(o, "e,state,unselected", "e");
  elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
  evas_object_pass_events_set(o, EINA_TRUE);
  evas_object_show(o);
  return o;
}

static void
_icon_detail_edje_text_set_free(Evas_Object *o, const char *part, char *str)
{
  edje_object_part_text_set(o, part, str);
  free(str);
}

static char *
_cb_progress_noval(double v EINA_UNUSED)
{
  return "";
}

static void
_cb_icon_detail_check_changed(void *data, Evas_Object *o,
                              void *info EINA_UNUSED)
{
  Icon        *icon  = data;
  Eina_Strbuf *buf   = cmd_strbuf_new("file-detail-change");
  const char  *state = "false";
  char         detail[32];
  int          v;

  _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
  v = (int)(unsigned long)evas_object_data_get(o, "detail");
  snprintf(detail, sizeof(detail), "detail%i", v);
  cmd_strbuf_append(buf, "detail", detail);
  if (elm_check_state_get(o)) state = "true";
  cmd_strbuf_append(buf, "state", state);
  cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
}

static void
_cb_icon_detail_button_clicked(void *data, Evas_Object *o,
                               void *info EINA_UNUSED)
{
  Icon        *icon = data;
  Eina_Strbuf *buf  = cmd_strbuf_new("file-detail-clicked");
  char         detail[32];
  int          v;

  _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
  v = (int)(unsigned long)evas_object_data_get(o, "detail");
  snprintf(detail, sizeof(detail), "detail%i", v);
  cmd_strbuf_append(buf, "detail", detail);
  cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
}

static void
_cb_icon_detail_segment_changed(void *data, Evas_Object *o, void *info)
{
  Icon            *icon = data;
  Eina_Strbuf     *buf  = cmd_strbuf_new("file-detail-select");
  int              v;
  char             detail[32];
  Elm_Object_Item *it = info;
  int              n  = (int)(unsigned long)elm_object_item_data_get(it);

  _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
  v = (int)(unsigned long)evas_object_data_get(o, "detail");
  snprintf(detail, sizeof(detail), "detail%i", v);
  cmd_strbuf_append(buf, "detail", detail);
  snprintf(detail, sizeof(detail), "%i", n);
  cmd_strbuf_append(buf, "item", detail);
  cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
}

static void
_cb_icon_detail_popdown_changed(void *data, Evas_Object *o, void *info)
{
  Icon            *icon = data;
  Eina_Strbuf     *buf  = cmd_strbuf_new("file-detail-select");
  int              v;
  char             detail[32];
  Elm_Object_Item *it = info;
  int              n  = (int)(unsigned long)elm_object_item_data_get(it);

  _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
  v = (int)(unsigned long)evas_object_data_get(o, "detail");
  snprintf(detail, sizeof(detail), "detail%i", v);
  cmd_strbuf_append(buf, "detail", detail);
  snprintf(detail, sizeof(detail), "%i", n);
  cmd_strbuf_append(buf, "item", detail);
  cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
}

static void
_cb_icon_detail_slider_changed(void *data, Evas_Object *o,
                               void *info EINA_UNUSED)
{
  Icon        *icon = data;
  Eina_Strbuf *buf  = cmd_strbuf_new("file-detail-change");
  char         detail[32];
  int          v;
  double       val, val2;

  _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
  v = (int)(unsigned long)evas_object_data_get(o, "detail");
  snprintf(detail, sizeof(detail), "detail%i", v);
  cmd_strbuf_append(buf, "detail", detail);
  if (elm_slider_range_enabled_get(o))
    {
      elm_slider_range_get(o, &val, &val2);
      snprintf(detail, sizeof(detail), "%i", (int)val);
      cmd_strbuf_append(buf, "value", detail);
      snprintf(detail, sizeof(detail), "%i", (int)val2);
      cmd_strbuf_append(buf, "value2", detail);
    }
  else
    {
      val = elm_slider_value_get(o);
      snprintf(detail, sizeof(detail), "%i", (int)val);
      cmd_strbuf_append(buf, "value", detail);
    }
  cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
}

static void
_cb_icon_detail_slider_mouse_down(void *data, Evas *e EINA_UNUSED,
                                  Evas_Object *obj EINA_UNUSED,
                                  void            *event_info)
{
  Evas_Event_Mouse_Down *ev   = event_info;
  Icon                  *icon = data;

  if (ev->button == 1) evas_object_raise(icon->o_base);
}

static void
_icon_detail_icon_set(Evas_Object *o, const char *icon, const char *dir)
{
  if (!strncmp(icon, "std:", 4)) elm_icon_standard_set(o, icon + 4);
  else
    {
      if (icon[0] == '/') efm_icon_file_set(o, icon);
      else
        {
          Eina_Strbuf *strbuf = eina_strbuf_new();

          eina_strbuf_append(strbuf, dir);
          eina_strbuf_append(strbuf, icon);
          efm_icon_file_set(o, eina_strbuf_string_get(strbuf));
          eina_strbuf_free(strbuf);
        }
    }
}

static Evas_Object *
_icon_detail_icon_add(Evas_Object *base, const char *icon)
{
  Evas_Object *o;

  if (!strncmp(icon, "std:", 4))
    {
      o = elm_icon_add(base);
      evas_object_pass_events_set(o, EINA_TRUE);
    }
  else
    {
      o = efm_icon_add(base);
      efm_icon_keep_aspect_set(o, EINA_TRUE);
      evas_object_pass_events_set(o, EINA_TRUE);
    }
  return o;
}

static Eina_Bool
_icon_detail_bool_parse(const char *str)
{
  if ((!strcasecmp(str, "1")) || (!strcasecmp(str, "on"))
      || (!strcasecmp(str, "true")) || (!strcasecmp(str, "yes")))
    return EINA_TRUE;
  return EINA_FALSE;
}

static void
_icon_detail_elm_object_prepare(Evas_Object *o, int col)
{
  elm_object_focus_allow_set(o, EINA_FALSE);
  evas_object_data_set(o, "detail", (void *)(unsigned long)(col + 1));
  evas_object_propagate_events_set(o, EINA_FALSE);
}

static void
_icon_detail_size_update(Evas_Object *o, const char *detail)
{
  char               buf[128];
  char             **plist;
  unsigned long long size, size_max;
  double             sz;

  plist = eina_str_split(detail, "/", 2);
  if (!plist) return;
  if (!(plist[0] && plist[1])) goto done;
  size     = atoll(plist[0]);
  size_max = atoll(plist[1]);
  if (size_max <= 0) size_max = 1;
  sz = (double)size / (double)size_max;
  // counteract _size_message doins a sqrt for show
  sz = sz * sz;
  _size_message(o, sz);

  size = _icon_detail_size_display(size, buf);
  edje_object_part_text_set(o, "e.text.unit", buf);
  snprintf(buf, sizeof(buf), "%i", (int)size);
  edje_object_part_text_set(o, "e.text.label", buf);

done:
  free(*plist);
  free(plist);
}

void
_icon_detail_add(Icon *icon, Smart_Data *sd, Evas *e,
                 const char *theme_edj_file, int col, const char *detail,
                 const char *format)
{ // can call this for add AND update - working on update now
  Evas_Object *o, *o2;

  if (!detail) detail = "";
  if (!strcmp(format, "text")) // format: just the string as-is
    {
      if (!icon->o_list_detail_swallow[col])
        _icon_detail_rectangle_add(icon, sd, e, col, detail);
      else _icon_detail_text_set(icon, col, detail);
    }
  else if (!strcmp(format, "size"))
    { // format: "13/28" = value/max_value
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, col,
                                             "e/fileman/default/filesize");
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      _icon_detail_size_update(o, detail);
    }
  else if (!strcmp(format, "timestamp"))
    { // format: "1284924" = time_in_seconds_since_epoch
      time_t     tmpt = atoll(detail);
      struct tm *info;

      info = localtime(&tmpt);
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, col,
                                             "e/fileman/default/filedate");
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      _icon_detail_edje_text_set_free(o, "e.text.year",
                                      eina_strftime("%y", info));
      _icon_detail_edje_text_set_free(o, "e.text.month",
                                      eina_strftime("%b", info));
      _icon_detail_edje_text_set_free(o, "e.text.day",
                                      eina_strftime("%d", info));
      _icon_detail_edje_text_set_free(o, "e.text.time",
                                      eina_strftime("%H:%M:%S", info));
    }
  else if (!strcmp(format, "checkview"))
    { // format: "true"/"yes"/"1" or "false"/"no"/"0"
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, col,
                                             "e/fileman/default/check");
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      if (_icon_detail_bool_parse(detail))
        edje_object_signal_emit(o, "e,state,on", "e");
      else edje_object_signal_emit(o, "e,state,off", "e");
    }
  else if (!strcmp(format, "progress"))
    { // format: "33/100" or "noval,41/50" = value / maxvalue optional noval
      int val = 0, max = 100;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = elm_progressbar_add(o);
          _icon_detail_elm_object_prepare(o, col);
          elm_progressbar_horizontal_set(o, EINA_TRUE);
          elm_progressbar_span_size_set(o, 1);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      if (sscanf(detail, "%i/%i", &val, &max) == 2)
        {
          if (max < 1) max = 0;
        }
      else if (sscanf(detail, "noval,%i/%i", &val, &max) == 2)
        {
          elm_progressbar_unit_format_function_set(o, _cb_progress_noval, NULL);
          if (max < 1) max = 0;
        }
      if (val < 0) val = 0;
      elm_progressbar_value_set(o, (double)val / (double)max);
      evas_object_show(o);
    }
  else if (!strcmp(format, "circle"))
    { // format: "#fff" or "#ffff" or "#ffffff" or "#ffffffff" = rgb(a)
      Edje_Message_String msg;

      msg.str = (char *)detail;
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, col,
                                             "e/fileman/default/circle");
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      edje_object_message_send(o, EDJE_MESSAGE_STRING, 1, &msg);
      edje_object_message_signal_process(o);
    }
  else if (!strcmp(format, "icon"))
    { // format: std:clock or /path/x.png or path/x.gif or x.gif
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = _icon_detail_icon_add(o, detail);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_show(o);
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      _icon_detail_icon_set(o, detail, sd->config.path);
    }
  else if (!strcmp(format, "check"))
    { // format: "true"/"yes"/"1" or "false"/"no"/"0"
      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          // use table to pack check into to force centering and put it in
          // the grid
          o2 = elm_table_add(o);
          elm_grid_pack(icon->o_list_detail_swallow[col], o2, 0, 0, 1, 1);
          o = elm_check_add(o);
          _icon_detail_elm_object_prepare(o, col);
          elm_table_pack(o2, o, 0, 0, 1, 1);
          evas_object_smart_callback_add(o, "changed",
                                         _cb_icon_detail_check_changed, icon);
          evas_object_show(o2);
          evas_object_show(o);
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      elm_check_state_set(o, _icon_detail_bool_parse(detail));
    }
  else if (!strcmp(format, "button"))
    { // format: "Label" or "Label!icon" = icon above
      char *label, *sep;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = elm_button_add(o);
          _icon_detail_elm_object_prepare(o, col);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_smart_callback_add(o, "clicked",
                                         _cb_icon_detail_button_clicked, icon);
          evas_object_show(o);
          icon->o_list_detail_swallow3[col] = o;
        }
      o     = icon->o_list_detail_swallow3[col];
      label = strdup(detail);
      if (label)
        {
          sep = strchr(label, '!');
          if (sep)
            {
              *sep++ = 0;
              o2     = elm_object_part_content_get(o, "icon");
              if (!o2) o2 = _icon_detail_icon_add(o, sep);
              _icon_detail_icon_set(o2, sep, sd->config.path);
              elm_object_part_content_set(o, "icon", o2);
              evas_object_show(o2);
            }
          elm_object_text_set(o, label);
          free(label);
        }
    }
  else if (!strcmp(format, "segmentsel"))
    { // format: "Opt1,*Opt2" or "*Opt!icon,Opt2" = icon above, *= SELECTED
      char           **plist, **p, *sep, *label;
      Elm_Object_Item *it;
      Eina_Bool        sel;
      int              n;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = elm_segment_control_add(o);
          _icon_detail_elm_object_prepare(o, col);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_smart_callback_add(o, "changed",
                                         _cb_icon_detail_segment_changed, icon);
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      n = elm_segment_control_item_count_get(o);
      while (n > 0)
        { // delete all existing items - brute force re-fill
          elm_segment_control_item_del_at(o, 0);
          n--;
        }
      plist = eina_str_split(detail, ",", -1);
      for (p = plist, n = 0; *p != NULL; p++, n++)
        {
          sel = EINA_FALSE;
          o2  = NULL;
          sep = strchr(*p, '!');
          if (sep)
            {
              *sep++ = 0;
              o2     = _icon_detail_icon_add(o, sep);
              _icon_detail_icon_set(o2, sep, sd->config.path);
            }
          label = *p;
          if (**p == '*')
            {
              label++;
              sel = EINA_TRUE;
            }
          if (*label) it = elm_segment_control_item_add(o, o2, label);
          else it = elm_segment_control_item_add(o, o2, NULL);
          elm_object_item_data_set(it, (void *)(unsigned long)n);
          if (sel) elm_segment_control_item_selected_set(it, EINA_TRUE);
          if (o2) evas_object_show(o2);
        }
      free(*plist);
      free(plist);
      evas_object_show(o);
    }
  else if (!strcmp(format, "popdown"))
    { // format: "Opt1,*Opt2" or "*Opt!icon,Opt2" = icon above, *= SELECTED
      char           **plist, **p, *sep, *label, *icpath;
      Elm_Object_Item *it;
      Eina_Bool        sel;
      Elm_Icon_Type    ictype;
      char             buf[PATH_MAX];
      int              n;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = elm_hoversel_add(o);
          elm_hoversel_auto_update_set(o, EINA_TRUE);
          elm_hoversel_hover_parent_set(o, sd->o_scroller);
          _icon_detail_elm_object_prepare(o, col);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_smart_callback_add(o, "selected",
                                         _cb_icon_detail_popdown_changed, icon);
          icon->o_list_detail_swallow3[col] = o;
        }
      o = icon->o_list_detail_swallow3[col];
      // brute force - clean hoversel if it is updated
      elm_hoversel_clear(o);
      plist = eina_str_split(detail, ",", -1);
      for (p = plist, n = 0; *p != NULL; p++, n++)
        {
          sel    = EINA_FALSE;
          o2     = NULL;
          icpath = NULL;
          ictype = ELM_ICON_NONE;
          sep    = strchr(*p, '!');
          if (sep)
            {
              *sep++ = 0;
              if (!strncmp(sep, "std:", 4))
                {
                  snprintf(buf, sizeof(buf), "%s", sep + 4);
                  icpath = buf;
                  ictype = ELM_ICON_STANDARD;
                }
              else if (sep[0] == '/')
                {
                  snprintf(buf, sizeof(buf), "%s", sep);
                  icpath = buf;
                  ictype = ELM_ICON_FILE;
                }
              else
                {
                  snprintf(buf, sizeof(buf), "%s%s", sd->config.path, sep);
                  icpath = buf;
                  ictype = ELM_ICON_FILE;
                }
            }
          label = *p;
          if (**p == '*')
            {
              label++;
              sel = EINA_TRUE;
            }
          if (**p)
            it = elm_hoversel_item_add(o, label, icpath, ictype, NULL, NULL);
          else it = elm_hoversel_item_add(o, label, icpath, ictype, NULL, NULL);
          elm_object_item_data_set(it, (void *)(unsigned long)n);
          if (sel) elm_object_text_set(o, label);
        }
      free(*plist);
      free(plist);
      evas_object_show(o);
    }
  else if (!strcmp(format, "slider"))
    { // format: "min/max/val" or "min/max/val/val2"
      char             **plist;
      unsigned long long min, max, val, val2;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = elm_slider_add(o);
          evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN,
                                         _cb_icon_detail_slider_mouse_down,
                                         icon);
          _icon_detail_elm_object_prepare(o, col);
          evas_object_smart_callback_add(o, "changed",
                                         _cb_icon_detail_slider_changed, icon);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_show(o);
          icon->o_list_detail_swallow3[col] = o;
        }
      o     = icon->o_list_detail_swallow3[col];
      plist = eina_str_split(detail, "/", 4);
      if (plist[0] && plist[1] && plist[2])
        {
          min = atoll(plist[0]);
          max = atoll(plist[1]);
          val = atoll(plist[2]);

          elm_slider_min_max_set(o, min, max);
          elm_slider_unit_format_set(o, "%1.0f");
          if (plist[3])
            {
              val2 = atoll(plist[3]);
              elm_slider_range_enabled_set(o, EINA_TRUE);
              elm_slider_range_set(o, val, val2);
            }
          else
            {
              elm_slider_range_enabled_set(o, EINA_FALSE);
              elm_slider_value_set(o, val);
            }
        }
      free(*plist);
      free(plist);
    }
  else if (!strcmp(format, "bargraph"))
    { // format: "0-100,13,23,80,100,78,17,0,35,57" = min-max,val1,val2,...
      // format: "#ff3810ff,0-100,10,20"
      // format: "cc::selected,0-100,10,20"
      // format: "#ffee4466,0-100,65,85,100,93|#ff882266,0-100,20,55,90,30"
      char **plist2, **p2, **plist, **p, *cc;
      int    min, max, num, *vals, start;

      if (!icon->o_list_detail_swallow[col])
        { // only add if not there
          o = _icon_detail_grid_add(icon, sd, col);
          o = efm_graph_add(o);
          elm_grid_pack(icon->o_list_detail_swallow[col], o, 0, 0, 1, 1);
          evas_object_show(o);
          icon->o_list_detail_swallow3[col] = o;
        }
      o      = icon->o_list_detail_swallow3[col];
      plist2 = eina_str_split(detail, "|", -1);
      for (p2 = plist2; *p2; p2++)
        {
          plist = eina_str_split(*p2, ",", -1);
          if (plist[0] && plist[1])
            {
              start = 0;
              cc    = NULL;
              if ((plist[0][0] == '#') || (plist[0][0] == 'c'))
                {
                  start++;
                  cc = plist[0];
                }
              if (sscanf(plist[start], "%i-%i", &min, &max) == 2)
                {
                  for (num = -1, p = plist + start; *p; p++) num++;

                  if (num > 0)
                    {
                      vals = malloc(num * sizeof(int));
                      for (num = -1, p = plist + start; *p; p++)
                        {
                          if (num >= 0) vals[num] = atoi(*p);
                          num++;
                        }
                      if (cc) efm_graph_colorspec_set(o, cc);
                      efm_graph_values_set(o, num, vals, min, max);
                      free(vals);
                    }
                }
            }
          free(*plist);
          free(plist);
        }
      free(*plist2);
      free(plist2);
    }
  else
    fprintf(stderr, "Uknown format for file '%s' column %i format '%s'\n",
            icon->info.file, col, format);
}

void
_icon_object_add(Icon *icon, Smart_Data *sd, Evas *e,
                 const char *theme_edj_file, Eina_Bool clip_set, int num)
{ // either image or theme element
  Evas_Object *o, *o2;
  const char  *icon_file = NULL, *icon_group = NULL, *icon_thumb = NULL;
  const char  *ic;
  char         buf[PATH_MAX], buf2[128];
  int          i;

  if (icon->o_base) return;
  icon_thumb = icon->info.thumb;
  if ((!icon->info.thumb) && (icon->info.pre_lookup_icon))
    { // we're being asked for a std theme icon
      ic = icon->info.pre_lookup_icon;
      if (icon->down)
        {
          if (icon->info.icon_clicked) ic = icon->info.icon_clicked;
        }
      else if (icon->selected)
        {
          if (icon->info.icon_selected) ic = icon->info.icon_selected;
        }
      if (ic[0] == '/') icon_file = ic;
      else
        {
          snprintf(buf, sizeof(buf), "e/icons/%s", ic);
          icon_file = elm_theme_group_path_find(NULL, buf);
          if (icon_file) icon_group = buf;
        }
    }
  if ((!icon_file) && (!icon_thumb))
    { // theme icon doesn't exit or other reason
      // use this icon file
      if (icon->info.icon) icon_file = icon->info.icon;
      else
        { // try a mime type std icon
          snprintf(buf, sizeof(buf), "e/icons/fileman/mime/%s",
                   icon->info.mime);
          icon_file = elm_theme_group_path_find(NULL, buf);
          // mime type icon exists
          if (icon_file) icon_group = buf;
          else
            { // use an out-of-theme std mime icon
              if (icon->info.mime_icon) icon_file = icon->info.mime_icon;
            }
        }
    }
  if ((!icon_group) && (!icon_file) && (!icon_thumb))
    { // fallback to plain empty file when no icon found
      snprintf(buf, sizeof(buf), "e/icons/fileman/mime/inode/file");
      icon_file  = elm_theme_group_path_find(NULL, buf);
      icon_group = buf;
    }
  // about to realize if it hasn't been
  // XXX: have a cache for base and icon objects to avoid re-creating them
  icon->o_base = o = edje_object_add(e);
  if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS) ||
      (sd->config.view_mode == EFM_VIEW_MODE_ICONS_VERTICAL))
    edje_object_file_set(o, theme_edj_file, "e/fileman/default/icon/fixed");
  else if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM) ||
           (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL))
    edje_object_file_set(o, theme_edj_file, "e/fileman/desktop/icon/fixed");
  else if (sd->config.view_mode == EFM_VIEW_MODE_LIST)
    { // odd/even coloring with different group
      if (num & 0x1)
        edje_object_file_set(o, theme_edj_file,
                             "e/fileman/default/list_odd/fixed");
      else
        edje_object_file_set(o, theme_edj_file, "e/fileman/default/list/fixed");
    }
  else if (sd->config.view_mode == EFM_VIEW_MODE_LIST_DETAILED)
    {
      const char *s;

      // odd/even coloring with different group
      if (num & 0x1)
        edje_object_file_set(o, theme_edj_file,
                             "e/fileman/default/list_odd/detailed");
      else
        edje_object_file_set(o, theme_edj_file,
                             "e/fileman/default/list/detailed");
      // is this a special detail format item?
      s = cmd_key_find(icon->cmd, "detail-format");
      if (s)
        { // fromat: fmt,fmt,fmt,fmt,fmt,fmt
          // where fmt is the format of 6 detail columns and is one of:
          // text,size,timestamp,...
          char **plist, **p;

          plist = eina_str_split(s, ",", 6);
          for (p = plist, i = 0; *p != NULL; p++, i++)
            {
              snprintf(buf2, sizeof(buf2), "detail%i", i + 1);
              s = cmd_key_find(icon->cmd, buf2);
              _icon_detail_add(icon, sd, e, theme_edj_file, i, s, *p);
            }
          free(*plist);
          free(plist);
        }
      else
        { // regular detail mode with file properties
          s = cmd_key_find(icon->cmd, "size");
          if (s)
            { // special sze object
              unsigned long long size = atoll(s);

              o2 = _icon_detail_grid_add(icon, sd, 0);
              o2 = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, 0,
                                                  "e/fileman/default/filesize");
              if (sd->file_max > 0)
                _size_message(o2, (double)size / (double)sd->file_max);
              else _size_message(o2, 0.0);

              size = _icon_detail_size_display(size, buf2);
              edje_object_part_text_set(o2, "e.text.unit", buf2);
              snprintf(buf2, sizeof(buf2), "%i", (int)size);
              edje_object_part_text_set(o2, "e.text.label", buf2);
              edje_object_message_signal_process(o2);
            }
          s = cmd_key_find(icon->cmd, "mtime");
          if (s)
            { // special time + date object
              time_t     tmpt = atoll(s);
              struct tm *info;

              info = localtime(&tmpt);
              o2   = _icon_detail_grid_add(icon, sd, 1);
              o2   = _icon_detail_grid_sub_edje_add(icon, e, theme_edj_file, 1,
                                                    "e/fileman/default/filedate");
              _icon_detail_edje_text_set_free(o2, "e.text.year",
                                              eina_strftime("%y", info));
              _icon_detail_edje_text_set_free(o2, "e.text.month",
                                              eina_strftime("%b", info));
              _icon_detail_edje_text_set_free(o2, "e.text.day",
                                              eina_strftime("%d", info));
              _icon_detail_edje_text_set_free(o2, "e.text.time",
                                              eina_strftime("%H:%M:%S", info));
            }
          s = icon->info.mime;
          if (!s) s = "";
          edje_object_part_text_set(icon->o_base, "e.text.detail3", s);
          s = cmd_key_find(icon->cmd, "user");
          if (s) edje_object_part_text_set(icon->o_base, "e.text.detail4", s);
          s = cmd_key_find(icon->cmd, "group");
          if (s) edje_object_part_text_set(icon->o_base, "e.text.detail5", s);
          s = cmd_key_find(icon->cmd, "mode");
          if (s)
            { // special mode info display obj
              int mode = _xtoi(s);

              icon->o_list_detail_swallow[5] = o2
                = elm_grid_add(sd->o_scroller);
              elm_grid_size_set(o2, 1, 1);
              evas_object_size_hint_min_set(
                o2, sd->config.detail_min_w[5] * _scale_get(sd), 0);
              edje_object_part_swallow(icon->o_base, "e.swallow.detail6", o2);
              icon->o_list_detail_swallow2[5] = o2 = edje_object_add(e);
              evas_object_pass_events_set(o2, EINA_TRUE);
              edje_object_file_set(o2, theme_edj_file,
                                   "e/fileman/default/fileperms");
              evas_object_data_set(o2, "is_edje", o2);
              if (icon->selected)
                edje_object_signal_emit(o2, "e,state,selected", "e");
              else edje_object_signal_emit(o2, "e,state,unselected", "e");
              elm_grid_pack(icon->o_list_detail_swallow[5], o2, 0, 0, 1, 1);
              evas_object_show(o2);
              edje_object_signal_emit(o2, "e,type,reset", "e");
              if (S_ISREG(mode))
                edje_object_signal_emit(o2, "e,type,none", "e");
              else if (S_ISDIR(mode))
                edje_object_signal_emit(o2, "e,type,dir", "e");
              else if (S_ISLNK(mode))
                edje_object_signal_emit(o2, "e,type,link", "e");
              else if (S_ISFIFO(mode))
                edje_object_signal_emit(o2, "e,type,pipe", "e");
              else if (S_ISSOCK(mode))
                edje_object_signal_emit(o2, "e,type,socket", "e");
              else if (S_ISBLK(mode))
                edje_object_signal_emit(o2, "e,type,block", "e");
              else if (S_ISCHR(mode))
                edje_object_signal_emit(o2, "e,type,char", "e");
              edje_object_signal_emit(o2, "e,perm,reset", "e");
              if ((mode & S_ISUID))
                edje_object_signal_emit(o2, "e,perm,user,setuid", "e");
              if ((mode & S_IRUSR))
                edje_object_signal_emit(o2, "e,perm,user,read", "e");
              if ((mode & S_IWUSR))
                edje_object_signal_emit(o2, "e,perm,user,write", "e");
              if ((mode & S_IXUSR))
                edje_object_signal_emit(o2, "e,perm,user,execute", "e");
              if ((mode & S_ISGID))
                edje_object_signal_emit(o2, "e,perm,group,setuid", "e");
              if ((mode & S_IRGRP))
                edje_object_signal_emit(o2, "e,perm,group,read", "e");
              if ((mode & S_IWGRP))
                edje_object_signal_emit(o2, "e,perm,group,write", "e");
              if ((mode & S_IXGRP))
                edje_object_signal_emit(o2, "e,perm,group,execute", "e");
              if ((mode & S_IROTH))
                edje_object_signal_emit(o2, "e,perm,other,read", "e");
              if ((mode & S_IWOTH))
                edje_object_signal_emit(o2, "e,perm,other,write", "e");
              if ((mode & S_IXOTH))
                edje_object_signal_emit(o2, "e,perm,other,execute", "e");
              edje_object_message_signal_process(o2);
            }
          // other columns - fill with sizing rect to set min size on
          for (i = 2; i < 5; i++)
            _icon_detail_rectangle_add(icon, sd, e, i, NULL);
        }
    }
  edje_object_preload(o, EINA_FALSE);
  edje_object_signal_callback_add(o, "e,action,label,click", "e",
                                  _cb_icon_label_longpress, icon);
  icon->edje = EINA_FALSE;

  if ((!icon_group) && (icon_file))
    { // image file
      icon->o_icon = o = efm_icon_add(o);
      evas_object_smart_callback_add(o, "resized", _cb_icon_resized, icon);
      evas_object_smart_callback_add(o, "loaded", _cb_icon_loaded, icon);
      _icon_file_set(icon, icon_file);
    }
  else if ((icon_group) && (icon_file))
    { // theme element
      icon->o_icon = o = edje_object_add(e);
      edje_object_file_set(o, icon_file, icon_group);
      edje_object_preload(o, EINA_FALSE);
      icon->edje = EINA_TRUE;
    }
  else if (icon_thumb)
    { // thumbnail file
      icon->o_icon = o = efm_icon_add(o);
      evas_object_smart_callback_add(o, "resized", _cb_icon_resized, icon);
      evas_object_smart_callback_add(o, "loaded", _cb_icon_loaded, icon);
      efm_icon_thumb_set(o, icon_thumb);
    }
  _icon_text_update(icon);
  edje_object_part_swallow(icon->o_base, "e.swallow.icon", o);
  evas_object_show(icon->o_icon);
  if (clip_set)
    {
      evas_object_smart_member_add(icon->o_base, sd->o_smart);
      evas_object_clip_set(icon->o_base, sd->o_clip);
    }

  // selected or not state for base edje
  if (icon->selected)
    edje_object_signal_emit(icon->o_base, "e,state,selected", "e");
  else edje_object_signal_emit(icon->o_base, "e,state,unselected", "e");
  // is it a link or broken link?
  if (icon->info.broken)
    edje_object_signal_emit(icon->o_base, "e,state,broken", "e");
  else if (icon->info.link)
    edje_object_signal_emit(icon->o_base, "e,state,link", "e");
  // is it a special file type?
  if (icon->info.special)
    edje_object_signal_emit(icon->o_base, "e,type,special", "e");
  if (icon->info.thumb)
    { // it's a thumb - is it a mono or full color thumb?
      if (efm_icon_mono_get(icon->o_icon))
        {
          edje_object_signal_emit(icon->o_base, "e,type,thumb,mono", "e");
          if (edje_object_part_exists(icon->o_base, "e.swallow.icon_mono"))
            edje_object_part_swallow(icon->o_base, "e.swallow.icon_mono", o);
          evas_object_show(icon->o_icon);
        }
      else edje_object_signal_emit(icon->o_base, "e,type,thumb", "e");
    }

  // listen to mouse events
  evas_object_event_callback_add(icon->o_base, EVAS_CALLBACK_MOUSE_DOWN,
                                 _cb_icon_mouse_down, icon);
  evas_object_event_callback_add(icon->o_base, EVAS_CALLBACK_MOUSE_UP,
                                 _cb_icon_mouse_up, icon);
  evas_object_event_callback_add(icon->o_base, EVAS_CALLBACK_MOUSE_MOVE,
                                 _cb_icon_mouse_move, icon);

  evas_object_show(icon->o_base);
  icon->realized = EINA_TRUE;
  if (clip_set)
    {
      evas_object_raise(sd->o_focus);
      evas_object_raise(sd->o_sel);
    }
  if (icon->renaming)
    { // in rename mode - begin rename mode
      icon->sd->rename_icon = NULL;
      _icon_rename_begin(icon);
    }
}

void
_icon_free(Icon *icon)
{ // free an icon - not needed anymore
  if (icon->sd)
    {
      if (icon->sd->last_selected == icon) icon->sd->last_selected = NULL;
      if (icon->sd->last_focused_before == icon)
        icon->sd->last_focused_before = NULL;
      if (icon->sd->last_focused == icon) icon->sd->last_focused = NULL;
      if (icon->sd->drag_icon == icon) icon->sd->drag_icon = NULL;
      if (icon->sd->over_icon == icon)
        {
          _icon_over_off(icon);
          icon->sd->over_icon = NULL;
        }
      if (icon->sd->drop_over == icon) icon->sd->drop_over = NULL;
      if (icon->sd->rename_icon == icon) _icon_rename_end(icon);
      icon->sd->icons = eina_list_remove(icon->sd->icons, icon);
    }
  if (icon->block)
    {
      if (icon->selected) icon->block->selected_num--;
      if (icon->realized) icon->block->realized_num--;
      icon->block->icons = eina_list_remove(icon->block->icons, icon);
    }
  if (icon->longpress_timer)
    {
      ecore_timer_del(icon->longpress_timer);
      icon->longpress_timer = NULL;
    }
  _icon_object_clear(icon);
  eina_stringshare_replace(&icon->info.file, NULL);
  eina_stringshare_replace(&icon->info.label, NULL);
  eina_stringshare_replace(&icon->info.label_selected, NULL);
  eina_stringshare_replace(&icon->info.label_clicked, NULL);
  eina_stringshare_replace(&icon->info.mime, NULL);
  eina_stringshare_replace(&icon->info.icon, NULL);
  eina_stringshare_replace(&icon->info.icon_selected, NULL);
  eina_stringshare_replace(&icon->info.icon_clicked, NULL);
  eina_stringshare_replace(&icon->info.mime_icon, NULL);
  eina_stringshare_replace(&icon->info.pre_lookup_icon, NULL);
  eina_stringshare_replace(&icon->info.thumb, NULL);
  cmd_free(icon->cmd);
  icon->cmd = NULL;
  free(icon);
}

static void
_block_free_final(Block *block)
{ // remove our block from storage
  block->icons = eina_list_free(block->icons);
  free(block);
}

void
_block_free(Block *block)
{ // free a block when we're not done with it - not used when reblocking
  Eina_List *il;
  Icon      *icon;

  if (block->realized_num > 0)
    { // we have some realized icons - so clear them
      EINA_LIST_FOREACH(block->icons, il, icon)
      {
        icon->realized = EINA_FALSE;
        icon->block    = NULL;
        _icon_object_clear(icon);
      }
    }
  else
    {
      EINA_LIST_FOREACH(block->icons, il, icon) { icon->block = NULL; }
    }
  _block_free_final(block);
}

void
_cb_reblock(void *data)
{ // re-do all our blocks of icons that we divide into to avoid looking at
  // batches of icons as a group/block when recvalculating when that whole
  // group is already known to not be visible as a whole, so skip it as
  // a while thus keeping the recalculations to a reasonably small set
  // instead of everything
  Smart_Data *sd = data;
  Eina_List  *l;
  Icon       *icon;
  Block      *block;

  sd->reblock_job = NULL;
  if (sd->listing_done_reblock)
    {
      sd->listing_done_reblock = EINA_FALSE;
      _listing_done(sd);
    }
  // special - remove our blocks but leave icons realized as we rebuild
  // the block list next
  EINA_LIST_FREE(sd->blocks, block) _block_free_final(block);
  // walk all icons and figure out which blocks they belong to
  block = NULL;
  EINA_LIST_FOREACH(sd->icons, l, icon)
  {
    // if our block is full move onto making a new block
    if ((block) && (eina_list_count(block->icons) >= BLOCK_MAX)) block = NULL;
    // we don't have a block to add this icon to - so make one
    if (!block)
      {
        block = calloc(1, sizeof(Block));
        if (!block) abort();
        // add the block to our block list
        sd->blocks = eina_list_append(sd->blocks, block);
        block->sd  = sd;
      }
    // add icon to block icons
    block->icons     = eina_list_append(block->icons, icon);
    icon->block      = block;
    icon->block_list = eina_list_last(block->icons);
    // adjust block reslize num if icon is realized
    if (icon->realized) block->realized_num++;
    if (icon->selected) block->selected_num++;
  }
  // flag the obj to have had our blocks re-done so we also then re-calc
  // which blocks are visible or not and unrealize/realize icons as
  // needed etc.
  sd->reblocked = EINA_TRUE;
  evas_object_smart_changed(sd->o_smart);
}
