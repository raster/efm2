#ifndef EFM_H
#define EFM_H 1

#include <Elementary.h>

#include "efm_config.h"

typedef enum
{
  EFM_VIEW_MODE_ICONS,
  EFM_VIEW_MODE_ICONS_VERTICAL,
  EFM_VIEW_MODE_ICONS_CUSTOM,
  EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL,
  EFM_VIEW_MODE_LIST,
  EFM_VIEW_MODE_LIST_DETAILED
} Efm_View_Mode;

typedef enum
{
  EFM_SORT_MODE_FLAGS = (0xffff << 16), // upper 16 bits for flags
  // flags - apper 16 bits
  EFM_SORT_MODE_DIRS_FIRST     = (1 << 16), // show dirs first then files
  EFM_SORT_MODE_NOCASE         = (1 << 17), // don't account for case in sort
  EFM_SORT_MODE_LABEL_NOT_PATH = (1 << 18), // use label not filename
  // mask for field
  EFM_SORT_MODE_MASK = 0xffff, // lower 16 bits is the sort field
  // fields
  EFM_SORT_MODE_NAME        = (0 << 0),
  EFM_SORT_MODE_SIZE        = (1 << 0),
  EFM_SORT_MODE_DATE        = (2 << 0),
  EFM_SORT_MODE_MIME        = (3 << 0),
  EFM_SORT_MODE_USER        = (4 << 0),
  EFM_SORT_MODE_GROUP       = (5 << 0),
  EFM_SORT_MODE_PERMISSIONS = (6 << 0),
} Efm_Sort_Mode;

typedef struct _Efm_Menu      Efm_Menu;
typedef struct _Efm_Menu_Item Efm_Menu_Item;

typedef enum
{
  EFM_MENU_ITEM_NORMAL,
  EFM_MENU_ITEM_SUBMENU,
  EFM_MENU_ITEM_SEPARATOR,
  EFM_MENU_ITEM_CHECK,
  EFM_MENU_ITEM_RADIO
} Efm_Menu_Item_Type;

struct _Efm_Menu_Item
{
  Efm_Menu_Item_Type type;
  const char        *label;
  const char        *icon;
  int                index;
  Eina_Bool          selected;
  Efm_Menu          *sub;
  void              *private1; // private to efm - ignore
  void              *private2; // private to efm - ignore
  void              *private3; // private to efm - ignore
};

struct _Efm_Menu
{
  const char    *title;
  const char    *title_icon;
  int            item_num;
  Efm_Menu_Item *item;
  void          *private1; // private to efm - ignore
  void          *private2; // private to efm - ignore
  void          *private3; // private to efm - ignore
};

// call this to build a menu or to close it (menu is NULL for close) - return truw if menu is shown
typedef void *(*Efm_Menu_Provider) (void *data, void *menu_data, Evas_Object *efm, const Efm_Menu *menu, Evas_Coord x, Evas_Coord y);

Evas_Object  *efm_add(Evas_Object *parent);
void          efm_scroller_set(Evas_Object *obj, Evas_Object *scroller);
Evas_Object  *efm_scroller_get(Evas_Object *obj);
Evas_Object  *efm_detail_header_get(Evas_Object *obj);
void          efm_path_view_mode_set(Evas_Object *obj, Efm_View_Mode mode);
Efm_View_Mode efm_path_view_mode_get(Evas_Object *obj);
void          efm_path_sort_mode_set(Evas_Object *obj, Efm_Sort_Mode mode);
Efm_Sort_Mode efm_path_sort_mode_get(Evas_Object *obj);
Evas_Coord    efm_column_min_get(Evas_Object *obj, int col);
void          efm_column_min_set(Evas_Object *obj, int col, Evas_Coord w);
void          efm_column_label_set(Evas_Object *obj, int col, const char *label);
const char   *efm_column_label_get(Evas_Object *obj, int col);
Evas_Coord    efm_iconsize_get(Evas_Object *obj);
void          efm_iconsize_set(Evas_Object *obj, Evas_Coord sz);
const char   *efm_backend_get(Evas_Object *obj);
void          efm_backend_set(Evas_Object *obj, const char *backend);
void          efm_menu_provider_set(Evas_Object *obj, Efm_Menu_Provider cb, void *data);
void          efm_menu_provider_select(Evas_Object *obj, int index);

// alweays call path_Set last after setup above like setting icon size etc.
void          efm_path_set(Evas_Object *obj, const char *path);
const char   *efm_path_get(Evas_Object *obj);
void          efm_path_parent(Evas_Object *obj);

// default menu provider callback for popup menus
void         *efm_menu_provider_default(void *data, void *menu_data, Evas_Object *obj, const Efm_Menu *m, Evas_Coord x, Evas_Coord y);

#define DBG(...) EINA_LOG_DOM_DBG(_log_dom, __VA_ARGS__)
#define INF(...) EINA_LOG_DOM_INFO(_log_dom, __VA_ARGS__)
#define WRN(...) EINA_LOG_DOM_WARN(_log_dom, __VA_ARGS__)
#define ERR(...) EINA_LOG_DOM_ERR(_log_dom, __VA_ARGS__)
#define CRI(...) EINA_LOG_DOM_CRIT(_log_dom, __VA_ARGS__)

extern int _log_dom;

#endif
