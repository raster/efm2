#ifndef EFM_PRIVATE_H
#define EFM_PRIVATE_H 1

#include <Elementary.h>
#include "efm_structs.h"

#define INVALID              -999999
#define SCROLL_SEL_TIMER     0.2
#define SCROLL_DND_TIMER     0.2
#define BLOCK_MAX            64
#define FOCUS_ANIM_TIME      0.2
#define DND_OVER_OPEN_TIMER  1.0
#define ICON_LONGPRESS_TIMER 1.0

#define ENTRY                                       \
  Smart_Data *sd = evas_object_smart_data_get(obj); \
  if (!sd) return

typedef void (*Efm_Menu_Callback)(void *data, void *data2,
                                  Evas_Object         *efm,
                                  const Efm_Menu      *menu);
typedef void (*Efm_Menu_Item_Callback)(void *data, void *data2,
                                       Evas_Object         *efm,
                                       const Efm_Menu_Item *menu_item);

void   _listing_done(Smart_Data *sd);
void   _cb_header_change(void *data);
void   _reset(Smart_Data *sd);
void   _redo_detail_sizes(Smart_Data *sd);
char  *_sanitize_dir(const char *path);

Eina_Bool _file_video_is(const char *file);

#define MENU_NORM(_lab, _icn, _idx, _cb, _cbdat) \
  EFM_MENU_ITEM_NORMAL, _lab, _icn, _idx, EINA_FALSE, NULL, (void *)_cb, (void *)_cbdat
#define MENU_SUB(_lab, _icn, _sub) \
  EFM_MENU_ITEM_SUBMENU, _lab, _icn, 0, EINA_FALSE, _sub, NULL, NULL
#define MENU_SEPARATOR() \
    EFM_MENU_ITEM_SEPARATOR, NULL, NULL, 0, EINA_FALSE, NULL, NULL, NULL

  void _efm_menu_show(Evas_Object *obj, Efm_Menu *m, Evas_Coord x,
                      Evas_Coord y);

  Efm_Menu      *_efm_menu_add(const char *title, const char *icon,
                               Efm_Menu_Callback cb, void *data, void *data2);
  void           _efm_menu_del(Efm_Menu *m);
  Efm_Menu_Item *_efm_menu_it_normal(Efm_Menu *m, const char *label,
                                     const char            *icon,
                                     Efm_Menu_Item_Callback cb, void *data,
                                     void *data2);
  Efm_Menu_Item *_efm_menu_it_separator(Efm_Menu *m);
  Efm_Menu_Item *_efm_menu_it_sub(Efm_Menu *m, const char *label,
                                  const char *icon, Efm_Menu *sub);
  Efm_Menu_Item *_efm_menu_it_radio(Efm_Menu *m, const char *label,
                                    const char *icon, Eina_Bool on,
                                    Efm_Menu_Item_Callback cb, void *data,
                                    void *data2);
  Efm_Menu_Item *_efm_menu_it_check(Efm_Menu *m, const char *label,
                                    const char *icon, Eina_Bool on,
                                    Efm_Menu_Item_Callback cb, void *data,
                                    void *data2);

  Efm_Menu *_efm_popup_icon_menu_add(Smart_Data *sd, Icon *ic, Evas_Coord x,
                                     Evas_Coord y);
  Efm_Menu *_efm_popup_main_menu_add(Smart_Data *sd, Evas_Coord x,
                                     Evas_Coord y);

  extern Eina_List *_efm_list;
  extern Eina_List *_pending_exe_dels;

#endif
