#ifndef EFM_CUSTOM_H
#define EFM_CUSTOM_H 1

#include <Elementary.h>

void _icon_custom_data_free(Smart_Data *sd);
void _icon_custom_position_find(Icon *icon);
void _icon_custom_data_all_icons_fill(Smart_Data *sd);
void _icon_custom_position_placed(Icon *icon);
void _icon_custom_data_bitmap_clear(Smart_Data *sd);

#endif
