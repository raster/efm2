#include "efm.h"
#include "efm_private.h"

Efm_Menu *
_efm_menu_add(const char *title, const char *icon, Efm_Menu_Callback cb,
              void *data, void *data2)
{
  Efm_Menu *m = calloc(1, sizeof(Efm_Menu));

  eina_stringshare_replace(&(m->title), title);
  eina_stringshare_replace(&(m->title_icon), icon);
  m->private1 = cb;
  m->private2 = data;
  m->private3 = data2;
  return m;
}

void
_efm_menu_del(Efm_Menu *m)
{
  Efm_Menu_Item    *mi;
  int               i;

  if (!m) return;
  eina_stringshare_replace(&(m->title), NULL);
  eina_stringshare_replace(&(m->title_icon), NULL);
  for (i = 0; i < m->item_num; i++)
    {
      mi = &(m->item[i]);
      eina_stringshare_replace(&(mi->label), NULL);
      eina_stringshare_replace(&(mi->icon), NULL);
      _efm_menu_del(mi->sub);
    }
  free(m->item);
  free(m);
}

static int
_m_index(void)
{
  static int mindex = 1;
  int        i      = mindex;

  mindex++;
  if (mindex > 0x0fffffff) mindex = 1; // wrap at 28 bit - we're good here
  // as long as all indexes in a single menu tree instance (menu + all subs)
  // are unique per item you can select with a callback. so that's about
  // 268 million items we need to be unique for... good enough
  return i;
}

static Efm_Menu_Item *
_mi_add(Efm_Menu *m, Efm_Menu_Item_Type type, const char *label,
        const char *icon, Efm_Menu_Item_Callback cb, void *data, void *data2)
{
  Efm_Menu_Item *mi;

  m->item_num++;
  mi = realloc(m->item, (m->item_num * sizeof(Efm_Menu_Item)));
  if (!mi)
    {
      m->item_num--;
      return NULL;
    }
  m->item = mi;
  mi      = &(m->item[m->item_num - 1]);
  memset(mi, 0, sizeof(Efm_Menu_Item));
  mi->type  = type;
  mi->index = _m_index();
  eina_stringshare_replace(&(mi->label), label);
  eina_stringshare_replace(&(mi->icon), icon);
  mi->private1 = cb;
  mi->private2 = data;
  mi->private2 = data2;
  return mi;
}

Efm_Menu_Item *
_efm_menu_it_normal(Efm_Menu *m, const char *label, const char *icon,
                    Efm_Menu_Item_Callback cb, void *data, void *data2)
{
  return _mi_add(m, EFM_MENU_ITEM_NORMAL, label, icon, cb, data, data2);
}

Efm_Menu_Item *
_efm_menu_it_separator(Efm_Menu *m)
{
  return _mi_add(m, EFM_MENU_ITEM_SEPARATOR, NULL, NULL, NULL, NULL, NULL);
}

Efm_Menu_Item *
_efm_menu_it_sub(Efm_Menu *m, const char *label, const char *icon,
                 Efm_Menu *sub)
{
  Efm_Menu_Item *mi
    = _mi_add(m, EFM_MENU_ITEM_SUBMENU, label, icon, NULL, NULL, NULL);

  if (mi) mi->sub = sub;
  return mi;
}

Efm_Menu_Item *
_efm_menu_it_radio(Efm_Menu *m, const char *label, const char *icon,
                   Eina_Bool on, Efm_Menu_Item_Callback cb, void *data,
                   void *data2)
{
  Efm_Menu_Item *mi
    = _mi_add(m, EFM_MENU_ITEM_RADIO, label, icon, cb, data, data2);

  if (mi) mi->selected = on;
  return mi;
}

Efm_Menu_Item *
_efm_menu_it_check(Efm_Menu *m, const char *label, const char *icon,
                   Eina_Bool on, Efm_Menu_Item_Callback cb, void *data,
                   void *data2)
{
  Efm_Menu_Item *mi
    = _mi_add(m, EFM_MENU_ITEM_CHECK, label, icon, cb, data, data2);

  if (mi) mi->selected = on;
  return mi;
}