#ifndef EFM_STRUCTS_H
#define EFM_STRUCTS_H 1

#include <Elementary.h>
#include "cmd.h"

// data structs used for the efm view
//
typedef struct _Smart_Data        Smart_Data;
typedef struct _Smart_Data_Thread Smart_Data_Thread;
typedef struct _Smart_Data_Timer  Smart_Data_Timer;
typedef struct _Block             Block;
typedef struct _Icon              Icon;

typedef struct _File_Info File_Info;

// data kept around as long as the thread is so it knows what it is for
struct _Smart_Data_Thread
{
  Smart_Data        *sd;
  Eina_Thread_Queue *thq;
};

struct _Smart_Data_Timer
{
  Smart_Data       *sd;
  Eina_Stringshare *name;
  Ecore_Timer      *timer;
  double            delay;
  Eina_Bool         repeat : 1;
};

// an icon view gui data
struct _Smart_Data
{
  Evas_Object_Smart_Clipped_Data __clipped_data;

  Eina_Rectangle geom;

  Evas_Object *o_smart;
  Evas_Object *o_back;
  Evas_Object *o_clip;
  Evas_Object *o_scroller;
  Evas_Object *o_focus;
  Evas_Object *o_sel;
  Evas_Object *o_over;
  Evas_Object *o_detail_header;
  Evas_Object *o_detail_header_item[7];

  Evas_Object *o_list_detailed_dummy;
  Evas_Object *o_list_detail[6];
  Evas_Object *o_list_detail_swallow[6];
  Evas_Object *o_overlay_grid_fill;
  Evas_Object *o_overlay_grid;
  Evas_Object *o_overlay_info;

  Ecore_Exe           *exe_open;
  Ecore_Event_Handler *handler_exe_del;
  Ecore_Event_Handler *handler_exe_data;

  Smart_Data_Thread *thread_data;
  Ecore_Thread      *thread;

  Eina_List *icons;
  Eina_List *blocks;
  Ecore_Job *reblock_job;
  Ecore_Job *refocus_job;
  Ecore_Job *size_bars_update_job;
  Ecore_Job *size_max_update_job;
  Ecore_Job *header_change_job;
  Icon      *last_selected;
  Icon      *last_focused_before;
  Icon      *last_focused;
  Icon      *drag_icon;
  Icon      *over_icon;
  Icon      *drop_over;
  Icon      *rename_icon;

  Evas_Coord      icon_min_w, icon_min_h;
  Evas_Coord      list_min_w, list_min_h;
  Evas_Coord      list_detailed_min_w, list_detailed_min_h;
  Evas_Coord      back_down_x, back_down_y;
  Evas_Coord      back3_down_x, back3_down_y;
  Evas_Coord      back_x, back_y;
  Evas_Coord      sel_x1, sel_y1, sel_x2, sel_y2;
  Evas_Coord      dnd_scroll_x, dnd_scroll_y;
  Evas_Coord      detail_down_x, detail_down_y;
  Evas_Coord      detail_down_start_min_w;
  Evas_Coord      detail_header_min_h[7];
  double          focus_start_time;
  double          focus_pos;
  Ecore_Animator *focus_animator;
  Ecore_Timer    *scroll_timer;
  Ecore_Timer    *dnd_scroll_timer;
  Ecore_Timer    *dnd_over_open_timer;
  Eina_List      *timers;
  Elm_Xdnd_Action dnd_action;
  Evas_Coord      dnd_x, dnd_y;
  char           *dnd_drop_data;
  void           *custom_data; // handled in efm_custom.c

  unsigned long long file_max; // largest file size in dir

  struct
  {
    Efm_Menu_Provider  cb;
    void              *data;
    Efm_Menu          *menu; // current menu if active
    void              *menu_data; // some handle to the currently active menu
  } menu_provider;

  Eina_Bool reblocked            : 1;
  Eina_Bool relayout             : 1;
  Eina_Bool focused              : 1;
  Eina_Bool focus_show           : 1;
  Eina_Bool key_control          : 1;
  Eina_Bool back_down            : 1;
  Eina_Bool back3_down           : 1;
  Eina_Bool sel_show             : 1;
  Eina_Bool drag                 : 1;
  Eina_Bool just_dragged         : 1;
  Eina_Bool cnp_have             : 1;
  Eina_Bool cnp_cut              : 1;
  Eina_Bool detail_down          : 1;
  Eina_Bool listing_done         : 1;
  Eina_Bool listing_done_reblock : 1;

  struct
  {
    Efm_View_Mode     view_mode;
    Efm_Sort_Mode     sort_mode;
    Evas_Coord        icon_size;
    Eina_Stringshare *path;
    Eina_Stringshare *backend;
    Evas_Coord        detail_min_w[6];
    Eina_Stringshare *detail_heading[7];
  } config;
};

// file info gagthered
struct _File_Info
{
  const char        *file;
  const char        *label;
  const char        *label_selected;
  const char        *label_clicked;
  const char        *mime;
  const char        *icon;
  const char        *icon_selected;
  const char        *icon_clicked;
  const char        *mime_icon;
  const char        *pre_lookup_icon;
  const char        *thumb;
  unsigned long long size;
  Eina_Bool          dir     : 1;
  Eina_Bool          link    : 1;
  Eina_Bool          broken  : 1;
  Eina_Bool          special : 1;
};

// a block of icons as a group
struct _Block
{
  Eina_Rectangle bounds; // relative to top-left of obj
  Smart_Data    *sd;
  Eina_List     *icons;
  int            realized_num;
  int            selected_num;
  Eina_Bool      changed : 1;
};

// an icon in a block of icons
struct _Icon
{
  Eina_Rectangle geom;
  Evas_Object   *o_base;
  Evas_Object   *o_icon;
  Evas_Object   *o_entry;
  Evas_Object   *o_list_detail_swallow[6];
  Evas_Object   *o_list_detail_swallow2[6];
  Evas_Object   *o_list_detail_swallow3[6];
  Smart_Data    *sd;
  Cmd           *cmd;
  Block         *block;
  Eina_List     *block_list;
  Ecore_Timer   *longpress_timer;
  File_Info      info;
  Evas_Coord     down_x, down_y, down_rel_x, down_rel_y;
  Evas_Coord     last_x, last_y;
  Eina_Bool      realized : 1;
  Eina_Bool      selected : 1;
  Eina_Bool      down     : 1;
  Eina_Bool      over     : 1;
  Eina_Bool      changed  : 1;
  Eina_Bool      edje     : 1;
  Eina_Bool      renaming : 1;
  Eina_Bool      drag     : 1;
};

typedef struct _Pending_Exe_Del Pending_Exe_Del;

// an entry in our list of back-end open processes waiting to exit
struct _Pending_Exe_Del
{
  Ecore_Exe   *exe;
  Ecore_Timer *timer;
};

// a message from thread to front-end ui in main loop
typedef struct
{
  Eina_Thread_Queue_Msg head;
  Cmd                  *c;
} Msg;

#endif
