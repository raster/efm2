// test gui front-end for new efm
#include "efm.h"

// #include "efm_icon.h"

static Evas_Object *o_detail_header_box = NULL;
static Evas_Object *o_detail_header     = NULL;

static void
_cb_parent(void *data, Evas_Object *obj EINA_UNUSED,
           void *event_info EINA_UNUSED)
{
  efm_path_parent(data);
}

static void
_cb_icons(void *data, Evas_Object *obj EINA_UNUSED,
          void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_ICONS);
}

static void
_cb_icons_v(void *data, Evas_Object *obj EINA_UNUSED,
            void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_ICONS_VERTICAL);
}

static void
_cb_icons_custom(void *data, Evas_Object *obj EINA_UNUSED,
                 void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_ICONS_CUSTOM);
}

static void
_cb_icons_custom_v(void *data, Evas_Object *obj EINA_UNUSED,
                   void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL);
}

static void
_cb_list(void *data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_LIST);
}

static void
_cb_list_detailed(void *data, Evas_Object *obj EINA_UNUSED,
                  void *event_info EINA_UNUSED)
{
  efm_path_view_mode_set(data, EFM_VIEW_MODE_LIST_DETAILED);
}

static void
_cb_header_change(void *data, Evas_Object *obj EINA_UNUSED,
                  void *event_info EINA_UNUSED)
{
  if (efm_path_view_mode_get(data) == EFM_VIEW_MODE_LIST_DETAILED)
    {
      if (!o_detail_header)
        {
          Evas_Object *o;

          o_detail_header = o = efm_detail_header_get(data);
          evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
          evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0.0);
          elm_box_pack_end(o_detail_header_box, o);
          evas_object_show(o);
        }
    }
  else
    {
      if (o_detail_header)
        {
          evas_object_del(o_detail_header);
          o_detail_header = NULL;
        }
    }
}

EAPI_MAIN int
elm_main(int argc, char **argv)
{
  const char  *path, *icon;
  Evas_Object *o, *win, *sc, *efm, *bx, *bx2, *par;
  char         buf[PATH_MAX];

  if (argc > 1)
    {
      path = ecore_file_realpath(argv[1]);
    }
  else
    {
      getcwd(buf, sizeof(buf));
      path = buf;
    }

  elm_need_efreet();
  elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
  elm_app_compile_bin_dir_set(PACKAGE_BIN_DIR);
  elm_app_compile_lib_dir_set(PACKAGE_LIB_DIR);
  elm_app_compile_data_dir_set(PACKAGE_DATA_DIR);
  elm_app_info_set(elm_main, "efm", "checkme");

  win = o = elm_win_util_standard_add("Main", "Files");
  elm_win_icon_name_set(o, "Files");
  elm_win_role_set(o, "efm_win_files");
  elm_win_autodel_set(o, EINA_TRUE);

  o = elm_icon_add(win);
  icon = efreet_icon_path_find(elm_config_icon_theme_get(),
                               "inode-directory", 128);
  if (icon)
    {
      elm_image_file_set(o, icon, NULL);
      elm_win_icon_object_set(win, o);
    }
  else evas_object_del(o);

  bx = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  elm_box_horizontal_set(o, EINA_FALSE);
  elm_win_resize_object_add(win, o);
  evas_object_show(o);

  bx2 = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_homogeneous_set(o, EINA_FALSE);
  elm_box_horizontal_set(o, EINA_TRUE);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  par = o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, 0, 0);
  elm_object_text_set(o, "<");
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "X");
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o_detail_header_box = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  sc = o = elm_scroller_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  efm = o = efm_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  evas_object_smart_callback_add(o, "header_change", _cb_header_change, efm);
  elm_object_content_set(sc, o);
  efm_scroller_set(o, sc);
  efm_path_set(o, path);
  evas_object_show(o);

  evas_object_smart_callback_add(par, "clicked", _cb_parent, efm);

  bx2 = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_homogeneous_set(o, EINA_TRUE);
  elm_box_horizontal_set(o, EINA_TRUE);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Icons");
  evas_object_smart_callback_add(o, "clicked", _cb_icons, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Icons V");
  evas_object_smart_callback_add(o, "clicked", _cb_icons_v, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "List");
  evas_object_smart_callback_add(o, "clicked", _cb_list, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Detailed");
  evas_object_smart_callback_add(o, "clicked", _cb_list_detailed, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Custom");
  evas_object_smart_callback_add(o, "clicked", _cb_icons_custom, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Custom V");
  evas_object_smart_callback_add(o, "clicked", _cb_icons_custom_v, efm);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  evas_object_resize(win, ELM_SCALE_SIZE(700), ELM_SCALE_SIZE(300));
  evas_object_show(win);

  elm_object_focus_set(sc, EINA_TRUE);

  elm_run();

  return 0;
}
ELM_MAIN()
