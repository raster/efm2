#ifndef EFM_ICON_H
#define EFM_ICON_H 1

#include <Elementary.h>

Evas_Object *efm_icon_add(Evas_Object *parent);
void         efm_icon_orig_set(Evas_Object *obj, const char *orig);
void         efm_icon_file_set(Evas_Object *obj, const char *file);
void         efm_icon_thumb_set(Evas_Object *obj, const char *thumb);
void         efm_icon_video_set(Evas_Object *obj, const char *video);
void         efm_icon_keep_aspect_set(Evas_Object *obj, Eina_Bool keep_aspect);
void         efm_icon_size_get(Evas_Object *obj, int *w, int *h);
Eina_Bool    efm_icon_alpha_get(Evas_Object *obj);
Eina_Bool    efm_icon_mono_get(Evas_Object *obj);

// smart callbacks:
//   "loaded"

#endif
