#include <Elementary.h>

#include "status_mon.h"

static Evas_Object *tasks_box = NULL;
static Eina_List   *tasks     = NULL;

typedef struct
{
  Status_Op   *op;
  Evas_Object *o_base, *o_progress, *o_label_dst, *o_label_str;
} Task;

static Task *
task_find(Status_Op *op)
{
  Eina_List *l;
  Task      *t;

  EINA_LIST_FOREACH(tasks, l, t)
  {
    if (t->op == op) return t;
  }
  return NULL;
}

static Task *
task_add(Status_Op *op)
{
  Task *t = calloc(1, sizeof(Task));

  if (!t) return NULL;
  tasks = eina_list_append(tasks, t);
  t->op = op;
  return t;
}

static void
task_del(Task *t)
{
  Eina_List *l;
  Task      *t2;

  EINA_LIST_FOREACH(tasks, l, t2)
  {
    if (t == t2)
      {
        tasks = eina_list_remove(tasks, t);
        free(t);
        return;
      }
  }
}

static void
task_new(Task *t)
{
  Evas_Object *o, *fr, *bx;

  fr = o = elm_frame_add(tasks_box);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_pack_end(tasks_box, o);
  evas_object_show(o);
  t->o_base = o;

  bx = o = elm_box_add(tasks_box);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_horizontal_set(o, EINA_FALSE);
  elm_object_content_set(fr, o);
  evas_object_show(o);

  o = elm_label_add(tasks_box);
  evas_object_size_hint_fill_set(o, 0, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_pack_end(bx, o);
  evas_object_show(o);
  t->o_label_dst = o;

  o = elm_label_add(tasks_box);
  evas_object_size_hint_fill_set(o, 0, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_pack_end(bx, o);
  evas_object_show(o);
  t->o_label_str = o;

  o = elm_progressbar_add(tasks_box);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_progressbar_horizontal_set(o, EINA_TRUE);
  elm_progressbar_unit_format_set(o, "%1.1f%%");
  elm_progressbar_span_size_set(o, ELM_SCALE_SIZE(200));
  elm_box_pack_end(bx, o);
  evas_object_show(o);
  t->o_progress = o;
}

static void
_cb_status(void *data EINA_UNUSED, Status_Op *op, Cmd *cmd)
{
  Task       *t;
  const char *s, *s2;

  t = task_find(op);
  if (!t)
    {
      t = task_add(op);
      if (!t) return;
      task_new(t);
    }
  if (!cmd)
    {
      evas_object_del(t->o_base);
      task_del(t);
      return;
    }
  if (!strcmp(cmd->command, "op"))
    {
      s = cmd_key_find(cmd, "op");
      if (s)
        {
          if (!strcmp(s, "mv")) elm_object_text_set(t->o_base, "Move");
        }
    }
  else if (!strcmp(cmd->command, "dst"))
    {
      s = cmd_key_find(cmd, "dst");
      if (s)
        {
          elm_object_text_set(t->o_label_dst, s);
        }
    }
  else if (!strcmp(cmd->command, "progress"))
    {
      s = cmd_key_find(cmd, "str");
      if (s)
        {
          elm_object_text_set(t->o_label_str, s);
        }
      s = cmd_key_find(cmd, "pos");
      if (s)
        {
          s2 = cmd_key_find(cmd, "count");
          if (s2)
            {
              unsigned long long pos, count;

              pos   = atoll(s);
              count = atoll(s2);

              if ((pos > 0) && (count > 0))
                {
                  double v = (double)pos / (double)count;

                  elm_progressbar_value_set(t->o_progress, v);
                }
            }
        }
    }
  else if (!strcmp(cmd->command, "end"))
    {
      evas_object_del(t->o_base);
      task_del(t);
    }
  // handle "error"
}

static void
_cb_exit(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
         void *event_info EINA_UNUSED)
{
  elm_exit();
}

EAPI_MAIN int
elm_main(int argc EINA_UNUSED, char **argv EINA_UNUSED)
{
  Evas_Object *o, *win, *bx, *sc, *bx2;

  elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);

  win = o = elm_win_util_standard_add("Main", "Tasks");
  elm_win_icon_name_set(o, "Tasks");
  elm_win_role_set(o, "efm_win_tasks");
  elm_win_autodel_set(o, EINA_TRUE);

  bx = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  elm_box_horizontal_set(o, EINA_FALSE);
  elm_win_resize_object_add(win, o);
  evas_object_show(o);

  sc = o = elm_scroller_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  tasks_box = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_homogeneous_set(o, EINA_TRUE);
  elm_box_horizontal_set(o, EINA_FALSE);
  elm_object_content_set(sc, o);
  evas_object_show(o);

  bx2 = o = elm_box_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_box_homogeneous_set(o, EINA_TRUE);
  elm_box_horizontal_set(o, EINA_TRUE);
  elm_box_pack_end(bx, o);
  evas_object_show(o);

  o = elm_button_add(win);
  evas_object_size_hint_fill_set(o, EVAS_HINT_FILL, 0);
  evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, 0);
  elm_object_text_set(o, "Exit");
  evas_object_smart_callback_add(o, "clicked", _cb_exit, NULL);
  elm_box_pack_end(bx2, o);
  evas_object_show(o);

  evas_object_resize(win, ELM_SCALE_SIZE(480), ELM_SCALE_SIZE(640));
  evas_object_show(win);

  status_mon_init();
  status_mon_callback_add(_cb_status, NULL);
  status_mon_begin();

  elm_run();

  status_mon_shutdown();

  return 0;
}
ELM_MAIN()