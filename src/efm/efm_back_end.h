#ifndef EFM_BACK_END_H
#define EFM_BACK_END_H 1

#include <Elementary.h>

void      _size_message(Evas_Object *o, double v);
Eina_Bool _cb_exe_del(void *data, int ev_type EINA_UNUSED, void *event);
Eina_Bool _cb_exe_data(void *data, int ev_type EINA_UNUSED, void *event);
Eina_Bool _cb_exe_pending_timer(void *data);
void      _cb_thread_main(void *data, Ecore_Thread *th);
void _cb_thread_notify(void *data, Ecore_Thread *th EINA_UNUSED, void *msg);
void _cb_thread_done(void *data, Ecore_Thread *th EINA_UNUSED);

#endif
