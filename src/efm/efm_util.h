#ifndef EFM_UTIL_H
#define EFM_UTIL_H 1

#include <Elementary.h>

#include "efm_structs.h"

typedef enum
{
  EFM_FOCUS_DIR_UP,
  EFM_FOCUS_DIR_DOWN,
  EFM_FOCUS_DIR_LEFT,
  EFM_FOCUS_DIR_RIGHT,
  EFM_FOCUS_DIR_PGDN,
  EFM_FOCUS_DIR_PGUP
} Efm_Focus_Dir;

char      *_escape_parse(const char *str);
Eina_Bool  _selected_icons_uri_strbuf_append(Smart_Data  *sd,
                                             Eina_Strbuf *strbuf);
void       _detail_realized_items_resize(Smart_Data *sd);
double     _scale_get(Smart_Data *sd);
Eina_List *_icons_path_find(const char *path);
Eina_Bool  _icon_focus_dir(Smart_Data *sd, Efm_Focus_Dir dir);
void       _icon_rename_end(Icon *icon);
void       _icon_open_with_cmd_strbuf_append(Eina_Strbuf *strbuf,
                                             const char *key, Smart_Data *sd);
void       _icon_path_cmd_strbuf_append(Eina_Strbuf *strbuf,
                                        const char  *key,
                                        Smart_Data *sd, Icon *icon);
void       _uri_list_cmd_strbuf_append(Eina_Strbuf *strbuf, const char *key,
                                       const char *urilist);
void       _icon_over_off(Icon *icon);
void       _icon_over_on(Icon *icon);
void       _icon_select(Icon *icon);
void       _icon_unselect(Icon *icon);
Eina_Bool  _unselect_all(Smart_Data *sd);
void       _select_range(Icon *icon_from, Icon *icon_to);
Icon      *_icon_dup(Icon *icon);

void _efm_sel_store_start(Smart_Data *sd, Evas_Coord x, Evas_Coord y);
void _efm_sel_store_end(Smart_Data *sd, Evas_Coord x, Evas_Coord y);
void _efm_sel_start(Smart_Data *sd);
void _efm_sel_end(Smart_Data *sd);

void _icon_focus(Smart_Data *sd);

void _icon_focus_show(Smart_Data *sd);
void _icon_focus_hide(Smart_Data *sd);
void _icon_object_clear(Icon *icon);

void           _efm_focus_position(Smart_Data *sd);
Eina_Rectangle _efm_sel_rect_get(Smart_Data *sd);
void           _efm_sel_position(Smart_Data *sd);

void _icon_detail_add(Icon *icon, Smart_Data *sd, Evas *e,
                      const char *theme_edj_file, int col, const char *detail,
                      const char *format);
void _icon_object_add(Icon *icon, Smart_Data *sd, Evas *e,
                      const char *theme_edj_file, Eina_Bool clip_set, int num);
void _icon_free(Icon *icon);
void _block_free(Block *block);
void _cb_reblock(void *data);

#endif
