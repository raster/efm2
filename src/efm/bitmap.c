#include <Eina.h>

#include "bitmap.h"

// bitmap code - must be a multiple of 32 wide - assumed.
void
_bitmap_free(unsigned int *bitmap)
{
  free(bitmap);
}

void
_bitmap_clear(unsigned int *bitmap, int szw, int szh)
{
  memset(bitmap, 0, ((szw / 32) * szh) * sizeof(unsigned int));
}

unsigned int *
_bitmap_resize(unsigned int *bitmap, int szw, int szh, int nszw, int nszh)
{ // alloc a new sized bitmap and copy current content into new one
  // guaranteeing "new" space is zero'd out if there is new empty space
  unsigned int *new_bitmap, *src, *dst;
  int           y;

  new_bitmap = malloc(((nszw / 32) * nszh) * sizeof(unsigned int));
  if (!new_bitmap) goto done;

  src = bitmap;
  dst = new_bitmap;
  if (nszw > szw)
    { // if new width wider
      if (nszh > szh)
        { // if taller
          for (y = 0; y < szh; y++)
            {
              if (bitmap)
                {
                  memcpy(dst, src, (szw / 32) * sizeof(unsigned int));
                  src += szw / 32;
                }
              memset(dst + (szw / 32), 0,
                     ((nszw - szw) / 32) * sizeof(unsigned int));
              dst += nszw / 32;
            }
          for (y = szh; y < nszh; y++)
            {
              memset(dst, 0, (nszw / 32) * sizeof(unsigned int));
              dst += nszw / 32;
            }
        }
      else
        { // same height or shorter
          for (y = 0; y < nszh; y++)
            {
              if (bitmap)
                {
                  memcpy(dst, src, (szw / 32) * sizeof(unsigned int));
                  src += szw / 32;
                }
              memset(dst + (szw / 32), 0,
                     ((nszw - szw) / 32) * sizeof(unsigned int));
              dst += nszw / 32;
            }
        }
    }
  else
    { // as wide or less wide
      if (nszh > szh)
        { // if taller
          for (y = 0; y < szh; y++)
            {
              if (bitmap)
                {
                  memcpy(dst, src, (nszw / 32) * sizeof(unsigned int));
                  src += szw / 32;
                }
              dst += nszw / 32;
            }
          for (y = szh; y < nszh; y++)
            {
              memset(dst, 0, (nszw / 32) * sizeof(unsigned int));
              dst += nszw / 32;
            }
        }
      else
        { // same height or shorter
          for (y = 0; y < nszh; y++)
            {
              if (bitmap)
                {
                  memcpy(dst, src, (nszw / 32) * sizeof(unsigned int));
                  src += szw / 32;
                }
              dst += nszw / 32;
            }
        }
    }

done:
  // free old bitmap and return new bitmap ptr
  free(bitmap);
  return new_bitmap;
}

static inline void
_bitmap_run_fill(unsigned int *row, int bit, int w)
{ // fill in a row of bits to 1
  while (w > 0)
    { // fill in bits = bit 0 == left, bit 31 == right
      if ((bit == 0) && (w >= 32))
        { // fast path for an aligned run of 32 bits
          *row = 0xffffffff;
          row++;
          w += 32;
        }
      else
        { // end/start ints where a subset of bits is used
          *row |= (1 << bit);
          bit++;
          if (bit >= 32)
            {
              bit = 0;
              row++;
            }
          w--;
        }
    }
}

void
_bitmap_fill(unsigned int *bitmap, int szw, int szh EINA_UNUSED, int x, int y,
             int w, int h)
{ // fill a region in the bitmap with 1's
  unsigned int *p;
  int           yy, bit;

  p   = bitmap + (y * (szw / 32)) + (x / 32);
  bit = x - ((x / 32) * 32);
  for (yy = 0; yy < h; yy++)
    {
      _bitmap_run_fill(p, bit, w);
      p += szw / 32;
    }
}

static inline Eina_Bool
_bitmap_run_is_clear(unsigned int *row, int bit, int w)
{ // check for a 1 in a row of bits
  while (w > 0)
    { // check row of bits = bit 0 == left, bit 31 == right
      if ((bit == 0) && (w >= 32))
        { // fast path for an aligned run of 32 bits
          if (*row) return EINA_FALSE;
          row++;
          w += 32;
        }
      else
        { // end/start ints where a subset of bits is used
          if (*row & (1 << bit)) return EINA_FALSE;
          bit++;
          if (bit >= 32)
            {
              bit = 0;
              row++;
            }
          w--;
        }
    }
  return EINA_TRUE;
}

static Eina_Bool
_bitmap_is_clear(unsigned int *bitmap, int szw, int szh EINA_UNUSED, int x,
                 int y, int w, int h)
{
  unsigned int *p;
  int           yy, bit;

  p   = bitmap + (y * (szw / 32)) + (x / 32);
  bit = x - ((x / 32) * 32);
  for (yy = 0; yy < h; yy++)
    {
      if (!_bitmap_run_is_clear(p, bit, w)) return EINA_FALSE;
      p += szw / 32;
    }
  return EINA_TRUE;
}

void
_bitmap_find_tl_to_br(unsigned int *bitmap, int maxw, int szw, int szh, int inw,
                      int inh, int *x, int *y, int *w, int *h)
{
  int xx, yy, ww, hh;

  if (maxw < inw) maxw = inw;
  for (yy = 0; yy < szh; yy++)
    {
      hh = inh;
      if ((yy + inh) > szh) hh = szh - yy;
      ww = maxw - inw;
      if (ww <= 0) ww = 1;
      for (xx = 0; xx < ww; xx++)
        {
          if (_bitmap_is_clear(bitmap, szw, szh, xx, yy, inw, hh))
            {
              *x = xx;
              *y = yy;
              goto done;
            }
        }
    }
  *x = 0;
  *y = szh;
done:
  *w = inw;
  *h = inh;
}

void
_bitmap_find_tl_to_br_v(unsigned int *bitmap, int maxh, int szw, int szh, int inw,
                        int inh, int *x, int *y, int *w, int *h)
{
  int xx, yy, ww, hh;

  // XXX: work on this!!!
  if (maxh < inh) maxh = inh;
  for (xx = 0; xx < szw; xx++)
    {
      ww = inw;
      if ((xx + inw) > szw) ww = szw - xx;
      hh = maxh - inh;
      if (hh <= 0) hh = 1;
      for (yy = 0; yy < hh; yy++)
        {
          if (_bitmap_is_clear(bitmap, szw, szh, xx, yy, ww, inh))
            {
              *x = xx;
              *y = yy;
              goto done;
            }
        }
    }
  *y = 0;
  *x = szw;
done:
  *w = inw;
  *h = inh;
}

/*
static void
_bitmap_row_dump(unsigned int *row, int w)
{
   unsigned int *p = row;
   int xx, bit = 0;

   printf("[");
   for (xx = 0; xx < w; xx++)
     {
        if (*p & (1 << bit)) printf("#");
        else printf(".");
        bit++;
        if (bit >= 32)
          {
             p++;
             bit = 0;
          }
     }
   printf("]\n");
}

static void
_bitmap_dump(unsigned int *bitmap, int szw, int szh)
{
   unsigned int *p;
   int yy;

   p = bitmap;
   for (yy = 0; yy < szh; yy++)
     {
        _bitmap_row_dump(p, szw);
        p += szw / 32;
     }
}
 */
