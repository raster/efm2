#ifndef EFM_DND_H
#define EFM_DND_H 1

#include <Elementary.h>
#include "efm_structs.h"

typedef enum
{
  EFM_ACTION_MOVE,
  EFM_ACTION_COPY,
  EFM_ACTION_ASK
} Efm_Action;

void _drop_init(Smart_Data *sd);
void _drag_start(Icon *icon, Efm_Action action);

#endif
