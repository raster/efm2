#include "efm.h"
#include "efm_private.h"
#include "efm_structs.h"
#include "eina_types.h"

static void
_cb_menu_done(void *data, void *data2, Evas_Object *efm, const Efm_Menu *menu)
{
  printf("menu done\n");  
}

static void
_cb_ic_item1(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic1\n");
}

static void
_cb_ic_item2(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic2\n");
}

static void
_cb_ic_item3(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic3\n");
}

static void
_cb_ic_item4(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic4\n");
}

static void
_cb_ic_item5(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic5\n");
}

static void
_cb_ic_item6(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic6\n");
}

static void
_cb_ic_item7(void *data, void *data2, Evas_Object *efm,
             const Efm_Menu_Item *menu_item)
{
  printf("ic7\n");
}

static const char *
_icon_strbuf_icon_def(Icon *icon, Eina_Strbuf *icbuf)
{
  const char *icstr;

  eina_strbuf_reset(icbuf);
  if (icon->info.thumb)
    {
      eina_strbuf_append(icbuf, "thumb:");
      eina_strbuf_append(icbuf, icon->info.thumb);
    }
  else
    {
      const char *ic;

      ic = icon->info.pre_lookup_icon;
      if (!ic) ic = icon->info.icon;
      if ((!ic) && (icon->info.mime))
        {
          const char *icfile;
          char        buf[1024];

          snprintf(buf, sizeof(buf), "e/icons/fileman/mime/%s",
                   icon->info.mime);
          icfile = elm_theme_group_path_find(NULL, buf);
          if (!icfile)
            {
              snprintf(buf, sizeof(buf), "e/icons/fileman/mime/inode/file");
              icfile = elm_theme_group_path_find(NULL, buf);
            }
          if (icfile)
            {
              eina_strbuf_append(icbuf, "edje:");
              eina_strbuf_append(icbuf, icfile);
              eina_strbuf_append(icbuf, "|");
              eina_strbuf_append(icbuf, buf);
            }
        }
      if ((ic) && (ic[0] == '/'))
        {
          // XXX: need common code for this to share
          if (_file_video_is(ic)) eina_strbuf_append(icbuf, "video:");
          else eina_strbuf_append(icbuf, "file:");
          eina_strbuf_append(icbuf, ic);
        }
      else if (ic)
        {
          eina_strbuf_append(icbuf, "std:");
          eina_strbuf_append(icbuf, ic);
        }
    }
  icstr = eina_strbuf_string_get(icbuf);
  if ((icstr) && (!icstr[0])) icstr = NULL;
  return icstr;
}

Efm_Menu *
_efm_popup_icon_menu_add(Smart_Data *sd, Icon *ic, Evas_Coord x, Evas_Coord y)
{
  Efm_Menu    *m1, *m2;
  Eina_Strbuf *icbuf;
  const char  *icstr;

  // XXX: right click icon menu.... should have
  //
  // regular menu:
  // (if num selected > 0) Open
  // (if dir) Open new win (or same win depending on cfg)
  // (if num selected > 0) Open with -> list mime desktops + Other...
  // Actions -> list of desktops that have actions for this mime
  // ---
  // (if can werite) Cut
  // Copy
  // (if can write) Paste
  // Copy path
  // ---
  // Seklect all / none
  // ---
  // (if can write) Trash
  // (if can write) Delete
  // ---
  // (if can write) Rename
  // ---
  // Properties
  // ---
  // View mode ->  list of efnm view modes if can change
  // Sort -> list of sort modes/flags if can change
  // (if can change) View Settings
  // ---
  // Refresh View
  // ---
  // (if can write) New Dir
  // (if view win) Close view
  // XXX: need to know to dismiss menu if icon freed
  //
  // allow backend to handkle right click instead and request show
  // of menu then from backend

  icbuf = eina_strbuf_new();
  m1    = _efm_menu_add("Submenu", "std:mail-reply-all", NULL, NULL, NULL);
  _efm_menu_it_normal(m1, "Item 1", "std:menu/folder", _cb_ic_item1, ic, NULL);
  _efm_menu_it_normal(m1, "Item 2", "std:system-lock-screen", _cb_ic_item2, ic,
                      NULL);
  _efm_menu_it_separator(m1);
  _efm_menu_it_normal(m1, "Item 3 - much longer label", "std:media-eject",
                      _cb_ic_item3, ic, NULL);
  _efm_menu_it_normal(m1, "Item 4", "std:battery", _cb_ic_item4, ic, NULL);
  m2    = _efm_menu_add("Main Menu", "std:security-high", _cb_menu_done, NULL,
                        NULL);
  icstr = _icon_strbuf_icon_def(ic, icbuf);
  _efm_menu_it_normal(m2, ic->info.label, icstr, _cb_ic_item5, ic, NULL);
  _efm_menu_it_normal(m2, "Item 5", "std:system-run", _cb_ic_item5, ic, NULL);
  _efm_menu_it_separator(m2);
  _efm_menu_it_check(m2, "Check 1", "std:folder", EINA_FALSE, _cb_ic_item6,
                     ic, NULL);
  _efm_menu_it_check(m2, "Check 2", "std:user-home", EINA_TRUE, _cb_ic_item7,
                     ic, NULL);
  _efm_menu_it_separator(m2);
  _efm_menu_it_radio(m2, "Radio 1", "std:user-desktop", EINA_FALSE,
                     _cb_ic_item6, ic, NULL);
  _efm_menu_it_radio(m2, "Radio 2", NULL, EINA_TRUE, _cb_ic_item6, ic, NULL);
  _efm_menu_it_radio(m2, "Radio 3", "std:computer", EINA_FALSE, _cb_ic_item6,
                     ic, NULL);
  _efm_menu_it_separator(m2);
  _efm_menu_it_radio(m2, "Radio 1", "std:drive-optical", EINA_FALSE,
                     _cb_ic_item6, ic, NULL);
  _efm_menu_it_radio(m2, "Radio 2", "std:drive-harddisk", EINA_FALSE,
                     _cb_ic_item6, ic, NULL);
  _efm_menu_it_radio(m2, "Radio 3", NULL, EINA_TRUE, _cb_ic_item6, ic, NULL);
  _efm_menu_it_sub(m2, "Submenu here", "fdo:terminology", m1);

  _efm_menu_show(ic->sd->o_smart, m2, x, y);
  eina_strbuf_free(icbuf);

  return m2;
}

Efm_Menu *
_efm_popup_main_menu_add(Smart_Data *sd, Evas_Coord x, Evas_Coord y)
{
  Efm_Menu *m;

  m = _efm_menu_add("Main Menu", "std:security-high", _cb_menu_done, NULL,
                    NULL);
  _efm_menu_it_normal(m, "Item 1", "std:menu/folder", _cb_ic_item1, sd, NULL);
  _efm_menu_it_normal(m, "Item 2", "std:system-lock-screen", _cb_ic_item2, sd,
                      NULL);
  _efm_menu_it_separator(m);
  _efm_menu_it_normal(m, "Item 3 - much longer label", "std:media-eject",
                      _cb_ic_item3, sd, NULL);
  _efm_menu_it_normal(m, "Item 4", "std:battery", _cb_ic_item4, sd, NULL);
  _efm_menu_show(sd->o_smart, m, x, y);
  return m;
}