#ifndef BITMAP_H
#define BITMAP_H 1

#include <Eina.h>

void          _bitmap_free(unsigned int *bitmap);
void          _bitmap_clear(unsigned int *bitmap, int szw, int szh);
unsigned int *_bitmap_resize(unsigned int *bitmap, int szw, int szh, int nszw,
                             int nszh);
void          _bitmap_fill(unsigned int *bitmap, int szw, int szh EINA_UNUSED, int x,
                           int y, int w, int h);
void _bitmap_find_tl_to_br(unsigned int *bitmap, int maxw, int szw, int szh,
                           int inw, int inh, int *x, int *y, int *w, int *h);
void _bitmap_find_tl_to_br_v(unsigned int *bitmap, int maxh, int szw, int szh,
                             int inw, int inh, int *x, int *y, int *w, int *h);

#endif
