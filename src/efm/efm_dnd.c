#include "Elementary.h"
#include "Evas.h"
#include "cmd.h"
#include "efm.h"
#include "efm_structs.h"
#include "efm_util.h"
#include "efm_dnd.h"
#include "efm_private.h"
#include "eina_strbuf.h"

// utils for draga and drop handling
static Eina_Bool
_cb_dnd_scroll_timer(void *data)
{
  Smart_Data *sd = data;
  Evas_Coord  fmx, fmy, sx, sy, sw, sh, x, y;
  Evas_Coord  scr_edge_x, scr_edge_y, scr_mul;

  evas_object_geometry_get(sd->o_smart, &fmx, &fmy, NULL, NULL);
  evas_object_geometry_get(sd->o_scroller, &sx, &sy, &sw, &sh);
  scr_mul    = 4;
  scr_edge_x = sd->icon_min_w / 2;
  scr_edge_y = sd->icon_min_h / 2;
  if (sd->dnd_scroll_x < scr_edge_x)
    x = sx - fmx + sd->dnd_scroll_x - scr_edge_x
        - ((scr_edge_x - sd->dnd_scroll_x) * scr_mul);
  else if (sd->dnd_scroll_x > (sw - scr_edge_x))
    x = sx - fmx + sd->dnd_scroll_x + scr_edge_x
        + ((sd->dnd_scroll_x - (sw - scr_edge_x)) * scr_mul);
  else x = sx - fmx + sd->dnd_scroll_x;
  if (sd->dnd_scroll_y < scr_edge_y)
    y = sy - fmy + sd->dnd_scroll_y - scr_edge_y
        - ((scr_edge_y - sd->dnd_scroll_y) * scr_mul);
  else if (sd->dnd_scroll_y > (sh - scr_edge_y))
    y = sy - fmy + sd->dnd_scroll_y + scr_edge_y
        + ((sd->dnd_scroll_y - (sh - scr_edge_y)) * scr_mul);
  else y = sy - fmy + sd->dnd_scroll_y;
  printf("SSS %i %i     | %i %i %ix%i | %i %i\n", x, y, sx, sy, sw, sh,
         sd->dnd_scroll_x, sd->dnd_scroll_y);
  elm_scroller_region_bring_in(sd->o_scroller, x, y, 1, 1);
  return EINA_TRUE;
}

static void
_dnd_scroll_handle(Smart_Data *sd, Evas_Coord x, Evas_Coord y)
{
  sd->dnd_scroll_x = x;
  sd->dnd_scroll_y = y;
  if (!sd->dnd_scroll_timer)
    sd->dnd_scroll_timer
      = ecore_timer_add(SCROLL_DND_TIMER, _cb_dnd_scroll_timer, sd);
}

static void
_dnd_scroll_end_handle(Smart_Data *sd)
{
  if (sd->dnd_scroll_timer)
    {
      ecore_timer_del(sd->dnd_scroll_timer);
      sd->dnd_scroll_timer = NULL;
    }
}

static void
_dnd_over(Icon *icon)
{
  _icon_over_on(icon);
}

static void
_dnd_over_none_handle(Smart_Data *sd)
{
  if (sd->over_icon) _icon_over_off(sd->over_icon);
  sd->over_icon = NULL;
}

static void
_dnd_over_handle(Smart_Data *sd, Evas_Coord x, Evas_Coord y)
{
  Icon      *icon;
  Eina_List *bl, *il;
  Block     *block;

  EINA_LIST_FOREACH(sd->blocks, bl, block)
  {
    if (!eina_rectangle_coords_inside(&(block->bounds), x, y)) continue;
    EINA_LIST_FOREACH(block->icons, il, icon)
    {
      if (!eina_rectangle_coords_inside(&(icon->geom), x, y)) continue;
      _dnd_over(icon);
      return;
    }
  }
  _dnd_over_none_handle(sd);
}

// drop handling
static void
_cb_drop_in(void *data EINA_UNUSED, Evas_Object *o EINA_UNUSED)
{
  //   Smart_Data *sd = data;
  printf("XXX: drop in.....\n");
  // XXX: call drop in smart callback}
}

static void
_cb_drop_out(void *data, Evas_Object *o EINA_UNUSED)
{
  Smart_Data *sd = data;
  _dnd_scroll_end_handle(sd);
  _dnd_over_none_handle(sd);
  printf("XXX: drop out.....\n");
  // XXX: call drop out smart callback}
}

void
_cb_drop_pos(void *data, Evas_Object *o EINA_UNUSED, Evas_Coord x, Evas_Coord y,
             Elm_Xdnd_Action action EINA_UNUSED)
{
  Smart_Data *sd = data;
  Evas_Coord  fmx, fmy, fmw, fmh;
  Evas_Coord  sx, sy, sw, sh;

  sd->dnd_action = action;
  evas_object_geometry_get(sd->o_smart, &fmx, &fmy, &fmw, &fmh);
  evas_object_geometry_get(sd->o_scroller, &sx, &sy, &sw, &sh);
  sd->dnd_x = x - fmx;
  sd->dnd_y = y - fmy;
  _dnd_scroll_handle(sd, x - sx, y - sy);
  if (((x - sx) > 0) && ((y - sy) > 0) && ((x - sx) < sw) && ((y - sy) < sh))
    _dnd_over_handle(sd, x - fmx, y - fmy);
  else _dnd_over_none_handle(sd);
  if (sd->over_icon) sd->drop_over = sd->over_icon;
  else sd->drop_over = NULL;
}

static void
_dnd_drop_handle(Smart_Data *sd, char *urilist, Elm_Xdnd_Action act)
{
  char       **plist, **p, *esc;
  Eina_List   *dropicons, *l;
  Icon        *icon;
  int          delta_x = 0, delta_y = 0;
  Eina_Strbuf *buf = cmd_strbuf_new("dnd-drop");

  switch (act) // we ignore ev->action and use local input mods as above
    {
    case ELM_XDND_ACTION_COPY:
      fprintf(stderr, "XXX: COPY\n");
      cmd_strbuf_append(buf, "action", "copy");
      break;
    case ELM_XDND_ACTION_MOVE:
      fprintf(stderr, "XXX: MOVE\n");
      cmd_strbuf_append(buf, "action", "move");
      break;
    case ELM_XDND_ACTION_LINK:
      fprintf(stderr, "XXX: LINK\n");
      cmd_strbuf_append(buf, "action", "link");
      break;
    /* here for documentation but not used
    case ELM_XDND_ACTION_ASK:
      break;
    case ELM_XDND_ACTION_LIST:
      break;
    case ELM_XDND_ACTION_DESCRIPTION:
      break;
    */
    default:
      eina_strbuf_free(buf);
      sd->drop_over = NULL;
      return;
    }

  if (sd->drop_over)
    {
      printf("XXX: DND DROP OVER [%s]\n", sd->drop_over->info.file);
      _icon_path_cmd_strbuf_append(buf, "over", sd, sd->drop_over);
    }
  else printf("XXX: DND DROP ...\n");

  plist = eina_str_split(urilist, "\n", -1);
  for (p = plist; *p != NULL; p++)
    {
      if (**p)
        {
          esc = _escape_parse(*p);
          if (!esc) continue;
          dropicons = _icons_path_find(esc);
          free(esc);
          EINA_LIST_FOREACH(dropicons, l, icon)
          {
            printf("XXX: DROPICON: [%s]\n", icon->info.file);
            if (icon->selected)
              {
                if ((icon->sd == sd) && (icon->drag))
                  {
                    printf("XXX:   dropicon is %s%s\n", icon->sd->config.path,
                           icon->info.file);
                    delta_x = icon->sd->dnd_x - icon->down_rel_x - icon->geom.x;
                    delta_y = icon->sd->dnd_y - icon->down_rel_y - icon->geom.y;
                    icon->drag = EINA_FALSE;
                    break;
                  }
              }
          }
          EINA_LIST_FREE(dropicons, icon);
        }
    }
  _uri_list_cmd_strbuf_append(buf, "path", urilist);
  cmd_strbuf_exe_consume(buf, sd->exe_open);
  for (p = plist; *p != NULL; ++p)
    {
      if (**p)
        {
          esc = _escape_parse(*p);
          if (!esc) continue;
          printf("XXX: DROP FILE: [%s]\n", esc);
          if ((sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM)
              || (sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL))
            {
              Eina_Bool found = EINA_FALSE;
              char      str[128];

              dropicons = _icons_path_find(esc);
              EINA_LIST_FREE(dropicons, icon)
              {
                if (icon->selected)
                  {
                    if (icon->sd == sd)
                      {
                        icon->geom.x += delta_x;
                        icon->geom.y += delta_y;
                      }
                    else
                      {
                        icon->geom.x = icon->sd->dnd_x - (icon->geom.w / 2);
                        icon->geom.y = icon->sd->dnd_y - (icon->geom.h / 2);
                      }
                    printf("XXX: ->     DROPICON: [%s]  %i   %i\n",
                           icon->info.file, icon->geom.x, icon->geom.y);
                    buf = cmd_strbuf_new("meta-set");
                    _icon_path_cmd_strbuf_append(buf, "path", icon->sd, icon);
                    snprintf(str, sizeof(str), "%i,%i",
                             (int)(icon->geom.x / _scale_get(icon->sd)),
                             (int)(icon->geom.y / _scale_get(icon->sd)));
                    cmd_strbuf_append(buf, "xy", str);
                    cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
                    found = EINA_TRUE;
                  }
              }
              if (!found)
                {
                  Eina_Strbuf *buf2 = eina_strbuf_new();

                  if (buf2)
                    {
                      printf("XXX: ->     DROPEXTICON: [%s]  %i   %i\n", esc,
                             sd->dnd_x, sd->dnd_y);
                      buf = cmd_strbuf_new("meta-set");

                      eina_strbuf_append(buf2, sd->config.path);
                      eina_strbuf_append(buf2, ecore_file_file_get(esc));
                      cmd_strbuf_append(buf, "path",
                                        eina_strbuf_string_get(buf2));
                      snprintf(str, sizeof(str), "%i,%i",
                               (int)(sd->dnd_x / _scale_get(sd)),
                               (int)(sd->dnd_y / _scale_get(sd)));
                      cmd_strbuf_append(buf, "xy", str);
                      cmd_strbuf_exe_consume(buf, sd->exe_open);
                      eina_strbuf_free(buf2);
                    }
                }
            }
          free(esc);
        }
    }
  sd->relayout = EINA_TRUE;
  sd->drop_over = NULL;
  evas_object_smart_calculate(sd->o_smart);
  free(*plist);
  free(plist);
}

static void
_cb_menu_dnd_ask_done(void *data, void *data2 EINA_UNUSED,
                      Evas_Object *o EINA_UNUSED,
                      const Efm_Menu *m EINA_UNUSED)
{
  Smart_Data *sd = data;

  if (sd->dnd_drop_data)
    {
      free(sd->dnd_drop_data);
      sd->dnd_drop_data = NULL;
    }
}

static void
_cb_menu_dnd_drop_copy(void *data, void *data2 EINA_UNUSED,
                       Evas_Object *o EINA_UNUSED,
                       const Efm_Menu_Item *mi EINA_UNUSED)
{
  Smart_Data *sd = data;

  _dnd_drop_handle(sd, sd->dnd_drop_data, ELM_XDND_ACTION_COPY);
}

static void
_cb_menu_dnd_drop_move(void *data, void *data2 EINA_UNUSED,
                       Evas_Object *o EINA_UNUSED,
                       const Efm_Menu_Item *mi EINA_UNUSED)
{
  Smart_Data *sd = data;

  _dnd_drop_handle(sd, sd->dnd_drop_data, ELM_XDND_ACTION_MOVE);
}

static void
_cb_menu_dnd_drop_link(void *data, void *data2 EINA_UNUSED,
                       Evas_Object *o EINA_UNUSED,
                       const Efm_Menu_Item *mi EINA_UNUSED)
{
  Smart_Data *sd = data;

  _dnd_drop_handle(sd, sd->dnd_drop_data, ELM_XDND_ACTION_LINK);
}

static Eina_Bool
_cb_drop(void *data, Evas_Object *o EINA_UNUSED, Elm_Selection_Data *ev)
{
  Smart_Data          *sd = data;
  char                *tmp;
  const Evas_Modifier *m   = evas_key_modifier_get(evas_object_evas_get(o));
  Elm_Xdnd_Action      act = ELM_XDND_ACTION_MOVE; // default action

  // We need to check modifiers
#define M(_m) evas_key_modifier_is_set(m, _m)

  // on win ctl -> ctl, alt -> alt, win -> super
  // on win shift == move, ctl == copy, alt = link, shift+ctl = link
  if (M("Shift") && M("Alt")) act = ELM_XDND_ACTION_LINK;
  else if (M("Control")) act = ELM_XDND_ACTION_COPY;
  else if (M("Shift")) act = ELM_XDND_ACTION_MOVE;
  else if (M("Alt")) act = ELM_XDND_ACTION_ASK; // added this myself
  // else ... leave as default action

  // on mac ctl -> ctl, option -> alt, command -> super
  // on mac: super == move, alt == copy, alt+super = link
  // if (M("Alt") && M("Super")) act = ELM_XDND_ACTION_LINK;
  // else if (M("Alt")) act = ELM_XDND_ACTION_COPY;
  // else if (M("Super")) act = ELM_XDND_ACTION_MOVE;
  // else if (M("Control")) act = ELM_XDND_ACTION_ASK; // added this myself
  // else ... leave as default action
#undef M

  tmp = malloc(ev->len + 1);
  if (!tmp)
    {
      sd->drop_over = NULL;
      goto err;
    }
  memcpy(tmp, ev->data, ev->len);
  tmp[ev->len] = 0;
  if (act == ELM_XDND_ACTION_ASK)
    {
      // XXX: store tmp as urilist and pop up ask menu
      Efm_Menu *m
        = _efm_menu_add("What to do?", NULL, _cb_menu_dnd_ask_done, sd, NULL);
      _efm_menu_it_normal(m, "Copy", NULL, _cb_menu_dnd_drop_copy, sd, NULL);
      _efm_menu_it_normal(m, "Move", NULL, _cb_menu_dnd_drop_move, sd, NULL);
      _efm_menu_it_normal(m, "Link", NULL, _cb_menu_dnd_drop_link, sd, NULL);
      _efm_menu_show(sd->o_smart, m, sd->dnd_x, sd->dnd_y);
      sd->dnd_drop_data = tmp;
    }
  else
    { // handle dnd here as we know the action
      _dnd_drop_handle(sd, tmp, act);
      free(tmp);
    }
err:
  return EINA_TRUE;
}

void
_drop_init(Smart_Data *sd)
{ // called once we have our elm scroller around our file view
  elm_drop_target_add(sd->o_scroller, ELM_SEL_FORMAT_URILIST, _cb_drop_in, sd,
                      _cb_drop_out, sd, _cb_drop_pos, sd, _cb_drop, sd);
}

// drag handling
static Evas_Object *
_cb_drag_icon_add(void *data, Evas_Object *parent, Evas_Coord *xoff,
                  Evas_Coord *yoff)
{
  Icon        *icon = data;
  Icon        *icon2;
  Block       *block;
  Evas_Object *obj = icon->sd->o_smart;
  Evas_Object *o;
  const char  *theme_edj_file;
  Evas        *e;
  ENTRY        NULL;
  Eina_List   *bl, *il;
  int          num = 0;

  if (sd->config.view_mode >= EFM_VIEW_MODE_LIST)
    { // figure out number so we know odd/even mode for list items
      EINA_LIST_FOREACH(sd->blocks, bl, block)
      {
        if (block->realized_num <= 0)
          {
            num += eina_list_count(block->icons);
            continue;
          }
        EINA_LIST_FOREACH(block->icons, il, icon2)
        {
          if (icon == icon2) goto found;
          num++;
        }
        num += eina_list_count(block->icons);
      }
    }
found:
  e = evas_object_evas_get(parent);
  theme_edj_file
    = elm_theme_group_path_find(NULL, "e/fileman/default/icon/fixed");
  _icon_object_add(icon, icon->sd, e, theme_edj_file, EINA_FALSE, num);
  o = icon->o_base;
  evas_object_resize(o, icon->geom.w, icon->geom.h);
  printf("DDD: %i , %i .... %i , %i\n", icon->down_x, icon->last_x,
         icon->down_y, icon->last_y);
  if (xoff)
    *xoff = (icon->geom.x + icon->sd->geom.x) + (icon->last_x - icon->down_x);
  if (yoff)
    *yoff = (icon->geom.y + icon->sd->geom.y) + (icon->last_y - icon->down_y);
  if (xoff && yoff)
    printf("DND: drag begin %p %p  off: %i %i\n", obj, o, *xoff, *yoff);
  else printf("DND: drag begin %p %p\n", obj, o);
  return o;
}

static void
_cb_drag_pos(void *data EINA_UNUSED, Evas_Object *obj_drag EINA_UNUSED,
             Evas_Coord x, Evas_Coord y, Elm_Xdnd_Action action)
{
  // get shift/ctrl/mouse button?
  printf("DND: pos %i %i act=%i\n", x, y, action);
}

static void
_cb_drag_accept(void *data EINA_UNUSED, Evas_Object *obj_drag EINA_UNUSED,
                Eina_Bool accept)
{
  printf("DND: accept %i\n", accept);
}

static void
_cb_drag_done(void *data, Evas_Object *obj_drag EINA_UNUSED)
{
  Icon        *icon = data;
  Evas_Object *obj;
  Eina_Strbuf *buf;

  if (icon->sd)
    {
      obj = icon->sd->o_smart;
      ENTRY _icon_free(icon);

      printf("DND: drag done %p %p\n", obj, obj_drag);
      buf = cmd_strbuf_new("dnd-drag-end");
      if (buf) cmd_strbuf_exe_consume(buf, sd->exe_open);
      sd->drag            = EINA_FALSE;
      icon->o_base        = NULL; // try and not del this as dnd will do it
      icon->sd->drag_icon = NULL;
    }
  _icon_free(icon);
}

void
_drag_start(Icon *icon, Efm_Action action)
{
  Eina_Strbuf *strbuf;
  Icon        *icon_dup;
  const char  *action_str = "move";
  Elm_Xdnd_Action action_xdnd = ELM_XDND_ACTION_MOVE;

    strbuf
    = eina_strbuf_new();
  if (!strbuf) return;
  icon_dup = _icon_dup(icon);
  if (!icon_dup) goto err;

  if (icon->sd->rename_icon) _icon_rename_end(icon->sd->rename_icon);
  printf("XXX: begin dnd\n");

  icon->sd->drag         = EINA_TRUE;
  icon->sd->just_dragged = EINA_TRUE;
  icon->sd->drag_icon    = icon_dup;
  if (_selected_icons_uri_strbuf_append(icon->sd, strbuf))
    {
      Eina_Strbuf *buf = cmd_strbuf_new("dnd-drag-begin");

      switch (action)
        {
        case EFM_ACTION_MOVE:
          action_str = "move";
          action_xdnd = ELM_XDND_ACTION_MOVE;
          break;
        case EFM_ACTION_COPY:
          action_str = "copy";
          action_xdnd = ELM_XDND_ACTION_COPY;
          break;
        case EFM_ACTION_ASK:
          action_str = "ask";
          action_xdnd = ELM_XDND_ACTION_ASK;
          break;
        default:
          break;
        }
       if (buf)
         {
           cmd_strbuf_append(buf, "action", action_str);
           _uri_list_cmd_strbuf_append(buf, "path",
                                       eina_strbuf_string_get(strbuf));
           cmd_strbuf_exe_consume(buf, icon->sd->exe_open);
         }
      elm_drag_start(icon_dup->sd->o_scroller, ELM_SEL_FORMAT_URILIST,
                     eina_strbuf_string_get(strbuf), action_xdnd,
                     _cb_drag_icon_add, icon_dup, _cb_drag_pos, icon_dup,
                     _cb_drag_accept, icon_dup, _cb_drag_done, icon_dup);
    }
err:
  eina_strbuf_free(strbuf);
}
