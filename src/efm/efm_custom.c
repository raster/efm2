#include "efm.h"
#include "efm_util.h"
#include "efm_custom.h"
#include "bitmap.h"

#define INVALID -999999

typedef struct
{
  int alloc_w, alloc_h; // size of tile region allocated to allow growth
  int tw, th;           // size of a tile
  unsigned int *bitmap; // allocated tile bitmap - 1 bit per tile
} Custom_Data;

////////////////////////////////////////////////////////////////////////////

void
_icon_custom_data_free(Smart_Data *sd)
{ // free custom data
  Custom_Data *cd = sd->custom_data;

  if (!cd) return;
  _bitmap_free(cd->bitmap);
  free(cd);
  sd->custom_data = NULL;
}

static void
_icon_custom_data_alloc(Smart_Data *sd)
{ // allocate base custom data
  Custom_Data *cd = sd->custom_data;

  if (cd) return;
  cd = calloc(1, sizeof(Custom_Data));
  if (!cd) return;
  // default tile size
  cd->tw          = 16;
  cd->th          = 16;
  sd->custom_data = cd;
}

static inline void
_icon_custom_data_coord1_to_tile(int tsz, int *x, int *w)
{ // 1d convert a coord to a tile sized block (round down x, round up w)
  *w = (*x + *w + tsz - 1) / tsz;
  *x = *x / tsz;
  *w -= *x;
}

static inline void
_icon_custom_data_coord_to_tile(Custom_Data *cd, int *x, int *y, int *w, int *h)
{ // translate x,y,w,h into tile units
  _icon_custom_data_coord1_to_tile(cd->tw, x, w);
  _icon_custom_data_coord1_to_tile(cd->th, y, h);
}

/* not used atm
static inline Eina_Bool
_icon_custom_data_region1_clip(int allocsz, int *x, int *w)
{ // 1d clip run of x+w to 0->sz run, return false == 0 sized
  if (*x < 0)
    {
      *w += *x;
      if (*w <= 0) return EINA_FALSE; // out of bounds
      *x = 0;
    }
  if ((*x + *w) > allocsz) *w = allocsz - *x;
  if (*w <= 0) return EINA_FALSE; // out of bounds
  return EINA_TRUE;
}
*/

/* not used atm
static inline Eina_Bool
_icon_custom_data_region_clip(Custom_Data *cd, int *x, int *y, int *w, int *h)
{ // clip to 0,0 -> alloc_w,alloc_h region
  if (!_icon_custom_data_region1_clip(cd->alloc_w, x, w)) return EINA_FALSE;
  if (!_icon_custom_data_region1_clip(cd->alloc_h, y, h)) return EINA_FALSE;
  return EINA_TRUE;
}
*/

static void
_icon_custom_data_resize(Smart_Data *sd, int w, int h)
{ // resize bitmap and return set bits in bitmap, with new empty bits being 0
  Custom_Data *cd = sd->custom_data;
  int          tw, th, alloc_w, alloc_h;

  if (!cd) _icon_custom_data_alloc(sd);
  cd = sd->custom_data;
  if (!cd) return;

  tw      = (w + cd->tw - 1) / cd->tw;
  th      = (h + cd->th - 1) / cd->th;
  alloc_w = 32 * ((tw + 31) / 32); // must do 32 chunks of tiles (int=32bit)
  alloc_h = 32 * ((th + 31) / 32); // chunks of 32 vertially to be consistent
  // if we're smaller size - return;
  if ((alloc_w <= cd->alloc_w) && (alloc_h <= cd->alloc_h)) return;

  cd->bitmap
    = _bitmap_resize(cd->bitmap, cd->alloc_w, cd->alloc_h, alloc_w, alloc_h);
  if (!cd->bitmap)
    {
      cd->alloc_w = 0;
      cd->alloc_h = 0;
      return;
    }
  cd->alloc_w = alloc_w;
  cd->alloc_h = alloc_h;
}

void
_icon_custom_data_bitmap_clear(Smart_Data *sd)
{ // set all of the bitmap to 0
  Custom_Data *cd = sd->custom_data;

  if ((!cd) || (!cd->bitmap)) return; // nothing to clear...
  _bitmap_clear(cd->bitmap, cd->alloc_w, cd->alloc_h);
}

static void
_icon_custom_data_bitmap_fill(Smart_Data *sd, int x, int y, int w, int h)
{ // fill in a rect region of the custom data bitmap
  Custom_Data *cd;
  int          neww, newh;

  _icon_custom_data_alloc(sd);
  cd = sd->custom_data;
  _icon_custom_data_coord_to_tile(cd, &x, &y, &w, &h);

  // if off the top or left completely... ignore
  if (x < 0)
    {
      w += x;
      x = 0;
      if (w < 0) return;
    }
  if (y < 0)
    {
      h += y;
      y = 0;
      if (h < 0) return;
    }

  if (((x + w) > cd->alloc_w) || ((y + h) > cd->alloc_h))
    { // our fill region requires we expand the bitmap...
      neww = (x + w);
      if (neww < cd->alloc_w) neww = cd->alloc_w;
      newh = (y + h);
      if (newh < cd->alloc_h) newh = cd->alloc_h;
      _icon_custom_data_resize(sd, neww * cd->tw, newh * cd->th);
    }
  if (!cd->bitmap) return;
  _bitmap_fill(cd->bitmap, cd->alloc_w, cd->alloc_h, x, y, w, h);
}

void
_icon_custom_data_all_icons_fill(Smart_Data *sd)
{ // fill in all placed icons in custom data bitmap
  Eina_List *l;
  Icon      *icon;

  EINA_LIST_FOREACH(sd->icons, l, icon)
  {
    if (icon->geom.x == INVALID) continue;
    if (icon->geom.w == INVALID)
      {
        icon->geom.w = sd->icon_min_w;
        icon->geom.h = sd->icon_min_h;
      }
    _icon_custom_data_bitmap_fill(sd, icon->geom.x, icon->geom.y, icon->geom.w,
                                  icon->geom.h);
  }
  /*
     Custom_Data *cd = sd->custom_data;
     if ((cd) && (cd->bitmap) && (cd->alloc_w > 0))
       _bitmap_dump(cd->bitmap, cd->alloc_w, cd->alloc_h);
   */
}

static void
_icon_custom_data_tl_to_br_find(Smart_Data *sd, int *x, int *y, int *w, int *h, Eina_Bool vert)
{
  Custom_Data *cd;

  _icon_custom_data_alloc(sd);
  cd = sd->custom_data;
  if (!cd) return;

  *x = 0;
  *y = 0;
  *w = (*w + cd->tw - 1) / cd->tw;
  *h = (*h + cd->th - 1) / cd->th;
  if (!vert)
    _bitmap_find_tl_to_br(cd->bitmap, sd->geom.w / cd->tw, cd->alloc_w,
                          cd->alloc_h, *w, *h, x, y, w, h);
  else 
    _bitmap_find_tl_to_br_v(cd->bitmap, sd->geom.h / cd->th, cd->alloc_w,
                            cd->alloc_h, *w, *h, x, y, w, h);
  *x *= cd->tw;
  *y *= cd->th;
  *w *= cd->tw;
  *h *= cd->th;
}

//////////////////////////////////////////////////////////////////////////////

void
_icon_custom_position_find(Icon *icon)
{ // allocate a spot for this icon as we don't have it stored
  Eina_Rectangle geom = icon->geom;

  geom.x = 0;
  geom.y = 0;
  _icon_custom_data_resize(icon->block->sd, icon->block->sd->geom.w,
                           icon->block->sd->geom.h);
  if (icon->sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM)
    _icon_custom_data_tl_to_br_find(icon->block->sd, &geom.x, &geom.y, &geom.w,
                                    &geom.h, EINA_FALSE);
  else if (icon->sd->config.view_mode == EFM_VIEW_MODE_ICONS_CUSTOM_VERTICAL)
    _icon_custom_data_tl_to_br_find(icon->block->sd, &geom.x, &geom.y, &geom.w,
                                    &geom.h, EINA_TRUE);
  _icon_custom_data_bitmap_fill(icon->block->sd, geom.x, geom.y, geom.w,
                                geom.h);
  icon->geom = geom;
}

void
_icon_custom_position_placed(Icon *icon)
{
  _icon_custom_data_bitmap_fill(icon->block->sd, icon->geom.x, icon->geom.y,
                                icon->geom.w, icon->geom.h);
}
