#include <Eina.h>
#include <ctype.h>

#include "esc.h"

static inline int
_xtov(char x)
{
  if ((x >= '0') && (x <= '9')) return x - '0';
  if ((x >= 'a') && (x <= 'f')) return 10 + (x - 'a');
  if ((x >= 'A') && (x <= 'F')) return 10 + (x - 'A');
  return 0;
}

char *
escape(const char *src_in)
{
  Eina_Strbuf *strbuf = eina_strbuf_new();
  char        *str;
  const char  *s;

  for (s = src_in; *s; s++)
    {
      if ((*s <= ',') || (*s == '%') || ((*s >= ':') && (*s <= '@'))
          || ((*s >= '[') && (*s <= '`')) || (*s >= '{'))
        {
          unsigned char tmp;

          tmp = s[0];
          eina_strbuf_append_printf(strbuf, "%%%02x", tmp);
        }
      else eina_strbuf_append_char(strbuf, *s);
    }
  str = eina_strbuf_string_steal(strbuf);
  eina_strbuf_free(strbuf);
  return str;
}

char *
unescape(const char *src_in)
{
  char       *dest = malloc(strlen(src_in) + 1);
  char       *d;
  const char *s;

  for (d = dest, s = src_in; *s; d++)
    {
      if ((s[0] == '%') && (!isspace(s[1])))
        {
          if ((s[1]) && (s[2]))
            {
              *d = (_xtov(s[1]) << 4) | (_xtov(s[2]));
              s += 3;
            }
          else s++;
        }
      else
        {
          *d = s[0];
          s++;
        }
    }
  *d = 0;
  return dest;
}