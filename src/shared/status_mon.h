#ifndef STATUS_MON_H
#define STATUS_MON_H

#include <Eina.h>
#include "cmd.h"

typedef struct _Status_Op Status_Op;

typedef void (*Status_Mon_Callback)(void *data, Status_Op *op, Cmd *cmd);

void status_mon_init(void);
void status_mon_callback_add(Status_Mon_Callback cb, void *data);
void status_mon_callback_del(Status_Mon_Callback cb, void *data);
void status_mon_begin(void);
void status_mon_shutdown(void);

#endif