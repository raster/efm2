#include "sha.h"
#include "efm_config.h"
#include <Eina.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

void
sha1_stat(const struct stat *st, unsigned char dst[20])
{
  char buf[128];

#ifdef STAT_NSEC
#  ifdef st_mtime
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st)->st_mtim.tv_nsec)
#  else
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st)->st_mtimensec)
#  endif
#else
#  define STAT_NSEC_MTIME(st) (unsigned long long)(0)
#endif

  snprintf(buf, sizeof(buf), "%llu %llu %llu",
           (unsigned long long)(st->st_size),
           (unsigned long long)(st->st_mtime), STAT_NSEC_MTIME(st));
  eina_sha1((unsigned char *)buf, strlen(buf), dst);
}

void
sha1_str(unsigned char sha[20], char shastr[41])
{
  const char *chmap = "0123456789abcdef";
  int         i;

  for (i = 0; i < 20; i++)
    {
      shastr[(i * 2)]     = chmap[(sha[i] >> 4) & 0xf];
      shastr[(i * 2) + 1] = chmap[sha[i] & 0xf];
    }
  shastr[i * 2] = 0;
}
