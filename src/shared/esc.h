#ifndef ESC_H
#define ESC_H 1

char *escape(const char *src_in);
char *unescape(const char *src_in);

#endif