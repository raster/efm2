#include "eina_list.h"
#include <stdio.h>
#define EFL_BETA_API_SUPPORT 1

#include <Eina.h>
#include <Ecore_File.h>

#include "status_mon.h"

struct _Status_Op
{
  const char *path;
  FILE       *file;
};

typedef struct
{
  Status_Mon_Callback cb;
  void               *data;
  Eina_Bool           delete_me : 1;
} Status_Callback;

static Ecore_File_Monitor *mon      = NULL;
static Eina_List          *ops      = NULL;
static Eina_List          *cbs      = NULL;
static int                 cbs_walk = 0;
static int                 cbs_del  = 0;

static void
status_callback_clean(void)
{
  Eina_List       *l, *ll;
  Status_Callback *sc;

  if ((cbs_walk > 0) || (cbs_del <= 0)) return;
  EINA_LIST_FOREACH_SAFE(cbs, l, ll, sc)
  {
    if (sc->delete_me)
      {
        cbs = eina_list_remove_list(cbs, l);
        free(sc);
      }
  }
  cbs_del = 0;
}

static Status_Op *
status_op_add(const char *path)
{
  Status_Op *so = calloc(1, sizeof(Status_Op));

  if (!so) return NULL;
  so->path = eina_stringshare_add(path);
  so->file = fopen(so->path, "r");
  ops      = eina_list_append(ops, so);
  return so;
}

static void
status_op_del(Status_Op *so)
{
  ops = eina_list_remove(ops, so);
  fclose(so->file);
  eina_stringshare_del(so->path);
  free(so);
}

static Status_Op *
status_op_find(const char *path)
{
  Eina_List *l;
  Status_Op *so;

  EINA_LIST_FOREACH(ops, l, so)
  {
    if (!strcmp(so->path, path)) return so;
  }
  return NULL;
}

static void
status_op_read(Status_Op *so)
{
  // XXX: move this to a thread, although these status files SHOULD
  // be in a ramdisk or cache and thus immediately readable without
  // any blocking, so i can do this later as an optimmization if needed
  char             buf[PATH_MAX + 256];
  Cmd             *cmd;
  long             pos;
  Eina_List       *l;
  Status_Callback *sc;

  pos = ftell(so->file);
  fseek(so->file, pos, SEEK_SET);
  while (fgets(buf, sizeof(buf), so->file))
    {
      size_t sz = strlen(buf);

      if (sz < 1) continue;
      buf[sz - 1] = 0; // nuke \n
      cmd         = cmd_parse(buf);
      if (!cmd) continue;
      cbs_walk++;
      EINA_LIST_FOREACH(cbs, l, sc) sc->cb(sc->data, so, cmd);
      cbs_walk--;
      status_callback_clean();
      cmd_free(cmd);
    }
}

static void
_file_add(const char *path)
{
  Status_Op *so = status_op_find(path);

  if (so) return; // it already exists - don't do anything
  so = status_op_add(path);
  if (so) status_op_read(so);
}

static void
_file_del(const char *path)
{
  Status_Op       *so = status_op_find(path);
  Eina_List       *l;
  Status_Callback *sc;

  if (!so) return;

  cbs_walk++;
  EINA_LIST_FOREACH(cbs, l, sc) sc->cb(sc->data, so, NULL);
  cbs_walk--;
  status_callback_clean();
  status_op_del(so);
}

static void
_file_mod(const char *path)
{
  Status_Op *so = status_op_find(path);

  if (so) status_op_read(so);
}

static void
_cb_mon(void *data EINA_UNUSED, Ecore_File_Monitor *em EINA_UNUSED,
        Ecore_File_Event event, const char *path)
{ // a efm ops dir event happened - deal with it
  if (event == ECORE_FILE_EVENT_CREATED_FILE) _file_add(path);
  else if (event == ECORE_FILE_EVENT_DELETED_FILE) _file_del(path);
  else if (event == ECORE_FILE_EVENT_MODIFIED) _file_mod(path);
}

void
status_mon_init(void)
{ // for completenexx - does nothing at the moment
}

void
status_mon_callback_add(Status_Mon_Callback cb, void *data)
{
  Status_Callback *sc = calloc(1, sizeof(Status_Callback));

  if (!sc) return;
  sc->cb   = cb;
  sc->data = data;
  cbs      = eina_list_append(cbs, sc);
}

void
status_mon_callback_del(Status_Mon_Callback cb, void *data)
{
  Eina_List       *l;
  Status_Callback *sc;

  EINA_LIST_FOREACH(cbs, l, sc)
  {
    if ((cb == sc->cb) && (data == sc->data))
      {
        if (cbs_walk <= 0)
          {
            cbs = eina_list_remove_list(cbs, l);
            free(sc);
          }
        else
          {
            sc->delete_me = EINA_TRUE;
            cbs_del++;
          }
        break;
      }
  }
}

void
status_mon_begin(void)
{
  Eina_Iterator         *it;
  Eina_File_Direct_Info *info;
  char                   buf[PATH_MAX];

  // where is the efm ops dir
  eina_vpath_resolve_snprintf(buf, sizeof(buf), "(:usr.run:)/efm/ops");
  ecore_file_mkpath(buf);
  // monitor for file changes in the ops dir
  mon = ecore_file_monitor_add(buf, _cb_mon, NULL);
  // list what is there right now to begin with
  it = eina_file_direct_ls(buf);
  if (it)
    {
      EINA_ITERATOR_FOREACH(it, info) _file_add(info->path);
      eina_iterator_free(it);
    }
}

void
status_mon_shutdown(void)
{
  if (mon) ecore_file_monitor_del(mon);
  mon = NULL;
  // XXX: delete so's
}