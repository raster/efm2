#include "util.h"
#include "efm_config.h"
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void
util_file_mode_parent_copy(const char *file, Eina_Bool is_dir)
{
  struct stat st;
  char       *s, *dir_parent = strdup(file);

  if (!dir_parent) return;
  s = strrchr(dir_parent, '/');
  if (!s) goto err;
  s[1] = '\0'; // get parent dir by truncating after /
  if (lstat(dir_parent, &st) == 0)
    { // copy the parent dir mode to the child file given
      chown(file, st.st_uid, st.st_gid);
      // if it's not a dir - filter out execute mode
      if (!is_dir)
        st.st_mode &= S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
      chmod(file, st.st_mode);
    }
err:
  free(dir_parent);
}

Util_Modtime
util_file_modtime_get(const char *file)
{
  struct stat st;
  Util_Modtime mt = { ~(0ull), ~(0ull) }; // default - ivalid all bits 1

  if (!file) return mt;
  if (stat(file, &st) == 0)
    {
      mt.sec = st.st_mtime;
#ifdef STAT_NSEC
#  ifdef st_mtime
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st).st_mtim.tv_nsec)
#  else
#    define STAT_NSEC_MTIME(st) (unsigned long long)((st).st_mtimensec)
#  endif
#else
#  define STAT_NSEC_MTIME(st) (unsigned long long)(0)
#endif
      mt.nsec = STAT_NSEC_MTIME(st);
    }
  return mt;
}

int
util_modtime_cmp(Util_Modtime t1, Util_Modtime t2)
{
  if (t1.sec > t2.sec) return 1; // t1 > t2
  else if (t1.sec < t2.sec) return -1; // t1 < t2
  // seconds equal - check nsec
  if (t1.nsec > t2.nsec) return 1; // t1 > t2
  else if (t1.nsec < t2.nsec) return -1; // t1 < t2
  // they are both equal
  return 0; // t1 == t2
}

Eina_Bool
util_modtime_valid(Util_Modtime t)
{
  const Util_Modtime mt = { ~(0ull), ~(0ull) }; // default - ivalid all bits 1

  if ((t.sec == mt.sec) && (t.nsec == mt.nsec)) return EINA_FALSE;
  return EINA_TRUE;
}