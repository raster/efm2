#include "mimeapps.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

static const char *
_xdg_defaults_get(const char *path, const char *mime)
{
  Efreet_Ini *ini;
  const char *str;

  if (access(path, R_OK) != 0) return NULL;
  ini = efreet_ini_new(path);
  if (!ini) return NULL;

  efreet_ini_section_set(ini, "Default Applications");
  str = eina_stringshare_add(efreet_ini_string_get(ini, mime));
  efreet_ini_free(ini);
  return str;
}

static Efreet_Desktop *
_xdg_desktop_from_string_list(const char *strlist)
{
  Efreet_Desktop *desktop = NULL;
  char **array = eina_str_split(strlist, ";", 0);
  unsigned int i;

  if (!array) return NULL;
  for (i = 0; array[i] != NULL; i++)
    {
      const char *name = array[i];

      if (name[0] == '/') desktop = efreet_desktop_new(name);
      else desktop = efreet_util_desktop_file_id_find(name);
      if (desktop)
        {
          if (desktop->exec) break;
          else
            {
              efreet_desktop_free(desktop);
              desktop = NULL;
            }
        }
    }
  free(array[0]);
  free(array);
  return desktop;
}

static Efreet_Desktop *
_desktop_first_free_others(Eina_List *lst)
{
  Efreet_Desktop *desktop, *d;

  if (!lst) return NULL;
  desktop = lst->data;
  efreet_desktop_ref(desktop);
  EINA_LIST_FREE(lst, d) efreet_desktop_free(d);
  return desktop;
}

Efreet_Desktop *
_mimeapps_handler_find(const char *mime)
{
  Efreet_Desktop *desktop = NULL;
  char path[PATH_MAX];
  const char *name;

  snprintf(path, sizeof(path), "%s/mimeapps.list",
           efreet_config_home_get());
  name = _xdg_defaults_get(path, mime);
  if (!name)
    {
      const Eina_List *n, *dirs = efreet_data_dirs_get();
      const char *d;
      EINA_LIST_FOREACH(dirs, n, d)
        {
          snprintf(path, sizeof(path), "%s/applications/defaults.list", d);
          name = _xdg_defaults_get(path, mime);
          if (name) break;
        }
    }
  if (name)
    {
      desktop = _xdg_desktop_from_string_list(name);
      eina_stringshare_del(name);
    }
  if (!desktop)
    {
      if (!strcmp(mime, "inode/directory"))
        desktop = efreet_util_desktop_file_id_find("enlightenment_filemanager.desktop");
    }
  if (!desktop)
    desktop = _desktop_first_free_others(efreet_util_desktop_mime_list(mime));
  return desktop;
}
