#ifndef MIMEAPPS_H
#define MIMEAPPS_H 1

#include <Eina.h>
#include <Efreet.h>

Efreet_Desktop *_mimeapps_handler_find(const char *mime);

#endif
