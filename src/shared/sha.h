#ifndef SHA_H
#define SHA_H 1

#include "efm_config.h"

#include <sys/stat.h>

void sha1_stat(const struct stat *st, unsigned char dst[20]);
void sha1_str(unsigned char sha[20], char shastr[41]);

#endif
