#ifndef UTIL_H
#define UTIL_H

#include <Eina.h>

typedef struct
{
  unsigned long long sec, nsec;
} Util_Modtime;

void         util_file_mode_parent_copy(const char *file, Eina_Bool is_dir);
Util_Modtime util_file_modtime_get(const char *file);
int          util_modtime_cmp(Util_Modtime t1, Util_Modtime t2);
Eina_Bool    util_modtime_valid(Util_Modtime t);

#endif