// XXX: should make this config

static inline Eina_Bool
check_thumb_image(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("image/*", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/x-docbook+xml", mime, EINA_FNMATCH_CASEFOLD))
    return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_font(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("font/*", mime, EINA_FNMATCH_CASEFOLD)) return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_paged(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("application/*pdf", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/acrobat", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/postscript", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/vnd.ms-powerpoint", mime,
                      EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/msword", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/vnd.ms-word", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch(
        "application/"
        "vnd.openxmlformats-officedocument.wordprocessingml.document",
        mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("application/vnd.oasis.opendocument.text*", mime,
                      EINA_FNMATCH_CASEFOLD))
    return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_music(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("audio/mpeg", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("audio/ogg", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("audio/aac", mime, EINA_FNMATCH_CASEFOLD)
      || eina_fnmatch("audio/flac", mime, EINA_FNMATCH_CASEFOLD))
    return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_video(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("video/*", mime, EINA_FNMATCH_CASEFOLD)) return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_edje(const char *path EINA_UNUSED, const char *mime)
{
  if (eina_fnmatch("application/x-edje", mime, EINA_FNMATCH_CASEFOLD))
    return EINA_TRUE;
  return EINA_FALSE;
}

static inline Eina_Bool
check_thumb_any(const char *path, const char *mime)
{
  if (check_thumb_image(path, mime) || check_thumb_font(path, mime)
      || check_thumb_paged(path, mime) || check_thumb_music(path, mime)
      || check_thumb_video(path, mime) || check_thumb_edje(path, mime))
    return EINA_TRUE;
  return EINA_FALSE;
}
