#ifndef CMD_H
#define CMD_H 1

#include <Eina.h>
#include <Ecore.h>
#include "efm.h"

// CURRENT COMMANDS:
////////////////////
//
// file-add               // a file in a dir being monitored has been added
// file-del               // a file in a dir being monitored has been deleted
// file-mod               // a file in a dir being monitored has been modified
// dir-set                // tell the backend what dir to open/list/watch
// dir-del                // the dir being monitored/listed has been deleted
// meta-set               // set metadata for a file path
// meta-del               // delete metadata for a file path
// list-begin             // begin initial listing of dir
// list-end               // end initial listing of dir
// viewmode-set           // force a specific view mode from backend
// detail-header-set      // set detail header text, size
// dir-request            // backend requests to show a new dir
// backend-set            // swich to a new named backend
// timer-add              // backend can ask for a timer to be added
// timer-del              // backend can ask for a timer to be deleted
// timer                  // event from front to backend that a timer ticked
// file-detail            // update the file detail in custom detail mode
// file-detail-change     // user interacted with detail - changed it
// file-detail-clicked    // user interacted with detail - clicked it
// file-detail-select     // user interacted with detail - selected sub item
//
// cnp-cut
// cnp-copy
// cnp-paste
// dnd-drag-begin
// dnd-drag-end
// dnd-drop
// dnd-hover
//
// file-run               // double-click file (or enter on kbd)
// file-clicked           // a single click
// file-selected          // file became selected
// file-unselected        // file became unselected
// file-delete            // user requested to delete file
// file-rename            // user provided a new name for file
//
////////////////////

// FUTURE COMMANDS:
///////////////////
//
// dir-usage  // how much data, number of files, dirs etc.
//
///////////////////

typedef struct _Cmd
{
  size_t        buf_size;
  char         *buf;
  const char   *command;
  Efm_Sort_Mode sort_mode;
  const char   *dict[];
} Cmd;

Cmd *cmd_parse(const char *cmd);
void cmd_free(Cmd *c);
Cmd *cmd_dup(const Cmd *c);
Cmd *cmd_modify(const Cmd *c, const char *key, const char *val);
void cmd_dump_sterr(Cmd *c);

Eina_Strbuf *cmd_strbuf_new(const char *command);
void cmd_strbuf_append(Eina_Strbuf *strbuf, const char *key, const char *val);
void cmd_strbuf_print_consume(Eina_Strbuf *strbuf);
void cmd_strbuf_exe_consume(Eina_Strbuf *strbuf, Ecore_Exe *exe);
const char *cmd_key_find(const Cmd *c, const char *key);

#endif
