# Things To Do

# NOTES

## Now

* View
  * Free x/y cleanup/grid align
  * Icon with no labels
  * Icons with flush view (like rage video browser view)
  * .efm/.efm in dir set more than backend type (view mode and more)
  * Custom style per icon
* DND auto-open dir on hover-over
* File properties dialog
* File Ops
  * mv <- done
  * cp
  * rm
  * trash (mv to trash dir)
* Progress feedback from file ops
* Display dir usage (# files, size)
* Display dir + subdir usage
* Each file/dir needs a busy status, progress
* In-fm view
  * Status - icon overlay label, icon, progress, busy spinner
  * Status - progress/busy spinner + label + icon
  * Dialog - label + icon + entry + buttons (ok/cancel etc.)
* Typebuf select/filter/cmd
* Choose fs abstraction dir setup etc.
* Filesystem info (df like with total available)
* List mode ".order" files
* Remember scroll pos, view size/pos
* Set window icon correctly for dir
* Set custom icon per file
* Special icons for special filenames/paths (~/Desktop, ~/Videos etc.)
* Single click/select mode
* Favorites view move (manual .order changes, cb's for selecting single click)
* Hide/show hidden files
* Immutable dirs (no moving files/changes - just browse/launch)
* Dir info pane
  * Pane
  * Table of label + icons
  * List of items (label + 2 icons)
* Device monitoring/listing for pluggable devices (mtp usb?)
  * simple-mtpfs
* Device mount/unmount
* Custom target vfs dir (eg sshfs) controls
* Show dir has no permission to view/go into (eg missing r/x on other/group)
* Right click context menu
* Open file with mime handler
* Open with ...
* File actions (separate to open with...)
* Callbacks when column sizes change
* Lock view mode from backend
* Disable dnd/cnp from backend
* Handle del of file in dir to del thumb+meta

## Medium term

* Find filename in tree
* Edit desktop file support (dialog)
* Add thumb edit/gen/modify tools so wrapper scripts can gen thumbs too
* Work out how to allow elm xdnd handling to work in e on shelf/desktop
* Network fs monitoring/listing (smb/nfs/etc.)
  * smbnetfs
  * fuse-nfs
  * sshfs
* File sharing/encrypting/compressing
  * magic-wormhole
  * pgp encrypt
  * gzip/bzip2/xz/7zip
  * email file to X
  * scp file to X
  * dd device to file or to device
* View background / overlay images/edj files
* Typebuf commands (ls, rm, cd, mv, ...)
* Special drop handling per dir (eg favorites adds links)
* Support running back-end cmds as another user
* Chmod support
* Tooltip previews
  * Multi-page show multiple pages
  * Video files show multiple timepoints
* Thumbnailing - add html thumbs bby running e.g.
  * chromium --headless --screenshot=sss.png --window-size=1024x1024     --force-device-scale-factor=0.5 --timeout=10000 file:///path/to/file.html


## Long term

* Encrypted drives/volumes
* Partitioning and formatting tool (chnage volume labels etc)
* Encryption added to partitioning/formatitng tool
  * luks
  * tomb
* Lvm support in partitioning/formatting tool
* Fstrim support
* Smart support for devices
* Badblocks support
* Lsattr/chattr support
* Xattr support (get/set/list)
* Syncing
  * rsync
  * syncthing
